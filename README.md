# Swansea University Student Demonstrator Manager

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=uni.hoffic.cz_web-system&metric=ncloc)](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=uni.hoffic.cz_web-system&metric=alert_status)](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=uni.hoffic.cz_web-system&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=uni.hoffic.cz_web-system&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=uni.hoffic.cz_web-system&metric=security_rating)](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=uni.hoffic.cz_web-system&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=uni.hoffic.cz_web-system&metric=sqale_index)](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system)

SDM is a web system to manage student demonstrators from application to assignment to labs.

It allows:

- an admin to import labs and their times
- lecturers to add information and requirements for the labs
- the students to apply to be student demonstrators
- the admin to select students to become demonstrators
- the admin to assign demonstrators to labs
- the demonstrators to see their up-to-date assignments calendar
- the admin to export the assignments data
- And more...

## Navigation

1. [Installation](#installation)
1. [Updating From Old Versions](#updating-from-old-versions)
1. [Quick Start Guide](#quick-start-guide)
1. [Manual](/docs/MANUAL.md) (dedicated page)
    - [For Administrators](/docs/MANUAL.md#for-administrators)
        - [Admin Interface Overview](/docs/MANUAL.md#admin-interface-overview)
        - [Dashboard](/docs/MANUAL.md#dashboard)
        - [Modules Admin Page](/docs/MANUAL.md#modules-admin-page)
        - [Module Flavours Admin Page](/docs/MANUAL.md#module-flavours-admin-page)
        - [Timetable Entries Admin Page](/docs/MANUAL.md#timetable-entries-admin-page)
        - [Module Configurations Admin Page](/docs/MANUAL.md#module-configurations-admin-page)
        - [Demonstrator Applications Admin Page](/docs/MANUAL.md#demonstrator-applications-admin-page)
        - [Demonstrators Admin Page](/docs/MANUAL.md#demonstrators-admin-page)
        - [Manager](/docs/MANUAL.md#manager)
            - [Timetable](/docs/MANUAL.md#timetable)
                - [Demonstrator Violations](/docs/MANUAL.md#demonstrator-violations)
                - [Lab Violations](/docs/MANUAL.md#lab-violations)
                - [Day Violations](/docs/MANUAL.md#day-violations)
                - [Week Violations](/docs/MANUAL.md#week-violations)
                - [Global Violations](/docs/MANUAL.md#global-violations)
            - [Toolbox](/docs/MANUAL.md#toolbox)
                - [File Tab](/docs/MANUAL.md#file-tab)
                - [Optimiser Tab (experimental)](/docs/MANUAL.md#optimiser-tab-experimental)
                - [Manual Tab](/docs/MANUAL.md#manual-tab)
    - [For Lecturers](/docs/MANUAL.md#for-lecturers)
    - [For Students](/docs/MANUAL.md#for-students)
    - [Troubleshooting](/docs/MANUAL.md#troubleshooting)
        - [Logging In](/docs/MANUAL.md#logging-in)
        - [Viewing Logs](/docs/MANUAL.md#viewing-logs)
        - [Still Having Issues?](/docs/MANUAL.md#still-having-issues)
1. [Documentation for Developers](/docs/DOCUMENTATION.md) (dedicated page)
    - [Architecture](docs/DOCUMENTATION.md#architecture)
        - [Architecture - Database](/docs/DOCUMENTATION.md#architecture---database)
        - [Architecture - Reverse Proxy](/docs/DOCUMENTATION.md#architecture---reverse-proxy)
        - [Architecture - Application](/docs/DOCUMENTATION.md#architecture---application)
    - [CI / CD Pipeline](docs/DOCUMENTATION.md#ci--cd-pipeline)
    - [Application Structure](docs/DOCUMENTATION.md#application-structure)
    - [Manage View](docs/DOCUMENTATION.md#manage-view)
    - [Web Workers](docs/DOCUMENTATION.md#web-workers)
    - [Fake Data](docs/DOCUMENTATION.md#fake-data)
    - [Authentication](docs/DOCUMENTATION.md#authentication)
    - [Timetable Entries File Structure](docs/DOCUMENTATION.md#timetable-entries-file-structure)
    - [Clearing All Data](docs/DOCUMENTATION.md#clearing-all-data)
1. [Rejected Designs](docs/REJECTED_DESIGNS.md) (dedicated page)
1. [Testing](docs/TESTING.md) (dedicated page)
1. [Contact](#contact)
1. [License](#license)

## Installation

The recommended OS is **Ubuntu Server 20.04**. There is no limitation for other distributions as long as the
pre-requisites are met. The OS must be based on UNIX.

The pre-requisites for this software are:

- Docker v20.10.2+
- Docker Compose v1.27.4+

Download setup scripts and configuration files into a directory you intend to use as the server root. A reasonable
directory might be one of:

- `/sdm`
- `/home/<user>/sdm`

```bash
for file in "pre-requisites.sh" "docker-compose.yml" "nginx.conf" "cert.sh" "update.sh"; do
  echo "Downloading $file..."
  curl -fsSL https://gitlab.com/uni.hoffic.cz/student-demonstrator-manager/web-system/-/raw/master/ops/server/$file -o $file
done
chmod +x *.sh
```

Install pre-requisites either manually or using this script

```bash
./pre-requisites.sh
```

Edit `docker-compose.yml` and configure `MAILER_DSN` according to
the [Symfony Mailer documentation](https://symfony.com/doc/4.4/mailer.html#using-built-in-transports).

**Note:** If you decide to use Gmail, it may look something like `gmail+smtp://user_name@gmail.com:password@default`.
Using Gmail is preferred as its use is free of charge. We recommend using a dedicated gmail account as it is required to
change the Google (Gmail) account's security settings to allow logging in from insecure devices. Google will also block
the first login on a new IP address using an insecure method, so it's necessary to log in on that machine with that
account using a web browser first. Alternatively, you may skip the browser authentication by attempting to log in
multiple times and from a separate computer navigating to the Google account's security dashboard, selecting the login
activity and marking it as "Yes, this was me". This will also result in whitelisting the machine.

**Note:** The system fully supports OAuth 2.0 authentication for Microsoft accounts including those used at Swansea
University. Swansea University, however, decided not to allow this application to authenticate students or staff. If
this changes in the future, it only takes to amend the `docker-compose.yml` file. More guidance is available inside the
file itself.

Edit `docker-compose.yml` (same file as before) and configure `ADMIN_ACCOUNTS` to set yourself administrator privileges.

**Note:** This will only be required for the persons responsible for accepting student demonstrators and assigning
demonstrators to lab sessions. Regular lecturer should not be added here.

Edit `nginx.conf` and set `server_name` to the DNS domain name you intend to use.

Set the DNS record for your domain to point to the server.

Generate a certificate by running the script below and following the prompts. You'll be asked to create a TXT DNS record
to prove your domain ownership.

```bash
./cert.sh
```

Spin up the docker compose array:

```bash
docker-compose up -d
```

Create the database and migrate to the newest schema:

```bash
docker-compose exec app ash -c "php bin/console doctrine:database:create"
docker-compose exec app ash -c "php bin/console doctrine:migrations:migrate --no-interaction"
```

Navigate to your domain, you should see the project landing page.

![](docs/images/landing_page.png)

To verify the mailer has been set up correctly try logging in. You should receive an email within a few seconds. If the
email does not arrive within 1 minute, consider the mailer not set up correctly.

## Updating From Old Versions

To update to a new version of the system run the following script:

```bash
./update.sh
```

**Note:** The script will fetch the newest pre-built docker images for the application and restart the docker array.
Finally, any database migrations will be executed.

## Quick Start Guide

This guide will take you through the steps needed to set system up for production use and assumes that
the [installation](/README.md#installation) has been successful.

Navigate to the landing page and select "I am an administrator" from the options available.

![](docs/images/manual_admin_landing.png)

Enter your university email address and click confirm.

![](docs/images/manual_admin_login_1.png)

You will receive an email with a one-time code that you will enter below, then click confirm.

![](docs/images/manual_admin_login_2.png)

**Note:** If you do not receive the email or the login page shows an error, make sure the `MAILER_DSN` from the
[Installation](#installation) chapter is configured properly.

You should see the admin dashboard:

![](docs/images/manual_admin_dashboard.png)

The dashboard contains links to operations that need to be executed in order. You should follow these links in order
unless you are absolutely certain you know what you are doing.

The one thing you need to do in order to start collecting module configurations from lecturers and demonstrator
applications from students is to import timetable entries scraped from the college of science timetable. In order to
gather the timetable data you may use the
[Timetable Parser](https://gitlab.com/uni.hoffic.cz/student-demonstrator-manager/timetable-parser) that was built as a
part of this project or any other that conforms with
the [file structure](docs/DOCUMENTATION.md#timetable-entries-file-structure). You may even choose to compile a
spreadsheet in LibreOffice Calc or Microsoft Excel and export it as CSV.

Navigate to the timetable import page, for example directly from the dashboard.

![](docs/images/manual_admin_import_1.png)

Select the file containing timetable entries and click the "Upload" button.

**Note:** The uploading and processing can take from 10 seconds to a few minutes, depending on the performance of the
computer the system is running on.

![](docs/images/manual_admin_import_2.png)

After the import has finished you should see a page with modules and a green banner on top saying the import was
successful.

![](docs/images/manual_admin_import_3.png)

**Warning:** Now is the time to make sure the data imported is correct. It has happened before that the data on the
College of Science timetable was incorrect, for example with a typo in the module name. This is the last chance to
correct the data by [clearing all data in the system](docs/DOCUMENTATION.md#clearing-all-data) and importing new,
corrected data.

When you're confident the data is correct, you can share the links to the lecturer survey and the demonstrator
application form with lecturers and students respectively. The links to these forms are available on the dashboard.

![](docs/images/manual_admin_dashboard_invite_forms.png)

This is the end of the quick start guide. For further information refer to the [manual](docs/MANUAL.md).

## Contact

Petr Hoffmann - https://hoffic.dev/ - petr@hoffic.dev

## License

Distributed under GNU General Public License v3.0. See [LICENSE](LICENSE) for more information.
