set -e
set -x

docker-compose pull
docker-compose down
docker-compose up -d
docker-compose exec app ash -c "php bin/console doctrine:migrations:migrate --no-interaction"

set +x
echo "Script finished successfully"
