set -e
set -x

docker run -it --rm --name certbot \
  -v "$PWD/letsencrypt:/etc/letsencrypt" \
  certbot/certbot --manual --preferred-challenges dns certonly

cp letsencrypt/live/*/cert.pem ./
cp letsencrypt/live/*/privkey.pem ./

set +x
echo "Script finished successfully"
