set -e
set -x

apt update
apt upgrade -y

if ! command -v docker &>/dev/null; then
  apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
  apt update
  apt install -y docker-ce docker-ce-cli containerd.io
else
  echo "Docker is already installed."
fi

if ! command -v docker-compose &>/dev/null; then
  curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
else
  echo "Docker Compose is already installed."
fi
