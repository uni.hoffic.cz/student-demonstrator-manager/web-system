set -e;

cd /etc/ssl;
docker run -it --rm --name certbot -v "$PWD/letsencrypt:/etc/letsencrypt" certbot/certbot --manual --preferred-challenges dns certonly;
