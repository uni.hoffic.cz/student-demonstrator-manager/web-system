<?php
declare(strict_types=1);


namespace App\Enum;


use Elao\Enum\AutoDiscoveredValuesTrait;
use Elao\Enum\ReadableEnum;

class StudentYearEnum extends ReadableEnum
{
    const FOUNDATION = 0;
    const FIRST = 1;
    const SECOND = 2;
    const THIRD = 3;
    const MASTERS = 4;
    const POSTGRADUATE = 5;

    use AutoDiscoveredValuesTrait;

    public static function readables(): array
    {
        return [
            self::FOUNDATION => 'Foundation year / 0',
            self::FIRST => 'First year / 1',
            self::SECOND => 'Second year / 2',
            self::THIRD => 'Third year / 3',
            self::MASTERS => 'Master\'s / 4',
            self::POSTGRADUATE => 'Postgraduate\'s / 5+',
        ];
    }
}
