<?php
declare(strict_types=1);


namespace App\Enum;


use Elao\Enum\AutoDiscoveredValuesTrait;
use Elao\Enum\Enum;

class PostgraduateRequirementEnum extends Enum
{
    const NONE_REQUIRED = 'None required';
    const SOME_REQUIRED = 'Some required';
    const ALL_REQUIRED = 'All required';

    use AutoDiscoveredValuesTrait;
}
