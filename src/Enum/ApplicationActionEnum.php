<?php
declare(strict_types=1);


namespace App\Enum;


use Elao\Enum\AutoDiscoveredValuesTrait;
use Elao\Enum\Enum;
use Elao\Enum\ReadableEnum;

class ApplicationActionEnum extends ReadableEnum
{
    const PENDING = 'Pending';
    const ACCEPTED = 'Accepted';
    const REJECTED = 'Rejected';

    use AutoDiscoveredValuesTrait;

    public static function readables(): array
    {
        return array_combine(self::values(), self::values());
    }
}
