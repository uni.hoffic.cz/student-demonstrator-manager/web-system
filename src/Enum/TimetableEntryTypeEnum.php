<?php
declare(strict_types=1);


namespace App\Enum;


use Elao\Enum\AutoDiscoveredValuesTrait;
use Elao\Enum\ReadableEnum;

class TimetableEntryTypeEnum extends ReadableEnum
{
    const LAB = 'LAB';
    const LECTURE = 'LECTURE';
    const OTHER = 'OTHER';

    use AutoDiscoveredValuesTrait;

    public static function readables(): array
    {
        return array_combine(self::values(), self::values());
    }
}
