<?php
declare(strict_types=1);


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class StudentMarkRecord
 *
 * @package App\Entity
 * @ORM\Entity
 */
class StudentMarkRecord extends AbstractEntity
{
    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $studentNumber;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     */
    private $marks;

    /**
     * @return int|null
     */
    public function getStudentNumber(): ?int
    {
        return $this->studentNumber;
    }

    /**
     * @param int|null $studentNumber
     */
    public function setStudentNumber(?int $studentNumber): void
    {
        $this->studentNumber = $studentNumber;
    }

    /**
     * @return array|null
     */
    public function getMarks(): ?array
    {
        return $this->marks;
    }

    /**
     * @param array|null $marks
     */
    public function setMarks(?array $marks): void
    {
        $this->marks = $marks;
    }
}
