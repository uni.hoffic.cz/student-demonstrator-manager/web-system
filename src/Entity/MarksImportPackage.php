<?php
declare(strict_types=1);


namespace App\Entity;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class MarksImportPackage
{
    /**
     * @var UploadedFile|null
     */
    private $file;

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     */
    public function setFile(?UploadedFile $file): void
    {
        $this->file = $file;
    }
}
