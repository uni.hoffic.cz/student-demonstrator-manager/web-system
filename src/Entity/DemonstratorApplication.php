<?php
declare(strict_types=1);


namespace App\Entity;

use App\Enum\ApplicationActionEnum;
use App\Enum\StudentYearEnum;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;

/**
 * Class DemonstratorApplication
 *
 * @package App\Entity
 * @Entity
 */
class DemonstratorApplication extends AbstractEntity
{
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    public $fullName;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    public $studentNumber;

    /** @var bool|null */
    public $hasAppliedOfficially;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     */
    public $modulePreferences;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    public $universityEmailAddress;

    /**
     * @var StudentYearEnum|null
     * @ORM\Column(type="studentyear", nullable=true)
     */
    public $year;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     */
    public $postgraduateResearch;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     */
    public $visaStudent;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     */
    public $modulesTaken;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     */
    public $timeslotsAvailable;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    public $timeslotsUnavailableReasons;

    /**
     * @var bool|null
     */
    public $acknowledgement;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $dateCreated;

    /**
     * @var ApplicationActionEnum|null
     * @ORM\Column(type="applaction", nullable=true)
     */
    public $action;

    public function __construct()
    {
        $this->dateCreated = new DateTime();
        $this->action = ApplicationActionEnum::get(ApplicationActionEnum::PENDING);
    }
}
