<?php
declare(strict_types=1);


namespace App\Entity;


class Timeslot
{
    /** @var int|null */
    public $dayOfWeek;

    /** @var int|null */
    public $hourStart;

    /** @var bool|null */
    public $available;
}
