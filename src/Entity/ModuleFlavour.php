<?php
declare(strict_types=1);


namespace App\Entity;


use App\FormEntity\FlavourLecturerFormExtension;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ModuleFlavour
 *
 * @package App\Entity
 * @ORM\Entity
 */
class ModuleFlavour extends AbstractEntity
{
    use FlavourLecturerFormExtension;

    /**
     * @var Module|null
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="flavours")
     * @Groups({"prop", "module-flavour"})
     */
    private $module;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     * @Groups({"prop", "module-flavour"})
     */
    private $flavour;

    /**
     * @var array|null
     * @ORM\OneToMany(targetEntity="TimetableEntry", mappedBy="moduleFlavour")
     */
    private $timetableEntries;

    /**
     * @return Module|null
     */
    public function getModule(): ?Module
    {
        return $this->module;
    }

    /**
     * @param Module|null $module
     */
    public function setModule(?Module $module): void
    {
        $this->module = $module;
    }

    /**
     * @return string|null
     */
    public function getFlavour(): ?string
    {
        return $this->flavour;
    }

    /**
     * @param string|null $flavour
     */
    public function setFlavour(?string $flavour): void
    {
        $this->flavour = $flavour;
    }
}
