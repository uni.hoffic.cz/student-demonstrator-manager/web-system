<?php
declare(strict_types=1);


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;

/**
 * Class LoginToken
 *
 * @package App\Entity
 * @Entity
 */
class LoginToken
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $oneTimeCode;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * LoginToken constructor.
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
        $this->oneTimeCode = uuid_create(UUID_TYPE_RANDOM);
        $this->createdAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getOneTimeCode(): string
    {
        return $this->oneTimeCode;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}
