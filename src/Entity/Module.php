<?php
declare(strict_types=1);


namespace App\Entity;

use App\FormEntity\ModuleDemonstratorFormExtension;
use App\FormEntity\ModuleLecturerFormExtension;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Module
 *
 * @package App\Entity
 * @ORM\Entity
 */
class Module extends AbstractEntity
{
    use ModuleLecturerFormExtension;
    use ModuleDemonstratorFormExtension;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     * @Groups({"prop", "module-flavour"})
     */
    private $name;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @Groups({"prop", "module-flavour"})
     */
    private $year;

    /**
     * @var array|int
     * @ORM\OneToMany(targetEntity="ModuleFlavour", mappedBy="module")
     */
    private $flavours;

    /**
     * @var array|int
     * @ORM\OneToMany(targetEntity="ModuleConfiguration", mappedBy="module")
     * @Groups({"prop", "module-flavour"})
     */
    private $configurations;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getYear(): ?int
    {
        return $this->year;
    }

    /**
     * @param int|null $year
     */
    public function setYear(?int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return array|int
     */
    public function getConfigurations()
    {
        return $this->configurations;
    }

    /**
     * @return array|int
     */
    public function getFlavours()
    {
        return $this->flavours;
    }
}
