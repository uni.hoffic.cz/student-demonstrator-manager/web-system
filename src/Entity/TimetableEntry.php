<?php

declare(strict_types=1);

namespace App\Entity;

use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class TimetableEntry
 *
 * @package App\Entity
 * @ORM\Entity
 */
class TimetableEntry extends AbstractEntity
{
    /**
     * @var ModuleFlavour|null
     * @ORM\ManyToOne(targetEntity="ModuleFlavour", inversedBy="timetableEntries")
     * @Groups({"prop", "timetable-entry"})
     */
    private $moduleFlavour;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"prop", "timetable-entry"})
     */
    private $start;

    /**
     * @var DateInterval|null
     * @ORM\Column(type="dateinterval", nullable=true)
     * @Groups({"prop", "timetable-entry"})
     */
    private $duration;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"prop", "timetable-entry"})
     */
    private $type;

    /**
     * @var Collection|Demonstrator[]
     * @ORM\ManyToMany(targetEntity="Demonstrator", inversedBy="timetableEntries")
     * @ORM\JoinTable(name="demonstrator_allocations")
     */
    private $demonstrators;

    public function __construct()
    {
        $this->demonstrators = new ArrayCollection();
    }

    /**
     * @return ModuleFlavour|null
     */
    public function getModuleFlavour(): ?ModuleFlavour
    {
        return $this->moduleFlavour;
    }

    /**
     * @param ModuleFlavour|null $moduleFlavour
     */
    public function setModuleFlavour(?ModuleFlavour $moduleFlavour): void
    {
        $this->moduleFlavour = $moduleFlavour;
    }

    /**
     * @return DateTime|null
     */
    public function getStart(): ?DateTime
    {
        return $this->start;
    }

    /**
     * @param DateTime|null $start
     */
    public function setStart(?DateTime $start): void
    {
        $this->start = $start;
    }

    /**
     * @return DateInterval|null
     */
    public function getDuration(): ?DateInterval
    {
        return $this->duration;
    }

    /**
     * @param DateInterval|null $duration
     */
    public function setDuration(?DateInterval $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getDemonstrators(): Collection
    {
        return clone $this->demonstrators;
    }

    public function getDemonstratorsList(): string
    {
        $studentNumbers = array_map(function ($demonstrator) {
            return sprintf('%s (%s)', $demonstrator->getFullName(), $demonstrator->getStudentNumber());
        }, $this->demonstrators->toArray());

        return $this->demonstrators->count() . ': ' . implode(', ', $studentNumbers);
    }

    public function getDemonstratorsStudentNumbers(): string
    {
        $studentNumbers = array_map(function ($demonstrator) {
            return $demonstrator->getStudentNumber();
        }, $this->demonstrators->toArray());

        return implode(', ', $studentNumbers);
    }
}
