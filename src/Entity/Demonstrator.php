<?php
declare(strict_types=1);


namespace App\Entity;

use App\Enum\StudentYearEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Demonstrator
 *
 * @package App\Entity
 * @Entity
 */
class Demonstrator extends AbstractEntity
{
    /**
     * @var string|null
     * @ORM\Column(type="string")
     * @Groups({"prop"})
     */
    private $fullName;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @Groups({"prop"})
     */
    private $studentNumber;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"prop"})
     */
    private $modulePreferences;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"prop"})
     */
    private $universityEmailAddress;

    /**
     * @var StudentYearEnum|null
     * @ORM\Column(type="studentyear", nullable=true)
     * @Groups({"prop"})
     */
    private $year;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"prop"})
     */
    private $postgraduateResearch;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"prop"})
     */
    private $visaStudent;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"prop"})
     */
    private $modulesTaken;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"prop"})
     */
    private $timeslotsAvailable;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="TimetableEntry", mappedBy="demonstrators")
     */
    private $timetableEntries;

    /**
     * @var array|null
     * @Groups({"prop"})
     */
    private $marks;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default":"invalid"})
     */
    private $shareKey;

    public function __construct()
    {
        $this->timetableEntries = new ArrayCollection();
        $this->shareKey = uuid_create(UUID_TYPE_RANDOM);
    }

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string|null $fullName
     */
    public function setFullName(?string $fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @return int|null
     */
    public function getStudentNumber(): ?int
    {
        return $this->studentNumber;
    }

    /**
     * @param int|null $studentNumber
     */
    public function setStudentNumber(?int $studentNumber): void
    {
        $this->studentNumber = $studentNumber;
    }

    /**
     * @return array|null
     */
    public function getModulePreferences(): ?array
    {
        return $this->modulePreferences;
    }

    /**
     * @param array|null $modulePreferences
     */
    public function setModulePreferences(?array $modulePreferences): void
    {
        $this->modulePreferences = $modulePreferences;
    }

    /**
     * @return string|null
     */
    public function getUniversityEmailAddress(): ?string
    {
        return $this->universityEmailAddress;
    }

    /**
     * @param string|null $universityEmailAddress
     */
    public function setUniversityEmailAddress(?string $universityEmailAddress): void
    {
        $this->universityEmailAddress = $universityEmailAddress;
    }

    /**
     * @return StudentYearEnum|null
     */
    public function getYear(): ?StudentYearEnum
    {
        return $this->year;
    }

    /**
     * @param StudentYearEnum|null $year
     */
    public function setYear(?StudentYearEnum $year): void
    {
        $this->year = $year;
    }

    /**
     * @return bool|null
     */
    public function getPostgraduateResearch(): ?bool
    {
        return $this->postgraduateResearch;
    }

    /**
     * @param bool|null $postgraduateResearch
     */
    public function setPostgraduateResearch(?bool $postgraduateResearch): void
    {
        $this->postgraduateResearch = $postgraduateResearch;
    }

    public function getPostgraduateResearchAsString(): string
    {
        return $this->postgraduateResearch ? 'TRUE' : 'FALSE';
    }

    /**
     * @return bool|null
     */
    public function getVisaStudent(): ?bool
    {
        return $this->visaStudent;
    }

    /**
     * @param bool|null $visaStudent
     */
    public function setVisaStudent(?bool $visaStudent): void
    {
        $this->visaStudent = $visaStudent;
    }

    public function getVisaStudentAsString(): string
    {
        return $this->visaStudent ? 'TRUE' : 'FALSE';
    }

    /**
     * @return array|null
     */
    public function getModulesTaken(): ?array
    {
        return $this->modulesTaken;
    }

    /**
     * @param array|null $modulesTaken
     */
    public function setModulesTaken(?array $modulesTaken): void
    {
        $this->modulesTaken = $modulesTaken;
    }

    /**
     * @return array|null
     */
    public function getTimeslotsAvailable(): ?array
    {
        return $this->timeslotsAvailable;
    }

    /**
     * @param array|null $timeslotsAvailable
     */
    public function setTimeslotsAvailable(?array $timeslotsAvailable): void
    {
        $this->timeslotsAvailable = $timeslotsAvailable;
    }

    public function getTimetableEntries(): Collection
    {
        return clone $this->timetableEntries;
    }

    public function getTimetableEntriesSummary(): array
    {
        $modules = [];

        /** @var TimetableEntry $lab */
        foreach ($this->timetableEntries as $lab) {
            $name = $lab->getModuleFlavour()->getModule()->getName();
            if (!isset($modules[$name])) {
                $modules[$name] = 0;
            }
            $modules[$name]++;
        }

        return $modules;
    }

    public function getTimetableEntriesSummaryAsString(): string
    {
        $summary = $this->getTimetableEntriesSummary();
        return implode(
            ', ',
            array_map(function ($key) use ($summary) {
                return $key . ': ' . $summary[$key];
            }, array_keys($summary))
        );
    }

    /**
     * @return array|null
     */
    public function getMarks(): ?array
    {
        return $this->marks;
    }

    /**
     * @param array|null $marks
     */
    public function setMarks(?array $marks): void
    {
        $this->marks = $marks;
    }

    /**
     * @return string
     */
    public function getShareKey(): string
    {
        return $this->shareKey;
    }
}
