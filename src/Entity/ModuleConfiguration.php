<?php
declare(strict_types=1);


namespace App\Entity;


use App\Enum\PostgraduateRequirementEnum;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ModuleConfiguration
 *
 * @package App\Entity
 * @Entity
 */
class ModuleConfiguration extends AbstractEntity
{
    /**
     * @var Module|null
     * @ORM\ManyToOne(targetEntity="Module")
     */
    private $module;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     * @Groups({"prop", "module-flavour"})
     */
    private $lecturerName;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     * @Groups({"prop", "module-flavour"})
     */
    private $lecturerEmail;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"prop", "module-flavour"})
     */
    private $numberOfStudents;

    /**
     * @var array|null
     * @ORM\Column(type="array")
     * @Groups({"prop", "module-flavour"})
     */
    private $flavourStudentNumbers;

    /**
     * @var array|null
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"prop", "module-flavour"})
     */
    private $studentsApproached;

    /**
     * @var PostgraduateRequirementEnum|null
     * @ORM\Column(type="pgreq", nullable=true)
     * @Groups({"prop", "module-flavour"})
     */
    private $ratioOfPostgraduates;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"prop", "module-flavour"})
     */
    private $notes;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $dateCreated;

    public function __construct()
    {
        $this->dateCreated = new DateTime();
    }

    /**
     * @return Module|null
     */
    public function getModule(): ?Module
    {
        return $this->module;
    }

    /**
     * @param Module|null $module
     */
    public function setModule(?Module $module): void
    {
        $this->module = $module;
    }

    /**
     * @return string|null
     */
    public function getLecturerName(): ?string
    {
        return $this->lecturerName;
    }

    /**
     * @param string|null $lecturerName
     */
    public function setLecturerName(?string $lecturerName): void
    {
        $this->lecturerName = $lecturerName;
    }

    /**
     * @return string|null
     */
    public function getLecturerEmail(): ?string
    {
        return $this->lecturerEmail;
    }

    /**
     * @param string|null $lecturerEmail
     */
    public function setLecturerEmail(?string $lecturerEmail): void
    {
        $this->lecturerEmail = $lecturerEmail;
    }

    /**
     * @return int|null
     */
    public function getNumberOfStudents(): ?int
    {
        return $this->numberOfStudents;
    }

    /**
     * @param int|null $numberOfStudents
     */
    public function setNumberOfStudents(?int $numberOfStudents): void
    {
        $this->numberOfStudents = $numberOfStudents;
    }

    /**
     * @return array|null
     */
    public function getFlavourStudentNumbers(): ?array
    {
        return $this->flavourStudentNumbers;
    }

    /**
     * @param array|null $flavourStudentNumbers
     */
    public function setFlavourStudentNumbers(?array $flavourStudentNumbers): void
    {
        $this->flavourStudentNumbers = $flavourStudentNumbers;
    }

    /**
     * @return array|null
     */
    public function getStudentsApproached(): ?array
    {
        return $this->studentsApproached;
    }

    /**
     * @param array|null $studentsApproached
     */
    public function setStudentsApproached(?array $studentsApproached): void
    {
        $this->studentsApproached = $studentsApproached;
    }

    /**
     * @return PostgraduateRequirementEnum|null
     */
    public function getRatioOfPostgraduates(): ?PostgraduateRequirementEnum
    {
        return $this->ratioOfPostgraduates;
    }

    /**
     * @param PostgraduateRequirementEnum|null $ratioOfPostgraduates
     */
    public function setRatioOfPostgraduates(?PostgraduateRequirementEnum $ratioOfPostgraduates): void
    {
        $this->ratioOfPostgraduates = $ratioOfPostgraduates;
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string|null $notes
     */
    public function setNotes(?string $notes): void
    {
        $this->notes = $notes;
    }

    public function __toString()
    {
        return sprintf('%s submitted by %s on %s', $this->module->getName(), $this->lecturerName, $this->dateCreated->format('Y-m-d H:i:s'));
    }
}
