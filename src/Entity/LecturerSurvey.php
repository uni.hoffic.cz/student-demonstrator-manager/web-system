<?php
declare(strict_types=1);


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;

class LecturerSurvey
{
    /**
     * @var string|null
     */
    private $fullName;

    /**
     * @var string|null
     */
    private $email;

    /** @var array|ArrayCollection */
    private $modules;

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string|null $fullName
     */
    public function setFullName(?string $fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return array|ArrayCollection
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @param array|ArrayCollection $modules
     */
    public function setModules($modules): void
    {
        $this->modules = $modules;
    }
}
