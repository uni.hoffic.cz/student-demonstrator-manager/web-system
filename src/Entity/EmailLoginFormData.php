<?php
declare(strict_types=1);


namespace App\Entity;


class EmailLoginFormData
{
    /** @var string|null */
    public $email;

    /** @var string|null */
    public $oneTimeCode;

    public function isComplete()
    {
        return !empty($this->email) && !empty($this->oneTimeCode);
    }
}
