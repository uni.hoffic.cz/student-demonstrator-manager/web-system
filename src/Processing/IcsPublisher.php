<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\TimetableEntry;
use Doctrine\Common\Collections\Collection;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Symfony\Component\HttpFoundation\RequestStack;

class IcsPublisher
{
    /** @var RequestStack */
    private $requestStack;

    /**
     * IcsPublisher constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function createIcsFeed(Collection $labs): string
    {
        $prodId = $this->requestStack->getCurrentRequest()->getHost();
        $calendar = new Calendar($prodId);

        /** @var TimetableEntry $lab */
        foreach ($labs as $lab) {
            $event = new Event($prodId . $lab->getId());
            $event->setDtStart($lab->getStart());
            $event->setDuration($lab->getDuration());
            $event->setSummary($lab->getModuleFlavour()->getFlavour());
            $calendar->addComponent($event);
        }

        return $calendar->render();
    }
}
