<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\Module;
use App\Entity\ModuleFlavour;
use App\Repository\ModuleFlavourRepository;
use App\Repository\ModuleRepository;
use App\Repository\TimetableEntryRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use League\Csv\Exception;
use League\Csv\Reader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TimetableImporter
{
    /** @var ModuleRepository */
    private $moduleRepository;

    /** @var TimetableEntryRepository */
    private $timetableEntryRepository;

    /** @var ModuleFlavourRepository */
    private $moduleFlavourRepository;

    /**
     * TimetableImporter constructor.
     *
     * @param ModuleRepository $moduleRepository
     * @param ModuleFlavourRepository $moduleFlavourRepository
     * @param TimetableEntryRepository $timetableEntryRepository
     */
    public function __construct(
        ModuleRepository $moduleRepository,
        ModuleFlavourRepository $moduleFlavourRepository,
        TimetableEntryRepository $timetableEntryRepository
    )
    {
        $this->moduleRepository = $moduleRepository;
        $this->moduleFlavourRepository = $moduleFlavourRepository;
        $this->timetableEntryRepository = $timetableEntryRepository;
    }

    /**
     * @param UploadedFile $file
     *
     * @throws Exception
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function importTimetable(UploadedFile $file)
    {
        $reader = Reader::createFromPath($file->getPathname(), 'r');
        $reader->setHeaderOffset(0);

        $counter = 0;
        foreach ($reader->getRecords() as $record) {
            $this->importRecord($record);
            if ($counter <= 0) {
                $counter = 16;
                $this->timetableEntryRepository->flush();
            } else {
                $counter--;
            }
        }
        $this->timetableEntryRepository->flush();
    }

    /** @var array|Module[] */
    private $moduleCache = [];

    /** @var array|ModuleFlavour[] */
    private $moduleFlavourCache = [];

    /**
     * @param array $record
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    private function importRecord(array $record)
    {
        $moduleName = $this->extractModuleName($record['TITLE']);
        if (isset($this->moduleCache[$moduleName])) {
            $module = $this->moduleCache[$moduleName];
        } else {
            $module = $this->moduleRepository->findOrCreateByName(
                $moduleName,
                $this->extractModuleYear($record['TITLE']));
            $this->moduleCache[$moduleName] = $module;
        }

        $moduleFlavourName = $record['TITLE'];
        if (isset($this->moduleFlavourCache[$moduleFlavourName])) {
            $moduleFlavour = $this->moduleFlavourCache[$moduleFlavourName];
        } else {
            $moduleFlavour = $this->moduleFlavourRepository->findOrCreateByFlavour(
                $moduleFlavourName,
                $module);
        }

        $this->timetableEntryRepository->createByModuleAndStart(
            $moduleFlavour,
            DateTime::createFromFormat(
                'Y-m-d\TH:i',
                $record['START']),
            new DateInterval($record['DURATION']),
            $record['TYPE']);
    }

    /**
     * @param string $moduleFlavour
     *
     * @return string
     */
    private function extractModuleName(string $moduleFlavour): ?string
    {
        $firstWord = explode(' ', $moduleFlavour)[0];

        if ($firstWord === 'Year') {
            return $moduleFlavour;
        } else {
            return $firstWord;
        }
    }

    /**
     * @param string $module
     *
     * @return int
     */
    private function extractModuleYear(string $module): int
    {
        $yearCharacter = strpbrk($module, '01234M')[0];

        if ($yearCharacter === false) {
            return -1;
        } elseif ($yearCharacter === 'M') {
            $yearCharacter = 4;
        } else {
            $yearCharacter = intval($yearCharacter);
        }

        return $yearCharacter;
    }
}
