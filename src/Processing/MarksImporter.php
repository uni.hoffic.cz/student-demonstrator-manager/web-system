<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\StudentMarkRecord;
use App\Repository\StudentMarkRecordRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use League\Csv\Reader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MarksImporter
{
    /** @var StudentMarkRecordRepository */
    private $studentMarkRecordRepository;

    /**
     * MarksImporter constructor.
     *
     * @param StudentMarkRecordRepository $studentMarkRecordRepository
     */
    public function __construct(StudentMarkRecordRepository $studentMarkRecordRepository)
    {
        $this->studentMarkRecordRepository = $studentMarkRecordRepository;
    }

    public function importMarks(UploadedFile $file)
    {
        $reader = Reader::createFromPath($file->getPathname(), 'r');
        $reader->setHeaderOffset(0);

        foreach ($reader->getRecords() as $record) {
            $this->importMarksRow($record);
        }
    }

    /**
     * @param array $record
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function importMarksRow(array $record)
    {
        $markRecord = new StudentMarkRecord();
        $markRecord->setStudentNumber(intval($record['Student Number']));
        $markRecord->setMarks($this->extractMarks($record));
        $this->studentMarkRecordRepository->upsert($markRecord);
    }

    private function extractMarks(array $record): array
    {
        $marks = [];

        foreach (array_keys($record) as $key) {
            if (substr($key, 0, 2) === 'CS' && is_numeric($record[$key])) {
                $marks[$key] = intval($record[$key]);
            }
        }

        return $marks;
    }
}
