<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\Demonstrator;
use App\Entity\ModuleFlavour;
use App\Entity\TimetableEntry;

class SlotPacker
{
    /**
     * @param array|TimetableEntry[] $timetableEntries
     *
     * @return array
     */
    public function packTimetableEntries(array $timetableEntries): array
    {
        $pack = [];

        foreach ($timetableEntries as $entry) {
            $dateKey = $entry->getStart()->format('Y-m-d');

            if (!isset($pack[$dateKey])) {
                $pack[$dateKey] = [];
            }

            $pack[$dateKey][] = $entry;
        }

        return $pack;
    }

    /**
     * @param array|ModuleFlavour[] $moduleFlavours
     *
     * @return array
     */
    public function packModuleFlavours(array $moduleFlavours): array
    {
        $pack = [];

        foreach ($moduleFlavours as $moduleFlavour) {
            $pack[$moduleFlavour->getId()] = $moduleFlavour;
        }

        return $pack;
    }

    /**
     * @param array|Demonstrator[] $demonstrators
     *
     * @return array
     */
    public function packDemonstrators(array $demonstrators)
    {
        $pack = [];

        foreach ($demonstrators as $demonstrator) {
            $pack[$demonstrator->getId()] = $demonstrator;
        }

        return $pack;
    }
}
