<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\Demonstrator;
use App\Repository\StudentMarkRecordRepository;

class DemonstratorMarksInjector
{
    /** @var StudentMarkRecordRepository */
    private $studentMarkRecordRepository;

    /**
     * DemonstratorMarksInjector constructor.
     *
     * @param StudentMarkRecordRepository $studentMarkRecordRepository
     */
    public function __construct(StudentMarkRecordRepository $studentMarkRecordRepository)
    {
        $this->studentMarkRecordRepository = $studentMarkRecordRepository;
    }

    /**
     * @param array|Demonstrator[] $demonstrators
     *
     * @return array
     */
    public function inject(array $demonstrators): array
    {
        foreach ($demonstrators as &$demonstrator) {
            $demonstrator->setMarks(
                $this->studentMarkRecordRepository->findStudentMarks(
                    $demonstrator->getStudentNumber()));
        }
        unset($demonstrator);

        return $demonstrators;
    }
}
