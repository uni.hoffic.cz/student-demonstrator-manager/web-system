<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\DemonstratorApplication;
use App\Entity\Module;
use App\Entity\Timeslot;
use App\Repository\DemonstratorApplicationRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class DemonstratorApplicationProcessor
{
    /**
     * @var DemonstratorApplicationRepository
     */
    private $demonstratorApplicationRepository;

    /**
     * DemonstratorApplicationProcessor constructor.
     *
     * @param DemonstratorApplicationRepository $demonstratorApplicationRepository
     */
    public function __construct(DemonstratorApplicationRepository $demonstratorApplicationRepository)
    {
        $this->demonstratorApplicationRepository = $demonstratorApplicationRepository;
    }

    /**
     * @param DemonstratorApplication $demonstratorApplication
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function process(DemonstratorApplication $demonstratorApplication)
    {
        $modulePreferences = [];
        /** @var Module $module */
        foreach ($demonstratorApplication->modulePreferences as $module) {
            if ($module->getYear() < $demonstratorApplication->year->getValue()) {
                $modulePreferences[$module->getName()] = $module->ranking;
            }
        }
        $demonstratorApplication->modulePreferences = $modulePreferences;

        $modulesTaken = [];
        /** @var Module $module */
        foreach ($demonstratorApplication->modulesTaken as $module) {
            if ($module->taken) {
                $modulesTaken[] = $module->getName();
            }
        }
        $demonstratorApplication->modulesTaken = $modulesTaken;

        $timeslotsAvailable = [];
        /** @var Timeslot $timeslot */
        foreach ($demonstratorApplication->timeslotsAvailable as $timeslot) {
            if ($timeslot->available) {
                $timeslotsAvailable[] = $timeslot->dayOfWeek . '-' . $timeslot->hourStart;
            }
        }
        $demonstratorApplication->timeslotsAvailable = $timeslotsAvailable;

        $this->demonstratorApplicationRepository->add($demonstratorApplication);
    }
}
