<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\Demonstrator;
use App\Entity\DemonstratorApplication;
use App\Enum\ApplicationActionEnum;
use App\Repository\DemonstratorApplicationRepository;
use App\Repository\DemonstratorRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use RuntimeException;

class DemonstratorApprovalProcessor
{
    /** @var DemonstratorApplicationRepository */
    private $demonstratorApplicationRepository;

    /** @var DemonstratorRepository */
    private $demonstratorRepository;

    /**
     * DemonstratorApprovalProcessor constructor.
     *
     * @param DemonstratorApplicationRepository $demonstratorApplicationRepository
     * @param DemonstratorRepository $demonstratorRepository
     */
    public function __construct(
        DemonstratorApplicationRepository $demonstratorApplicationRepository,
        DemonstratorRepository $demonstratorRepository
    )
    {
        $this->demonstratorApplicationRepository = $demonstratorApplicationRepository;
        $this->demonstratorRepository = $demonstratorRepository;
    }

    /**
     * @param DemonstratorApplication $application
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RuntimeException
     */
    public function approve(DemonstratorApplication $application)
    {
        $existing = $this->demonstratorRepository->findOneBy([
            'universityEmailAddress' => $application->universityEmailAddress,
        ]);
        if (!is_null($existing)) {
            throw new RuntimeException(sprintf('A demonstrator with email %s already exists.', $application->universityEmailAddress));
        }

        $demonstrator = new Demonstrator();
        $demonstrator->setFullName($application->fullName);
        $demonstrator->setUniversityEmailAddress($application->universityEmailAddress);
        $demonstrator->setStudentNumber($application->studentNumber);
        $demonstrator->setYear($application->year);
        $demonstrator->setVisaStudent($application->visaStudent);
        $demonstrator->setPostgraduateResearch($application->postgraduateResearch);
        $demonstrator->setModulePreferences($application->modulePreferences);
        $demonstrator->setModulesTaken($application->modulesTaken);
        $demonstrator->setTimeslotsAvailable($application->timeslotsAvailable);

        $this->demonstratorRepository->add($demonstrator);
        $this->demonstratorApplicationRepository->updateApplicationAction(
            $application,
            ApplicationActionEnum::get(ApplicationActionEnum::ACCEPTED));
    }

    public function reject(DemonstratorApplication $application)
    {
        $this->demonstratorRepository->deleteDemonstratorIfExists($application->universityEmailAddress);

        $this->demonstratorApplicationRepository->updateApplicationAction(
            $application,
            ApplicationActionEnum::get(ApplicationActionEnum::REJECTED));
    }
}
