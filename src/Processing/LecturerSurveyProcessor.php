<?php
declare(strict_types=1);


namespace App\Processing;


use App\Entity\LecturerSurvey;
use App\Entity\Module;
use App\Entity\ModuleConfiguration;
use App\Entity\ModuleFlavour;
use App\Repository\ModuleConfigurationRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class LecturerSurveyProcessor
{
    /** @var ModuleConfigurationRepository */
    private $moduleConfigurationRepository;

    /**
     * LecturerSurveyProcessor constructor.
     *
     * @param ModuleConfigurationRepository $moduleConfigurationRepository
     */
    public function __construct(ModuleConfigurationRepository $moduleConfigurationRepository)
    {
        $this->moduleConfigurationRepository = $moduleConfigurationRepository;
    }

    /**
     * @param LecturerSurvey $lecturerSurvey
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function process(LecturerSurvey $lecturerSurvey): void
    {
        /** @var Module $module */
        foreach ($lecturerSurvey->getModules() as $module) {
            if ($module->selected) {
                $this->saveModuleConfiguration($module, $lecturerSurvey->getFullName(), $lecturerSurvey->getEmail());
            }
        }
    }

    /**
     * @param Module $module
     *
     * @param string $lecturerName
     * @param string $lecturerEmail
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function saveModuleConfiguration(Module $module, string $lecturerName, string $lecturerEmail)
    {
        $moduleConfiguration = new ModuleConfiguration();

        $moduleConfiguration->setModule($module);
        $moduleConfiguration->setLecturerName($lecturerName);
        $moduleConfiguration->setLecturerEmail($lecturerEmail);
        $moduleConfiguration->setNumberOfStudents($module->numberOfStudents);
        if (!empty($module->studentsApproached)) {
            $moduleConfiguration->setStudentsApproached(explode("\n", $module->studentsApproached));
        }
        $moduleConfiguration->setRatioOfPostgraduates($module->postgraduateStudentsRequired);
        $moduleConfiguration->setNotes($module->notes);
        $flavourStudentNumbers = [];
        /** @var ModuleFlavour $flavour */
        foreach ($module->getFlavours() as $flavour) {
            $flavourStudentNumbers[$flavour->getFlavour()] = $flavour->numberOfStudents;
        }
        $moduleConfiguration->setFlavourStudentNumbers($flavourStudentNumbers);

        $this->moduleConfigurationRepository->add($moduleConfiguration);
    }
}
