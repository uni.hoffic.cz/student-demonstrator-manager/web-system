<?php
declare(strict_types=1);


namespace App\Console;


use App\Entity\Demonstrator;
use App\Entity\DemonstratorApplication;
use App\Entity\Module;
use App\Entity\ModuleConfiguration;
use App\Entity\ModuleFlavour;
use App\Entity\StudentMarkRecord;
use App\Entity\TimetableEntry;
use App\Enum\ApplicationActionEnum;
use App\Enum\PostgraduateRequirementEnum;
use App\Enum\StudentYearEnum;
use App\Enum\TimetableEntryTypeEnum;
use App\Processing\DemonstratorApprovalProcessor;
use App\Repository\DemonstratorApplicationRepository;
use App\Repository\DemonstratorRepository;
use App\Repository\ModuleConfigurationRepository;
use App\Repository\ModuleRepository;
use App\Repository\StudentMarkRecordRepository;
use App\Repository\TimetableEntryRepository;
use Elao\Enum\Bridge\Faker\Provider\EnumProvider;
use Faker\Factory;
use Faker\Generator;
use League\Csv\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class FakeDataCommand extends Command
{
    /** @var Generator */
    private $faker;

    /** @var EnumProvider */
    private $enumProvider;

    /** @var ModuleRepository */
    private $moduleRepository;

    /** @var DemonstratorApplicationRepository */
    private $demonstratorApplicationRepository;

    /** @var DemonstratorApprovalProcessor */
    private $demonstratorApprovalProcessor;

    /** @var DemonstratorRepository */
    private $demonstratorRepository;

    /** @var StudentMarkRecordRepository */
    private $studentMarkRecordRepository;

    /** @var ModuleConfigurationRepository */
    private $moduleConfigurationRepository;

    /** @var TimetableEntryRepository */
    private $timetableEntryRepository;

    /** @var ClearDataCommand */
    private $clearDataCommand;

    /** @var KernelInterface */
    private $kernel;

    /** @var array */
    private $randomStudentsPool = [];

    /**
     * FakeDataCommand constructor.
     *
     * @param ModuleRepository $moduleRepository
     * @param DemonstratorApplicationRepository $demonstratorApplicationRepository
     * @param DemonstratorApprovalProcessor $demonstratorApprovalProcessor
     * @param DemonstratorRepository $demonstratorRepository
     * @param StudentMarkRecordRepository $studentMarkRecordRepository
     * @param ModuleConfigurationRepository $moduleConfigurationRepository
     * @param TimetableEntryRepository $timetableEntryRepository
     * @param ClearDataCommand $clearDataCommand
     * @param KernelInterface $kernel
     */
    public function __construct(
        ModuleRepository $moduleRepository,
        DemonstratorApplicationRepository $demonstratorApplicationRepository,
        DemonstratorApprovalProcessor $demonstratorApprovalProcessor,
        DemonstratorRepository $demonstratorRepository,
        StudentMarkRecordRepository $studentMarkRecordRepository,
        ModuleConfigurationRepository $moduleConfigurationRepository,
        TimetableEntryRepository $timetableEntryRepository,
        ClearDataCommand $clearDataCommand,
        KernelInterface $kernel
    )
    {
        parent::__construct('sdm:data:fake');

        $this->addOption('assignments');
        $this->addOption('force');

        $this->faker = Factory::create();
        $this->enumProvider = new EnumProvider([
            'studentRatio' => PostgraduateRequirementEnum::class,
        ]);

        $this->moduleRepository = $moduleRepository;
        $this->demonstratorApplicationRepository = $demonstratorApplicationRepository;
        $this->demonstratorApprovalProcessor = $demonstratorApprovalProcessor;
        $this->demonstratorRepository = $demonstratorRepository;
        $this->studentMarkRecordRepository = $studentMarkRecordRepository;
        $this->moduleConfigurationRepository = $moduleConfigurationRepository;
        $this->timetableEntryRepository = $timetableEntryRepository;
        $this->clearDataCommand = $clearDataCommand;
        $this->kernel = $kernel;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->kernel->getEnvironment() == 'prod' && !$input->getOption('force')) {
            $output->writeln('This command cannot be run in production.');
            return 1;
        }

        if ($input->getOption('assignments')) {
            $output->writeln('Clearing demonstrator assignments...');
            $this->timetableEntryRepository->clearAssignments();

            $output->writeln('Faking demonstrator assignments...');
            $this->fakeAssignments();

        } else {
            $output->writeln('Clearing demonstratorApplications...');
            $this->demonstratorApplicationRepository->createQueryBuilder('a')->delete()->getQuery()->execute();

            $output->writeln('Clearing demonstrators...');
            $this->demonstratorRepository->createQueryBuilder('d')->delete()->getQuery()->execute();

            $output->writeln('Clearing module configurations...');
            $this->moduleConfigurationRepository->createQueryBuilder('c')->delete()->getQuery()->execute();

            $output->writeln('Clearing marks...');
            $this->studentMarkRecordRepository->createQueryBuilder('m')->delete()->getQuery()->execute();

            $output->writeln('Faking demonstrator applications...');
            $this->fakeDemonstratorApplications();

            $output->writeln('Accepting demonstrators...');
            $this->acceptDemonstratorApplications();

            $output->writeln('Faking module configurations...');
            $this->fakeModuleConfigurations();
        }

        $output->writeln('<fg=green>Done!</>');
        return 0;
    }

    private function fakeDemonstratorApplications()
    {
        $reader = Reader::createFromPath($this->kernel->getProjectDir() . '/assets/sim/demonstrator_applicants.csv', 'r');
        $reader->setHeaderOffset(0);

        foreach ($reader->getRecords() as $record) {
            switch ($record['Year']) {
                case 'PGR':
                    $year = StudentYearEnum::POSTGRADUATE;
                    break;
                case 'M':
                    $year = StudentYearEnum::MASTERS;
                    break;
                default:
                    $year = $record['CS-230'] == 'Enrolled'
                        ? StudentYearEnum::SECOND
                        : StudentYearEnum::THIRD;
                    break;
            }
            $year = StudentYearEnum::get($year);

            $application = new DemonstratorApplication();

            $application->year = $year;

            [$name, $email, $studentNumber] = $this->generateRandomStudent($year->getValue() == StudentYearEnum::POSTGRADUATE);
            $application->fullName = $name;
            $application->universityEmailAddress = $email;
            $application->studentNumber = $studentNumber;

            $application->postgraduateResearch = !empty($record['Weekly Limit']);

            $application->visaStudent = random_int(0, 9) === 0;

            $application->timeslotsAvailable = [];
            for ($day = 1; $day <= 5; $day++) {
                for ($slot = 9; $slot <= 17; $slot++) {
                    if (random_int(0, 7) > 0) {
                        $application->timeslotsAvailable[] = $day . '-' . $slot;
                    }
                }
            }
            $application->timeslotsUnavailableReasons = $this->faker->text(75);

            $application->modulePreferences = [];
            /** @var Module $module */
            foreach ($this->moduleRepository->listModulesWithLabs() as $module) {
                $application->modulePreferences[$module->getName()] = random_int(1, 5);
            }

            $application->modulesTaken = [];
            $marks = [];
            foreach ($record as $column => $value) {
                if ($value === 'Enrolled') {
                    $application->modulesTaken[] = $column;
                } elseif (ctype_digit($value)) {
                    $marks[$column] = $value;
                }
            }
            if (!empty($marks)) {
                $marksRecord = new StudentMarkRecord();
                $marksRecord->setStudentNumber($studentNumber);
                $marksRecord->setMarks($marks);
                $this->studentMarkRecordRepository->upsert($marksRecord);
            }

            $application->dateCreated = $this->faker->dateTimeThisMonth;

            $this->demonstratorApplicationRepository->add($application);
        }
    }

    private function fakeModuleConfigurations()
    {
        /** @var Module $module */
        foreach ($this->moduleRepository->listModulesWithLabs() as $module) {
            $moduleConfiguration = new ModuleConfiguration();

            $moduleConfiguration->setModule($module);

            [$name, $email] = $this->generateRandomLecturer();
            $moduleConfiguration->setLecturerName($name);
            $moduleConfiguration->setLecturerEmail($email);

            $moduleConfiguration->setNumberOfStudents(random_int(10, 400));
            $flavourStudentNumbers = [];
            /** @var ModuleFlavour $flavour */
            foreach ($module->getFlavours() as $flavour) {
                $flavourStudentNumbers[$flavour->getFlavour()] = intval($moduleConfiguration->getNumberOfStudents() / count($module->getFlavours()));
            }
            $moduleConfiguration->setFlavourStudentNumbers($flavourStudentNumbers);

            $approachedStudentsCount = random_int(-10, 2);
            if ($approachedStudentsCount > 0) {
                $students = [];
                for ($i = 0; $i < $approachedStudentsCount; $i++) {
                    $students[] = $this->randomStudentsPool[array_rand($this->randomStudentsPool)];
                }
                $moduleConfiguration->setStudentsApproached($students);
            }

            if (random_int(0, 1) === 0) {
                $moduleConfiguration->setRatioOfPostgraduates($this->enumProvider->randomEnum('studentRatio'));
            } else {
                $moduleConfiguration->setRatioOfPostgraduates(PostgraduateRequirementEnum::get(PostgraduateRequirementEnum::NONE_REQUIRED));
            }

            if (random_int(0, 3) === 0) {
                $moduleConfiguration->setNotes($this->faker->text('200'));
            }

            $moduleConfiguration->dateCreated = $this->faker->dateTimeThisMonth;

            $this->moduleConfigurationRepository->add($moduleConfiguration);
        }
    }

    private function generateRandomLecturer(): array
    {
        $name = $this->faker->name;
        $email = preg_replace('/[^A-z]/', '', strtolower($name)) . '@swansea.ac.uk';

        return [$name, $email];
    }

    private function generateRandomStudent(?bool $pg = null): array
    {
        $name = $this->faker->name;
        $studentNumber = random_int(100000, 999999);
        if ($pg === true || (is_null($pg) && random_int(0, 1) === 0)) {
            $email = preg_replace('/[^A-z]/', '', strtolower($name)) . '@swansea.ac.uk';
        } else {
            $email = $studentNumber . '@swansea.ac.uk';
        }

        $this->randomStudentsPool[] = $email;

        return [$name, $email, $studentNumber];
    }

    private function fakeAssignments()
    {
        /** @var array|Demonstrator[] $demonstratorPool */
        $demonstratorPool = $this->demonstratorRepository->findAll();

        $assignments = [];

        /** @var TimetableEntry $timetableEntry */
        foreach ($this->timetableEntryRepository->findBy(['type' => TimetableEntryTypeEnum::LAB]) as $timetableEntry) {
            $assignmentRow = [];

            $indexes = array_rand($demonstratorPool, random_int(2, 7));
            foreach ($indexes as $index) {
                $assignmentRow[] = $demonstratorPool[$index]->getId();
            }

            $assignments[$timetableEntry->getId()] = $assignmentRow;
        }

        $this->timetableEntryRepository->setAssignments($assignments);
    }

    private function acceptDemonstratorApplications()
    {
        $pendingApplications = $this->demonstratorApplicationRepository->findBy([
            'action' => ApplicationActionEnum::get(ApplicationActionEnum::PENDING),
        ]);
        foreach ($pendingApplications as $application) {
            $this->demonstratorApprovalProcessor->approve($application);
        }
    }
}
