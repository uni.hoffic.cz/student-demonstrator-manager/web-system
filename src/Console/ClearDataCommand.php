<?php
declare(strict_types=1);


namespace App\Console;


use App\Entity\Module;
use App\Entity\ModuleConfiguration;
use App\Entity\ModuleFlavour;
use App\Entity\TimetableEntry;
use App\Repository\ModuleFlavourRepository;
use App\Repository\ModuleRepository;
use App\Repository\TimetableEntryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class ClearDataCommand extends Command
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var KernelInterface */
    private $kernel;

    /**
     * ClearDataCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param KernelInterface $kernel
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        KernelInterface $kernel
    )
    {
        parent::__construct('sdm:data:clear');

        $this->addOption('force');

        $this->entityManager = $entityManager;
        $this->kernel = $kernel;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->kernel->getEnvironment() == 'prod' && !$input->getOption('force')) {
            $output->writeln('This command cannot be run in production.');
            return 1;
        }

        $output->writeln('Clearing all data...');
        $entities = [
            ModuleConfiguration::class,
            TimetableEntry::class,
            ModuleFlavour::class,
            Module::class,
        ];
        foreach ($entities as $entity) {
            $this->entityManager->createQueryBuilder()->delete($entity, 'e')->getQuery()->execute();
        }
        $output->writeln('Data cleared successfully.');
    }
}
