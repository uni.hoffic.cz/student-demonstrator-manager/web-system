<?php
declare(strict_types=1);


namespace App\Util;


use App\Entity\LoginToken;
use DateInterval;
use DateTime;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class DateUtil
{
    /** @var ContainerBagInterface */
    private $params;

    /**
     * DateUtil constructor.
     *
     * @param ContainerBagInterface $params
     */
    public function __construct(ContainerBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param DateTime $dateTime
     * @param string $interval
     *
     * @return DateTime
     */
    public function addInterval(DateTime $dateTime, string $interval): DateTime
    {
        $_interval = new DateInterval($interval);
        $_date = clone $dateTime;
        $_date->add($_interval);

        return $_date;
    }

    public function getExpiry(LoginToken $token): DateTime
    {
        return $this->addInterval($token->getCreatedAt(), $this->params->get('email_otc_expiry'));
    }

    public function isExpired(LoginToken $token): bool
    {
        return $this->getExpiry($token) < new DateTime();
    }
}
