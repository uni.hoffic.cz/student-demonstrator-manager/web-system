<?php
declare(strict_types=1);


namespace App\Controller;


use App\Entity\LecturerSurvey;
use App\Form\LecturerSurveyFormType;
use App\Processing\LecturerSurveyProcessor;
use App\Repository\ModuleRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class LecturerSurveyController extends AbstractController
{
    /**
     * @Route(path="/lecturer-survey", name="lecturer-survey")
     * @param Request $request
     * @param ModuleRepository $moduleRepository
     *
     * @param LecturerSurveyProcessor $lecturerSurveyProcessor
     *
     * @param Security $security
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(
        Request $request,
        ModuleRepository $moduleRepository,
        LecturerSurveyProcessor $lecturerSurveyProcessor,
        Security $security
    )
    {
        $lecturerSurvey = new LecturerSurvey();
        $lecturerSurvey->setEmail($security->getUser()->getUsername());
        $lecturerSurvey->setModules($moduleRepository->listModulesWithLabs());

        $form = $this->createForm(LecturerSurveyFormType::class, $lecturerSurvey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lecturerSurveyProcessor->process($form->getData());
            $this->addFlash('success', 'Lecturer survey submitted successfully.');
            return $this->redirectToRoute('index');

        } else {
            return $this->render('page/lecturer_survey.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }
}
