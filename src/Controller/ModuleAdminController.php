<?php
declare(strict_types=1);


namespace App\Controller;


use App\Entity\ModuleImportPackage;
use App\Form\ModuleImportFormType;
use App\Processing\TimetableImporter;
use Psr\Log\LoggerInterface;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModuleAdminController extends CRUDController
{
    /**
     * @var TimetableImporter
     */
    private $timetableImporter;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ModuleAdminController constructor.
     *
     * @param TimetableImporter $timetableImporter
     */
    public function __construct(TimetableImporter $timetableImporter, LoggerInterface $logger)
    {
        $this->timetableImporter = $timetableImporter;
        $this->logger = $logger;
    }

    public function importAction(Request $request): Response
    {
        $form = $this->createForm(ModuleImportFormType::class, new ModuleImportPackage());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            set_time_limit(300);

            try {
                $this->timetableImporter->importTimetable($form->getData()->getFile());
                $this->addFlash('success', 'Successfully imported.');
            } catch (\Throwable $e) {
                $this->logger->critical($e->getMessage(), $e->getTrace());
                $this->addFlash('error', 'An error has occurred during import.');
            }

            return $this->redirectToList();

        } else {
            return $this->renderWithExtraParams(
                'admin/action/module/import.html.twig',
                [
                    'form' => $form->createView(),
                ]
            );
        }
    }
}
