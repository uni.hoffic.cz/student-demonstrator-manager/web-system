<?php
declare(strict_types=1);


namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class MicrosoftController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/microsoft", name="connect_microsoft_start")
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        // will redirect to Microsoft!
        return $clientRegistry
            ->getClient('microsoft') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect(
                [
                    '.default',
                ],
                []
            );
    }
}
