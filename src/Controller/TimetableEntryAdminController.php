<?php
declare(strict_types=1);


namespace App\Controller;


use App\Encoding\EntitySerializer;
use App\Processing\DemonstratorMarksInjector;
use App\Processing\SlotPacker;
use App\Repository\DemonstratorRepository;
use App\Repository\ModuleFlavourRepository;
use App\Repository\ModuleRepository;
use App\Repository\TimetableEntryRepository;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

class TimetableEntryAdminController extends CRUDController
{
    /** @var TimetableEntryRepository */
    private $timetableEntryRepository;

    /** @var ModuleFlavourRepository */
    private $moduleFlavourRepository;

    /** @var ModuleRepository */
    private $moduleRepository;

    /** @var DemonstratorRepository */
    private $demonstratorRepository;

    /** @var EntitySerializer */
    private $entitySerializer;

    /** @var SlotPacker */
    private $slotPacker;

    /** @var DemonstratorMarksInjector */
    private $demonstratorMarksInjector;

    /** @var KernelInterface */
    private $kernel;

    /**
     * TimetableEntryAdminController constructor.
     *
     * @param TimetableEntryRepository $timetableEntryRepository
     * @param ModuleFlavourRepository $moduleFlavourRepository
     * @param DemonstratorRepository $demonstratorRepository
     * @param EntitySerializer $entitySerializer
     * @param SlotPacker $slotPacker
     * @param DemonstratorMarksInjector $demonstratorMarksInjector
     * @param KernelInterface $kernel
     */
    public function __construct(
        TimetableEntryRepository $timetableEntryRepository,
        ModuleFlavourRepository $moduleFlavourRepository,
        ModuleRepository $moduleRepository,
        DemonstratorRepository $demonstratorRepository,
        EntitySerializer $entitySerializer,
        SlotPacker $slotPacker,
        DemonstratorMarksInjector $demonstratorMarksInjector,
        KernelInterface $kernel
    )
    {
        $this->timetableEntryRepository = $timetableEntryRepository;
        $this->moduleFlavourRepository = $moduleFlavourRepository;
        $this->moduleRepository = $moduleRepository;
        $this->demonstratorRepository = $demonstratorRepository;
        $this->entitySerializer = $entitySerializer;
        $this->slotPacker = $slotPacker;
        $this->demonstratorMarksInjector = $demonstratorMarksInjector;
        $this->kernel = $kernel;
    }

    public function manageAction(): Response
    {
        if (count($this->moduleRepository->listLabModulesWithoutConfiguration()) > 0) {
            $this->addFlash('danger', 'All modules must have a configuration before proceeding.');
            return $this->redirectToRoute('admin_app_moduleconfiguration_list');

        } else {
            return $this->renderWithExtraParams(
                'admin/action/timetable_entry/manage.html.twig',
                [
                    'timetableEntries' => $this->entitySerializer->serialize(
                        $this->slotPacker->packTimetableEntries(
                            $this->timetableEntryRepository->findAll()),
                        'timetable-entry'),
                    'moduleFlavours' => $this->entitySerializer->serialize(
                        $this->slotPacker->packModuleFlavours(
                            $this->moduleFlavourRepository->findAllFullTree()),
                        'module-flavour'),
                    'demonstrators' => $this->entitySerializer->serialize(
                        $this->slotPacker->packDemonstrators(
                            $this->demonstratorMarksInjector->inject(
                                $this->demonstratorRepository->findAll())),
                        'prop'),
                    'assignments' => json_encode($this->timetableEntryRepository->getAssignments()),
                    'debug' => $this->kernel->isDebug(),
                ]
            );
        }
    }

    public function saveAssignmentsAction(Request $request)
    {
        $assignments = json_decode($request->getContent(), true);
        $this->timetableEntryRepository->clearAssignments();
        $this->timetableEntryRepository->setAssignments($assignments);
        return new Response('Saved successfully!');
    }
}
