<?php
declare(strict_types=1);


namespace App\Controller\Security;


use App\Entity\EmailLoginFormData;
use App\Form\EmailLoginFormType;
use App\Security\EmailAuthenticator;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;

class EmailLoginController extends AbstractController
{
    /**
     * @Route(path="/login/email", name="login_email")
     * @param Request $request
     *
     * @param EmailAuthenticator $emailAuthenticator
     *
     * @return Response
     */
    public function __invoke(Request $request, EmailAuthenticator $emailAuthenticator)
    {
        $emailLoginData = new EmailLoginFormData();
        $form = $this->createForm(EmailLoginFormType::class, $emailLoginData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $emailLoginData->isComplete()) {
            try {
                return $emailAuthenticator->authenticate($request, $emailLoginData);
            } catch (LogicException $exception) {
                $this->addFlash('danger', $exception->getMessage());
            }
        }

        if ($form->isSubmitted() && $form->isValid() && !$emailLoginData->isComplete()) {
            try {
                $emailAuthenticator->sendCode($emailLoginData);
            } catch (LogicException $exception) {
                $this->addFlash('danger', $exception->getMessage());
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('danger', 'The mailer is misconfigured. If you are using Gmail, you must log in in a browser using the same IP address as the server has.');
            }
        }

        return $this->render('page/email_login.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
