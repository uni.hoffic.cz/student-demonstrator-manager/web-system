<?php
declare(strict_types=1);


namespace App\Controller\Security;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AuthenticationGateway extends AbstractController
{
    /**
     * @Route(path="/login/gateway", name="login_gateway")
     * @param UrlGeneratorInterface $urlGenerator
     * @param ContainerBagInterface $params
     *
     * @return Response
     */
    public function __invoke(UrlGeneratorInterface $urlGenerator, ContainerBagInterface $params)
    {
        $route = $params->get('authentication_method') === 'email' ? 'login_email' : 'connect_microsoft_start';

        return new RedirectResponse(
            $urlGenerator->generate($route),
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}
