<?php
declare(strict_types=1);


namespace App\Controller;


use App\Entity\DemonstratorApplication;
use App\Entity\Timeslot;
use App\Form\DemonstratorApplicationFormType;
use App\Processing\DemonstratorApplicationProcessor;
use App\Repository\ModuleRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class DemonstratorApplicationController extends AbstractController
{
    /**
     * @Route(path="/demonstrator-application", name="demonstrator-application")
     * @param Request $request
     *
     * @param ModuleRepository $moduleRepository
     *
     * @param DemonstratorApplicationProcessor $demonstratorApplicationProcessor
     *
     * @param Security $security
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __invoke(
        Request $request,
        ModuleRepository $moduleRepository,
        DemonstratorApplicationProcessor $demonstratorApplicationProcessor,
        Security $security
    )
    {
        $demonstratorApplication = new DemonstratorApplication();

        $demonstratorApplication->universityEmailAddress = $security->getUser()->getUsername();
        $demonstratorApplication->modulePreferences = $moduleRepository->listModulesWithLabs();
        $demonstratorApplication->modulesTaken = $moduleRepository->listAllModules();
        $demonstratorApplication->timeslotsAvailable = $this->generateTimeslots();

        $form = $this->createForm(DemonstratorApplicationFormType::class, $demonstratorApplication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $demonstratorApplicationProcessor->process($form->getData());
            $this->addFlash('success', 'Application submitted successfully.');
            return $this->redirectToRoute('index');

        } else {
            return $this->render('page/demonstrator_application.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }

    /**
     * @return array
     */
    private function generateTimeslots(): array
    {
        $timeslots = [];

        for ($dayOfWeek = 1; $dayOfWeek <= 5; $dayOfWeek++) {
            for ($hourStart = 9; $hourStart <= 17; $hourStart++) {
                $timeslot = new Timeslot();
                $timeslot->dayOfWeek = $dayOfWeek;
                $timeslot->hourStart = $hourStart;
                $timeslot->available = true;

                $timeslots[] = $timeslot;
            }
        }

        return $timeslots;
    }
}
