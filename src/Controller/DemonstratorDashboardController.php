<?php
declare(strict_types=1);


namespace App\Controller;


use App\Entity\Demonstrator;
use App\Processing\IcsPublisher;
use App\Repository\DemonstratorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;


class DemonstratorDashboardController extends AbstractController
{
    /** @var DemonstratorRepository */
    private $demonstratorRepository;

    /**
     * DemonstratorDashboardController constructor.
     *
     * @param DemonstratorRepository $demonstratorRepository
     */
    public function __construct(DemonstratorRepository $demonstratorRepository)
    {
        $this->demonstratorRepository = $demonstratorRepository;
    }

    /**
     * @Route(path="/demonstrator-dashboard", name="demonstrator-dashboard")
     * @param Security $security
     *
     * @return Response
     */
    function __invoke(Security $security)
    {
        $user = $security->getUser();

        /** @var Demonstrator|null $demonstrator */
        $demonstrator = $this->demonstratorRepository->findOneBy([
            'universityEmailAddress' => $user->getUsername(),
        ]);

        if (!is_null($demonstrator)) {
            $assignedLabs = $demonstrator->getTimetableEntries();
            $shareKey = $demonstrator->getShareKey();
        }

        return $this->render('page/demonstrator_dashboard.html.twig', [
            'assignedLabs' => $assignedLabs ?? [],
            'shareKey' => $shareKey ?? null,
        ]);
    }

    /**
     * @Route(path="/api/ics/{shareKey}.ics", name="api-demonstrator-calendar")
     * @param string $shareKey
     *
     * @param IcsPublisher $icsPublisher
     *
     * @return Response
     */
    function demonstratorCalendar(string $shareKey, IcsPublisher $icsPublisher)
    {
        /** @var Demonstrator|null $demonstrator */
        $demonstrator = $this->demonstratorRepository->findOneBy([
            'shareKey' => $shareKey,
        ]);

        if (is_null($demonstrator)) {
            throw new NotFoundHttpException('This calendar does not belong to any demonstrator.');
        }

        return new Response($icsPublisher->createIcsFeed($demonstrator->getTimetableEntries()));
    }
}
