<?php
declare(strict_types=1);


namespace App\Controller;


use App\Entity\MarksImportPackage;
use App\Form\MarksImportFormType;
use App\Processing\DemonstratorApprovalProcessor;
use App\Processing\MarksImporter;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\Request;

class DemonstratorApplicationAdminController extends CRUDController
{
    /**
     * @var DemonstratorApprovalProcessor
     */
    private $demonstratorApprovalProcessor;

    /** @var MarksImporter */
    private $marksImporter;

    /**
     * DemonstratorApplicationAdminController constructor.
     *
     * @param DemonstratorApprovalProcessor $demonstratorApprovalProcessor
     * @param MarksImporter $marksImporter
     */
    public function __construct(
        DemonstratorApprovalProcessor $demonstratorApprovalProcessor,
        MarksImporter $marksImporter
    )
    {
        $this->demonstratorApprovalProcessor = $demonstratorApprovalProcessor;
        $this->marksImporter = $marksImporter;
    }

    public function batchActionApprove(ProxyQueryInterface $query)
    {
        try {
            $applications = $query->execute();

            foreach ($applications as $application) {
                $this->demonstratorApprovalProcessor->approve($application);
            }

            $this->addFlash(
                'sonata_flash_success',
                'Demonstrators approved!'
            );
        } catch (\Exception $e) {
            $this->addFlash(
                'sonata_flash_error',
                'An error occurred: ' . $e->getMessage()
            );
        }

        return $this->redirectToList();
    }

    public function batchActionReject(ProxyQueryInterface $query)
    {
        try {
            $applications = $query->execute();

            foreach ($applications as $application) {
                $this->demonstratorApprovalProcessor->reject($application);
            }

            $this->addFlash(
                'sonata_flash_success',
                'Demonstrators rejected!'
            );
        } catch (\Exception $e) {
            $this->handleModelManagerException($e);
            $this->addFlash(
                'sonata_flash_error',
                'An error occurred: ' . $e->getMessage()
            );
        }

        return $this->redirectToList();
    }

    public function importMarksAction(Request $request)
    {
        $form = $this->createForm(MarksImportFormType::class, new MarksImportPackage());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                /** @var MarksImportPackage $data */
                $data = $form->getData();
                $this->marksImporter->importMarks($data->getFile());
                $this->addFlash('success', 'Successfully imported.');
            } catch (\Throwable $e) {
                $this->addFlash('error', 'An error has occurred during import: ' . $e->getMessage());
            }

            return $this->redirectToList();

        } else {
            return $this->renderWithExtraParams(
                'admin/action/demonstrator_application/import_marks.html.twig',
                [
                    'form' => $form->createView(),
                ]
            );
        }
    }
}
