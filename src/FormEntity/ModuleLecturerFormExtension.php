<?php


namespace App\FormEntity;


use App\Enum\PostgraduateRequirementEnum;

trait ModuleLecturerFormExtension
{
    /** @var bool|null */
    public $selected;

    /** @var int|null */
    public $numberOfStudents;

    /** @var string|null */
    public $studentsApproached;

    /** @var PostgraduateRequirementEnum|null */
    public $postgraduateStudentsRequired;

    /** @var string|null */
    public $notes;
}
