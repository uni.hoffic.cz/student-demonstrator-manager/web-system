<?php


namespace App\FormEntity;


trait ModuleDemonstratorFormExtension
{
    /** @var double|null */
    public $ranking;

    /** @var bool|null */
    public $taken;
}
