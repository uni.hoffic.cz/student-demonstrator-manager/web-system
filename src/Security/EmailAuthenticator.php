<?php
declare(strict_types=1);


namespace App\Security;


use App\Entity\EmailLoginFormData;
use App\Entity\LoginToken;
use App\Repository\LoginTokenRepository;
use App\Repository\UserRepository;
use App\Util\DateUtil;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use LogicException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Twig\Environment as Twig;

class EmailAuthenticator
{
    /** @var LoginTokenRepository */
    private $loginTokenRepository;

    /** @var SessionInterface */
    private $session;

    /** @var ContainerBagInterface */
    private $params;

    /** @var GuardAuthenticatorHandler */
    private $guardHandler;

    /** @var UserRepository */
    private $userRepository;

    /** @var MicrosoftAuthenticator */
    private $microsoftAuthenticator;

    /** @var UrlGeneratorInterface */
    private $urlGenerator;

    /** @var RequestStack */
    private $requestStack;

    /** @var MailerInterface */
    private $mailer;

    /** @var Twig */
    private $twig;

    /** @var DateUtil */
    private $dateUtil;

    /**
     * EmailAuthenticator constructor.
     *
     * @param LoginTokenRepository $loginTokenRepository
     * @param SessionInterface $session
     * @param ContainerBagInterface $params
     * @param GuardAuthenticatorHandler $guardHandler
     * @param UserRepository $userRepository
     * @param MicrosoftAuthenticator $microsoftAuthenticator
     * @param UrlGeneratorInterface $urlGenerator
     * @param RequestStack $requestStack
     * @param MailerInterface $mailer
     * @param Twig $twig
     * @param DateUtil $dateUtil
     */
    public function __construct(
        LoginTokenRepository $loginTokenRepository,
        SessionInterface $session,
        ContainerBagInterface $params,
        GuardAuthenticatorHandler $guardHandler,
        UserRepository $userRepository,
        MicrosoftAuthenticator $microsoftAuthenticator,
        UrlGeneratorInterface $urlGenerator,
        RequestStack $requestStack,
        MailerInterface $mailer,
        Twig $twig,
        DateUtil $dateUtil
    )
    {
        $this->loginTokenRepository = $loginTokenRepository;
        $this->session = $session;
        $this->params = $params;
        $this->guardHandler = $guardHandler;
        $this->userRepository = $userRepository;
        $this->microsoftAuthenticator = $microsoftAuthenticator;
        $this->urlGenerator = $urlGenerator;
        $this->requestStack = $requestStack;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->dateUtil = $dateUtil;
    }

    /**
     * @param EmailLoginFormData $data
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransportExceptionInterface
     */
    public function sendCode(EmailLoginFormData $data): void
    {
        /** @var LoginToken $existingToken */
        $existingToken = $this->loginTokenRepository->findOneBy(['email' => $data->email]);

        if (!is_null($existingToken) && !$this->dateUtil->isExpired($existingToken)) {
            throw new LogicException('A valid token for this email address already exists, it will not be sent again.');
        } else {
            $this->establishNewToken($data->email);
            $this->session->getFlashBag()->add('success', 'A one time code has been sent to the provided email address.');
        }
    }

    /**
     * @param string $email
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransportExceptionInterface
     */
    private function establishNewToken(string $email)
    {
        $loginToken = new LoginToken($email);
        $this->sendEmail($loginToken);
        $this->loginTokenRepository->upsert($loginToken);
    }

    /**
     * @param LoginToken $loginToken
     *
     * @throws TransportExceptionInterface
     */
    private function sendEmail(LoginToken $loginToken)
    {
        $email = new Email();
        $email->from('admin@' . $this->requestStack->getCurrentRequest()->getHost());
        $email->to($loginToken->getEmail());
        $email->subject('One Time Code - Student Demonstrator Manager');
        $email->html($this->twig->render(
            'email/one_time_code.html.twig',
            [
                'oneTimeCode' => $loginToken->getOneTimeCode(),
                'validUntil' => $this->dateUtil->getExpiry($loginToken),
            ]
        ));

        $this->mailer->send($email);
    }

    /**
     * @param Request $request
     * @param EmailLoginFormData $data
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function authenticate(Request $request, EmailLoginFormData $data): Response
    {
        /** @var LoginToken $token */
        $token = $this->loginTokenRepository->findOneBy(['email' => $data->email]);

        if (is_null($token) || $token->getOneTimeCode() !== $data->oneTimeCode) {
            throw new LogicException('The one time code is not valid.');
        } elseif ($this->dateUtil->isExpired($token)) {
            $this->establishNewToken($data->email);
            throw new LogicException('This one time code has expired, a new one has been sent to your email address.');
        } else {
            $this->loginTokenRepository->remove($token);
            $user = $this->userRepository->findOrCreateUserByEmail($data->email);
            return $this->guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $this->microsoftAuthenticator,
                'main');
        }
    }
}
