<?php
declare(strict_types=1);


namespace App\Security;


namespace App\Security;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Stevenmaguire\OAuth2\Client\Provider\MicrosoftResourceOwner;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class MicrosoftAuthenticator extends SocialAuthenticator
{
    /** @var ClientRegistry */
    private $clientRegistry;

    /** @var EntityManagerInterface */
    private $em;

    /** @var RouterInterface */
    private $router;

    /** @var UrlGeneratorInterface */
    private $urlGenerator;

    /** @var SessionInterface */
    private $session;

    /** @var UserRepository */
    private $userRepository;

    public function __construct(
        ClientRegistry $clientRegistry,
        EntityManagerInterface $em,
        RouterInterface $router,
        UrlGeneratorInterface $urlGenerator,
        SessionInterface $session,
        UserRepository $userRepository
    )
    {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
        $this->router = $router;
        $this->urlGenerator = $urlGenerator;
        $this->session = $session;
        $this->userRepository = $userRepository;
    }

    public function supports(Request $request)
    {
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === 'connect_microsoft_check';
    }

    public function getCredentials(Request $request)
    {
        // this method is only called if supports() returns true

        // For Symfony lower than 3.4 the supports method need to be called manually here:
        // if (!$this->supports($request)) {
        //     return null;
        // }

        return $this->fetchAccessToken($this->getMicrosoftClient());
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var MicrosoftResourceOwner $microsoftUser */
        $microsoftUser = $this->getMicrosoftClient()
            ->fetchUserFromToken($credentials);

        $email = $microsoftUser->getEmail();

        if (preg_match('/.+@swansea\.ac\.uk/i', $email) !== 1) {
            throw new CustomUserMessageAuthenticationException(sprintf(
                'You must log in using a @swansea.ac.uk account. %s does not meet this requirement. If you are being logged in automatically try an anonymous window.',
                $email
            ));
        }

        return $this->userRepository->findOrCreateUserByEmail($email);
    }

    /**
     * @return OAuth2ClientInterface
     */
    private function getMicrosoftClient()
    {
        return $this->clientRegistry->getClient('microsoft');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $targetKey = '_security.main.target_path';
        $targetUrl = $request->getSession()->get($targetKey);

        if (is_null($targetUrl)) {
            $targetUrl = $this->router->generate('index');
        } else {
            $request->getSession()->remove($targetKey);
        }

        return new RedirectResponse($targetUrl);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->session->getFlashBag()->add('danger', $exception->getMessage());

        return new RedirectResponse(
            $this->urlGenerator->generate('index'),
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     *
     * @param Request $request
     * @param AuthenticationException|null $authException
     *
     * @return RedirectResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse(
            $this->urlGenerator->generate('login_gateway'),
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}
