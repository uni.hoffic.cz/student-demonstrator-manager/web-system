<?php
declare(strict_types=1);


namespace App\Admin;


use App\Entity\Module;
use App\Entity\ModuleFlavour;
use App\Enum\TimetableEntryTypeEnum;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter;
use Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TimetableEntryAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'export']);
        $collection->add('manage', 'manage');
        $collection->add('saveAssignments', 'save-assignments');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        $list['manage']['template'] = 'admin/action_button/timetable_entry/manage.html.twig';

        return $list;
    }

    protected function configureListFields(ListMapper $list)
    {
        unset($this->listModes['mosaic']);

        $list->add('moduleFlavour', null, [
            'associated_property' => 'flavour',
        ]);
        $list->add('type', null);
        $list->add('start');
        $list->add('duration', null, [
            'template' => 'admin/list_field/timetable_entry/duration.html.twig',
        ]);
        $list->add('demonstrators', null, [
            'template' => 'admin/list_field/timetable_entry/demonstrators.html.twig',
            'label' => 'Demonstrators Assigned',
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('type', ChoiceFilter::class, [], ChoiceType::class, [
            'choices' => TimetableEntryTypeEnum::readables(),
        ]);
        $filter->add('moduleFlavour.module', ChoiceFilter::class, [
            'label' => 'Module',
        ], EntityType::class, [
            'class' => Module::class,
            'choice_label' => 'name',
        ]);
        $filter->add('moduleFlavour', ChoiceFilter::class, [], EntityType::class, [
            'class' => ModuleFlavour::class,
            'choice_label' => 'flavour',
        ]);
        $filter->add('start', DateRangeFilter::class, [], null, []);
    }

    protected function configureDefaultFilterValues(array &$filterValues)
    {
        $filterValues['type'] = ['value' => TimetableEntryTypeEnum::LAB];
    }

    protected function configureExportFields(): array
    {
        return [
            'moduleFlavour.module.name',
            'moduleFlavour.flavour',
            'type',
            'start',
            'duration',
            'demonstratorsList',
            'demonstratorsStudentNumbers',
        ];
    }
}
