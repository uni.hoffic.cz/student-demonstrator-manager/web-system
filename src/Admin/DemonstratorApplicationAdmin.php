<?php
declare(strict_types=1);


namespace App\Admin;


use App\Enum\ApplicationActionEnum;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter;
use Sonata\DoctrineORMAdminBundle\Filter\NumberFilter;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DemonstratorApplicationAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'batch']);
        $collection->add('import_marks', 'import-marks');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        $list['import_marks']['template'] = 'admin/action_button/demonstrator_application/import_marks.html.twig';

        return $list;
    }

    protected function configureBatchActions($actions)
    {
        $actions['approve'] = ['ask_confirmation' => true];
        $actions['reject'] = ['ask_confirmation' => true];

        return $actions;
    }

    protected function configureListFields(ListMapper $list)
    {
        unset($this->listModes['mosaic']);

        $list->add('fullName');
        $list->add('studentNumber');
        $list->add('year');
        $list->add('postgraduateResearch');
        $list->add('visaStudent');
        $list->add('universityEmailAddress');
        $list->add('timeslotsAvailable', null, [
            'template' => 'admin/list_field/demonstrator_application/timeslots_available.html.twig',
        ]);
        $list->add('timeslotsUnavailableReasons');
        $list->add('Marks', null, [
            'mapped' => false,
            'template' => 'admin/list_field/demonstrator_application/marks.html.twig',
        ]);
        $list->add('dateCreated');
        $list->add('action', null, [
            'template' => 'admin/list_field/-/enum.html.twig',
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('action', ChoiceFilter::class, [], ChoiceType::class, [
            'choices' => ApplicationActionEnum::readables(),
        ]);
        $filter->add('fullName', StringFilter::class);
        $filter->add('universityEmailAddress', StringFilter::class);
        $filter->add('studentNumber', NumberFilter::class);
    }

    protected function configureDefaultFilterValues(array &$filterValues)
    {
        $filterValues['action'] = ['value' => ApplicationActionEnum::PENDING];
    }
}
