<?php
declare(strict_types=1);


namespace App\Admin;


use App\Entity\Module;
use App\Enum\PostgraduateRequirementEnum;
use Elao\Enum\Bridge\Symfony\Form\Type\EnumType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ModuleConfigurationAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'edit', 'delete', 'batch', 'export']);
    }

    protected function configureListFields(ListMapper $list)
    {
        unset($this->listModes['mosaic']);

        $list->add('_action', null, [
            'actions' => [
                'edit' => [],
            ]
        ]);
        $list->add('module', null, [
            'associated_property' => 'name',
        ]);
        $list->add('lecturerName');
        $list->add('lecturerEmail');
        $list->add('numberOfStudents');
        $list->add('flavourStudentNumbers');
        $list->add('studentsApproached', null, [
            'template' => 'admin/list_field/module_configuration/students_approached.html.twig',
        ]);
        $list->add('ratioOfPostgraduates', null, [
            'template' => 'admin/list_field/-/enum.html.twig',
        ]);
        $list->add('notes', null, [

        ]);
        $list->add('dateCreated');
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('module', 'doctrine_orm_choice', [
            'field_type' => EntityType::class,
            'field_options' => [
                'class' => Module::class,
                'choice_label' => function (?Module $entity) {
                    return $entity ? $entity->getName() : '';
                },
            ],
            'show_filter' => true,
        ]);
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form->add('module', EntityType::class, [
            'class' => Module::class,
            'choice_label' => 'name',
            'disabled' => true,
        ]);
        $form->add('lecturerName', TextType::class);
        $form->add('lecturerEmail', EmailType::class);
        $form->add('numberOfStudents', IntegerType::class);
        $form->add('flavourStudentNumbers', TextareaType::class, [
            'attr' => ['rows' => 10],
        ]);
        $form->get('flavourStudentNumbers')
            ->addModelTransformer(new CallbackTransformer(
                function ($tagsAsArray) {
                    return json_encode($tagsAsArray, JSON_PRETTY_PRINT);
                },
                function ($tagsAsString) {
                    return json_decode($tagsAsString, true);
                }
            ));
        $form->add('studentsApproached', CollectionType::class, [
            'allow_add' => true,
            'allow_delete' => true,
            'entry_type' => TextType::class,
        ]);
        $form->add('ratioOfPostgraduates', EnumType::class, [
            'enum_class' => PostgraduateRequirementEnum::class,
        ]);
        $form->add('notes', TextareaType::class, [
            'required' => false,
        ]);
    }

    protected function configureExportFields(): array
    {
        return [
            'module.name',
            'lecturerName',
            'lecturerEmail',
            'numberOfStudents',
            'flavourStudentNumbers',
            'studentsApproached',
            'ratioOfPostgraduates.value',
            'notes',
            'dateCreated',
        ];
    }
}
