<?php
declare(strict_types=1);


namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\NumberFilter;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;

class DemonstratorAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'export']);
    }

    protected function configureListFields(ListMapper $list)
    {
        unset($this->listModes['mosaic']);

        $list->add('fullName');
        $list->add('studentNumber');
        $list->add('year');
        $list->add('visaStudent');
        $list->add('postgraduateResearch');
        $list->add('universityEmailAddress');
        $list->add('timetableEntriesSummary', null, [
            'template' => 'admin/list_field/demonstrator/timetable_entries.html.twig',
            'label' => 'Assigned Lab Sessions',
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('fullName', StringFilter::class);
        $filter->add('universityEmailAddress', StringFilter::class);
        $filter->add('studentNumber', NumberFilter::class);
    }

    protected function configureExportFields(): array
    {
        return [
            'fullName',
            'studentNumber',
            'year',
            'visaStudentAsString',
            'postgraduateResearchAsString',
            'universityEmailAddress',
            'timetableEntriesSummaryAsString',
        ];
    }
}
