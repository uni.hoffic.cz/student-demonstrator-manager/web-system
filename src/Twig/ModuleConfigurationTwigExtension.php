<?php
declare(strict_types=1);


namespace App\Twig;


use App\Repository\ModuleRepository;
use Twig\Extension\RuntimeExtensionInterface;

class ModuleConfigurationTwigExtension implements RuntimeExtensionInterface
{
    /** @var ModuleRepository */
    private $moduleRepository;

    /**
     * ModuleConfigurationTwigExtension constructor.
     *
     * @param ModuleRepository $moduleRepository
     */
    public function __construct(ModuleRepository $moduleRepository)
    {
        $this->moduleRepository = $moduleRepository;
    }

    public function getModulesWithoutConfig()
    {
        return $this->moduleRepository->listLabModulesWithoutConfiguration();
    }

    public function getModulesWithDuplicateConfig()
    {
        return $this->moduleRepository->listLabModulesWithDuplicateConfiguration();
    }
}
