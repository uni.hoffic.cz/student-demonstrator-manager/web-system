<?php
declare(strict_types=1);


namespace App\Twig;


use App\Entity\DemonstratorApplication;
use App\Repository\StudentMarkRecordRepository;
use Twig\Extension\RuntimeExtensionInterface;

class StudentMarkRecordExtension implements RuntimeExtensionInterface
{
    /** @var StudentMarkRecordRepository */
    private $studentMarkRecordRepository;

    /**
     * StudentMarkRecordExtension constructor.
     *
     * @param StudentMarkRecordRepository $studentMarkRecordRepository
     */
    public function __construct(StudentMarkRecordRepository $studentMarkRecordRepository)
    {
        $this->studentMarkRecordRepository = $studentMarkRecordRepository;
    }

    public function getStudentMarkRecord(DemonstratorApplication $application)
    {
        $result = $this->studentMarkRecordRepository->findBy(['studentNumber' => $application->studentNumber]);
        return array_shift($result);
    }
}
