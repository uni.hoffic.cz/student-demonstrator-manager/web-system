<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\DemonstratorApplication;
use App\Enum\ApplicationActionEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class DemonstratorApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DemonstratorApplication::class);
    }

    /**
     * @param DemonstratorApplication $demonstratorApplication
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(DemonstratorApplication $demonstratorApplication)
    {
        $this->getEntityManager()->persist($demonstratorApplication);
        $this->getEntityManager()->flush();
    }

    public function updateApplicationAction(DemonstratorApplication $application, ApplicationActionEnum $action)
    {
        $conn = $this->getEntityManager()->getConnection();
        $st = $conn->prepare(<<<'SQL'
update
    demonstrator_application
set
    action = :action
where
    id = :id
SQL
        );
        $st->bindValue('action', $action);
        $st->bindValue('id', $application->getId());
        $st->execute();
    }
}
