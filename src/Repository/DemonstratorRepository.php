<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\Demonstrator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class DemonstratorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Demonstrator::class);
    }

    /**
     * @param Demonstrator $demonstrator
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Demonstrator $demonstrator)
    {
        $this->getEntityManager()->persist($demonstrator);
        $this->getEntityManager()->flush();
    }

    public function deleteDemonstratorIfExists(string $universityEmailAddress)
    {
        $conn = $this->getEntityManager()->getConnection();
        $st = $conn->prepare(<<<'SQL'
delete
from
    demonstrator
where
    university_email_address = :email
SQL
        );
        $st->bindValue('email', $universityEmailAddress);
        $st->execute();
    }
}
