<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\Module;
use App\Enum\TimetableEntryTypeEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class ModuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Module::class);
    }

    /**
     * @param Module $module
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Module $module)
    {
        $this->getEntityManager()->persist($module);
        $this->getEntityManager()->flush();
    }

    /**
     * @param string $name
     *
     * @param int $year
     *
     * @return Module|object
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function findOrCreateByName(string $name, int $year): Module
    {
        $module = $this->findOneBy(['name' => $name]);

        if (is_null($module)) {
            $module = new Module();
            $module->setName($name);
            $module->setYear($year);
            $this->add($module);
        }

        return $module;
    }

    public function listModulesWithLabs()
    {
        $qb = $this->createQueryBuilder('m');

        $qb->innerJoin('m.flavours', 'f');
        $qb->innerJoin('f.timetableEntries', 't');
        $qb->andWhere($qb->expr()->eq('t.type', ':type'));
        $qb->setParameter('type', TimetableEntryTypeEnum::LAB);
        $qb->orderBy('m.name', 'ASC');

        return $qb->getQuery()->execute();
    }

    public function listAllModules()
    {
        $qb = $this->createQueryBuilder('m');
        $qb->orderBy('m.name', 'ASC');
        return $qb->getQuery()->execute();
    }

    public function listLabModulesWithoutConfiguration()
    {
        $conn = $this->getEntityManager()->getConnection();
        $st = $conn->prepare(<<<'SQL'
select distinct
    module.id, module.name
from
    module
    left join module_configuration as c on module.id = c.module_id
    inner join module_flavour as f on module.id = f.module_id
    inner join timetable_entry as t on f.id = t.module_flavour_id
where
    c is null
    and t.type = :type
order by
    module.name asc
SQL
        );
        $st->bindValue('type', TimetableEntryTypeEnum::LAB);
        $st->execute();
        return array_column($st->fetchAllAssociative(), 'name');
    }

    public function listLabModulesWithDuplicateConfiguration()
    {
        $conn = $this->getEntityManager()->getConnection();
        $st = $conn->prepare(<<<'SQL'
select distinct
    module.id, module.name
from
    module
    cross join module_configuration as c1
    cross join module_configuration as c2
    inner join module_flavour as f on module.id = f.module_id
    inner join timetable_entry as t on f.id = t.module_flavour_id
where
    module.id = c1.module_id
    and module.id = c2.module_id
    and c1.id <> c2.id
    and t.type = :type
order by
    module.name asc
SQL
        );
        $st->bindValue('type', TimetableEntryTypeEnum::LAB);
        $st->execute();
        return $st->fetchAllAssociative();
    }
}
