<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /** @var ContainerBagInterface */
    private $params;

    public function __construct(ManagerRegistry $registry, ContainerBagInterface $params)
    {
        parent::__construct($registry, User::class);

        $this->params = $params;
    }

    /**
     * @param string $email
     *
     * @return User
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function findOrCreateUserByEmail(string $email): User
    {
        $user = $this->findOneBy(['email' => $email]);

        if (is_null($user)) {
            $user = new User();
            $user->setEmail($email);
            $user->setRoles($this->calculateRoles($email));

            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush();
        }

        return $user;
    }

    private function calculateRoles(string $email): array
    {
        $admins = explode(',', $this->params->get('admin_accounts'));
        $roles = [];

        if (in_array($email, $admins)) {
            $roles[] = 'ROLE_ADMIN';
        } elseif ($this->isStudentEmail($email)) {
            $roles[] = 'ROLE_STUDENT';
        } else {
            $roles[] = 'ROLE_LECTURER';
        }

        return $roles;
    }

    private function isStudentEmail(string $email): bool
    {
        return preg_match('/\\d+@.+/', $email) === 1;
    }
}
