<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\LoginToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class LoginTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LoginToken::class);
    }

    /**
     * @param LoginToken $module
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function upsert(LoginToken $module)
    {
        if (empty($module->getEmail())) {
            throw new \RuntimeException('Email cannot be empty.');
        }

        $st = $this->getEntityManager()->getConnection()->prepare('DELETE FROM login_token WHERE email = :email');
        $st->bindValue('email', $module->getEmail());
        $st->execute();

        $this->getEntityManager()->persist($module);
        $this->getEntityManager()->flush();
    }

    /**
     * @param LoginToken $token
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(LoginToken $token)
    {
        $this->getEntityManager()->remove($token);
        $this->getEntityManager()->flush();
    }
}
