<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\ModuleConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class ModuleConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModuleConfiguration::class);
    }

    /**
     * @param ModuleConfiguration $module
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ModuleConfiguration $module)
    {
        $this->getEntityManager()->persist($module);
        $this->getEntityManager()->flush();
    }
}
