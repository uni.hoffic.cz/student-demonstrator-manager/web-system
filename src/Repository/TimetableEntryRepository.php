<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\Module;
use App\Entity\ModuleFlavour;
use App\Entity\TimetableEntry;
use App\Enum\TimetableEntryTypeEnum;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class TimetableEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimetableEntry::class);
    }

    /**
     * @param TimetableEntry $timetableEntry
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(TimetableEntry $timetableEntry)
    {
        $this->getEntityManager()->persist($timetableEntry);
        $this->getEntityManager()->flush();
    }

    public function findAllFullTree()
    {
        $qb = $this->createQueryBuilder('t');
        $qb->leftJoin('t.moduleFlavour', 'f');
        $qb->leftJoin('f.module', 'm');
        $qb->leftJoin('m.configurations', 'c');

        $qb->addSelect('f');
        $qb->addSelect('m');
        $qb->addSelect('c');

        return $qb->getQuery()->execute();
    }

    /**
     * @param ModuleFlavour $moduleFlavour
     * @param DateTime $start
     * @param DateInterval $duration
     *
     * @param string $timetableEntryType
     *
     * @return TimetableEntry
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function findOrCreateByModuleAndStart(
        ModuleFlavour $moduleFlavour,
        DateTime $start,
        DateInterval $duration,
        string $timetableEntryType
    ): TimetableEntry
    {
        $entry = $this->findOneBy([
            'moduleFlavour' => $moduleFlavour,
            'start' => $start,
        ]);

        if (is_null($entry)) {
            $entry = new TimetableEntry();
            $entry->setModuleFlavour($moduleFlavour);
            $entry->setStart($start);
            $entry->setDuration($duration);
            $entry->setType($timetableEntryType);

            $this->add($entry);
        }

        return $entry;
    }

    /**
     * @param ModuleFlavour $moduleFlavour
     * @param DateTime $start
     * @param DateInterval $duration
     * @param string $timetableEntryType
     */
    public function createByModuleAndStart(
        ModuleFlavour $moduleFlavour,
        DateTime $start,
        DateInterval $duration,
        string $timetableEntryType
    )
    {
        $entry = new TimetableEntry();
        $entry->setModuleFlavour($moduleFlavour);
        $entry->setStart($start);
        $entry->setDuration($duration);
        $entry->setType($timetableEntryType);

        $this->getEntityManager()->persist($entry);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    public function clearAssignments()
    {
        $this->getEntityManager()->getConnection()->prepare('delete from demonstrator_allocations')->execute();
    }

    public function getAssignments()
    {
        $st = $this->getEntityManager()->getConnection()->prepare('select * from demonstrator_allocations');
        $st->execute();
        $results = $st->fetchAllAssociative();

        $assignments = [];

        foreach ($results as $result) {
            $timetableEntryId = $result['timetable_entry_id'];
            $demonstratorId = $result['demonstrator_id'];

            if (!isset($assignments[$timetableEntryId])) {
                $assignments[$timetableEntryId] = [];
            }

            $assignments[$timetableEntryId][] = $demonstratorId;
        }

        return $assignments;
    }

    public function setAssignments(array $assignments)
    {
        $values = [];

        foreach ($assignments as $timetableEntryId => $assignmentRow) {
            foreach ($assignmentRow as $assignment) {
                $values[] = $timetableEntryId;
                $values[] = $assignment;
            }
        }

        $valueStatement = implode(',', array_fill(0, count($values) / 2, '(?,?)'));

        if (!empty($valueStatement)) {
            $st = $this->getEntityManager()->getConnection()->prepare('insert into demonstrator_allocations values ' . $valueStatement);

            for ($i = 0; $i < count($values); $i++) {
                $st->bindValue($i + 1, $values[$i]);
            }

            $st->execute();
        }
    }
}
