<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\Module;
use App\Entity\ModuleFlavour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class ModuleFlavourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModuleFlavour::class);
    }

    /**
     * @param ModuleFlavour $module
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ModuleFlavour $module)
    {
        $this->getEntityManager()->persist($module);
        $this->getEntityManager()->flush();
    }

    /**
     * @param string $flavour
     *
     * @param Module $module
     *
     * @return ModuleFlavour
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function findOrCreateByFlavour(string $flavour, Module $module): ModuleFlavour
    {
        $moduleFlavour = $this->findOneBy(['flavour' => $flavour]);

        if (is_null($moduleFlavour)) {
            $moduleFlavour = new ModuleFlavour();
            $moduleFlavour->setModule($module);
            $moduleFlavour->setFlavour($flavour);
            $this->add($moduleFlavour);
        }

        return $moduleFlavour;
    }

    public function findAllFullTree()
    {
        $qb = $this->createQueryBuilder('f');
        $qb->leftJoin('f.module', 'm');
        $qb->leftJoin('m.configurations', 'c');

        $qb->addSelect('m');
        $qb->addSelect('c');

        return $qb->getQuery()->execute();
    }
}
