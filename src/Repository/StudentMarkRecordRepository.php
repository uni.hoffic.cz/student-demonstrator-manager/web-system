<?php
declare(strict_types=1);


namespace App\Repository;

use App\Entity\StudentMarkRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class StudentMarkRecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentMarkRecord::class);
    }

    /**
     * @param StudentMarkRecord $markRecord
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    public function upsert(StudentMarkRecord $markRecord)
    {
        $st = $this->getEntityManager()->getConnection()->prepare('delete from student_mark_record where student_number = :student_number');
        $st->bindValue('student_number', $markRecord->getStudentNumber());
        $st->execute();

        $this->getEntityManager()->persist($markRecord);
        $this->getEntityManager()->flush();
    }

    public function findStudentMarks(int $studentNumber): ?array
    {
        /** @var StudentMarkRecord $record */
        $record = $this->findOneBy(['studentNumber' => $studentNumber]);

        if (!is_null($record)) {
            return $record->getMarks();
        } else {
            return null;
        }
    }
}
