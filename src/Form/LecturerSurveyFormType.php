<?php
declare(strict_types=1);


namespace App\Form;


use App\Entity\LecturerSurvey;
use App\Entity\Module;
use App\Entity\ModuleFlavour;
use App\Form\Constraint\FullNameConstraint;
use App\Form\Constraint\SwanseaEmailConstraint;
use App\Repository\ModuleRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class LecturerSurveyFormType extends AbstractType
{
    /** @var ModuleRepository */
    private $moduleRepository;

    /** @var SwanseaEmailConstraint */
    private $swanseaEmailConstraint;

    /**
     * LecturerSurveyFormType constructor.
     *
     * @param ModuleRepository $moduleRepository
     * @param SwanseaEmailConstraint $swanseaEmailConstraint
     */
    public function __construct(ModuleRepository $moduleRepository, SwanseaEmailConstraint $swanseaEmailConstraint)
    {
        $this->moduleRepository = $moduleRepository;
        $this->swanseaEmailConstraint = $swanseaEmailConstraint;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fullName', TextType::class, [
            'constraints' => [
                FullNameConstraint::i(),
            ],
        ]);

        $builder->add('email', EmailType::class, [
            'disabled' => true,
        ]);

        $builder->add('modules', CollectionType::class, [
            'entry_type' => LecturerSurveyModuleType::class,
            'entry_options' => ['label' => false],
            'constraints' => [
                new Callback([
                    'callback' => [static::class, 'validateModules'],
                    'payload' => $this->swanseaEmailConstraint,
                ]),
            ],
            'error_bubbling' => false,
        ]);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Submit',
            'attr' => [
                'class' => 'btn btn-primary',
            ],
        ]);
    }

    /**
     * @param array|null $object
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function validateModules(?array $object, ExecutionContextInterface $context, SwanseaEmailConstraint $payload)
    {
        $hasSelected = false;
        /** @var Module $module */
        foreach ($object as $module) {
            if ($module->selected) {
                $hasSelected = true;

                if (!is_null($module->studentsApproached)) {
                    foreach (explode("\n", $module->studentsApproached) as $studentEmail) {
                        if (!$payload->test($studentEmail)) {
                            $context->addViolation('Students approached must be university email addresses, one per line.');
                            break;
                        }
                    }
                }
            }
        }

        if (!$hasSelected) {
            $context->addViolation('You must select at least one module.');
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LecturerSurvey::class,
        ]);
    }
}
