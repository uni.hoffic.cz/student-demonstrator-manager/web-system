<?php
declare(strict_types=1);


namespace App\Form;


use App\Entity\DemonstratorApplication;
use App\Entity\Timeslot;
use App\Enum\StudentYearEnum;
use App\Form\Constraint\FullNameConstraint;
use Elao\Enum\Bridge\Symfony\Form\Type\EnumType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class DemonstratorApplicationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fullName', TextType::class, [
            'constraints' => [
                FullNameConstraint::i(),
            ],
            'help' => 'At least 2 words, each starting with a capital letter. If your name only has 1 word, repeat the word 2 times.',
        ]);
        $builder->add('studentNumber', IntegerType::class);
        $builder->add('universityEmailAddress', EmailType::class, [
            'disabled' => true,
        ]);
        $builder->add('visaStudent', ChoiceType::class, [
            'label' => 'Visa requirements',
            'choices' => [
                'I DO need a visa to study in the UK.' => true,
                'I do NOT need a visa to study in the UK.' => false,
            ],
        ]);
        $builder->add('year', EnumType::class, [
            'enum_class' => StudentYearEnum::class,
        ]);
        $builder->add('postgraduateResearch', ChoiceType::class, [
            'required' => false,
            'choices' => [
                'I am on a postgraduate RESEARCH course.' => true,
                'I am NOT on a postgraduate RESEARCH course.' => false,
            ],
        ]);
        $builder->add('hasAppliedOfficially', CheckboxType::class, [
            'label' => 'I confirm I have applied through the university\'s careers website before filling out this form.',
            'help' => 'If this is not the case you will not be allowed to start demonstrating.',
        ]);
        $builder->add('modulePreferences', CollectionType::class, [
            'entry_type' => DemonstratorApplicationModulePreferenceType::class,
            'entry_options' => ['label' => false],
        ]);
        $builder->add('modulesTaken', CollectionType::class, [
            'entry_type' => DemonstratorApplicationModulesTakenType::class,
            'entry_options' => ['label' => false],
        ]);
        $builder->add('timeslotsAvailable', CollectionType::class, [
            'entry_type' => TimeslotsType::class,
            'entry_options' => ['label' => false],
        ]);
        $builder->add('timeslotsUnavailableReasons', TextareaType::class, [
            'required' => false,
            'help' => 'For each timeslot you are unavailable to demonstrate on you need to provide a valid reason.',
            'attr' => [
                'rows' => 3,
                'cols' => 50,
            ],
            'constraints' => [
                new Callback([
                    'callback' => [DemonstratorApplicationFormType::class, 'validate'],
                ])
            ],
        ]);
        $builder->add('acknowledgement', CheckboxType::class, [
            'label' => 'I hereby declare that all information above is correct and impartial to the best of my knowledge.',
        ]);
        $builder->add('submit', SubmitType::class, [
            'label' => 'Submit Application',
            'attr' => [
                'class' => 'btn btn-primary',
            ],
        ]);
    }

    /**
     * @param string|null $object
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public static function validate(?string $object, ExecutionContextInterface $context, $payload)
    {
        $allTimeslotsAvailable = true;
        /** @var Timeslot $timeslot */
        foreach ($context->getObject()->getRoot()->getData()->timeslotsAvailable as $timeslot) {
            if (!$timeslot->available) {
                $allTimeslotsAvailable = false;
                break;
            }
        }

        if (!$allTimeslotsAvailable && empty($object)) {
            $context->addViolation('You must provide a reason for unavailable timeslots.');
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DemonstratorApplication::class,
        ]);
    }
}
