<?php
declare(strict_types=1);


namespace App\Form;


use App\Entity\Module;
use App\Entity\ModuleFlavour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class LecturerSurveyFlavourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numberOfStudents', IntegerType::class, [
            'required' => false,
            'label' => false,
            'attr' => ['min' => 0],
            'constraints' => [
                new Callback([
                    'callback' => [static::class, 'validateFlavour'],
                ]),
            ],
            'error_bubbling' => false,
        ]);
    }

    public static function validateFlavour($value, ExecutionContextInterface $context, $payload)
    {
        /** @var Module $module */
        $module = $context->getObject()->getParent()->getParent()->getParent()->getData();

        if ($module->selected && is_null($value)) {
            $context->addViolation('Required');
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ModuleFlavour::class,
        ]);
    }
}
