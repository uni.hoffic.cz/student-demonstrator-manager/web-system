<?php
declare(strict_types=1);


namespace App\Form;


use App\Entity\EmailLoginFormData;
use App\Form\Constraint\SwanseaEmailConstraint;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailLoginFormType extends AbstractType
{
    /** @var SwanseaEmailConstraint */
    private $swanseaEmailConstraint;

    /**
     * EmailLoginFormType constructor.
     *
     * @param SwanseaEmailConstraint $swanseaEmailConstraint
     */
    public function __construct(SwanseaEmailConstraint $swanseaEmailConstraint)
    {
        $this->swanseaEmailConstraint = $swanseaEmailConstraint;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'constraints' => [
                $this->swanseaEmailConstraint->i(),
            ],
        ]);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Confirm',
        ]);

        $builder->get('email')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $email = $event->getData();
                $form = $event->getForm()->getParent();

                if (!empty($email) && $this->swanseaEmailConstraint->test($email)) {

                    $form->add('oneTimeCode', PasswordType::class, [
                        'help' => 'You will receive it by an email.',
                    ]);
                }

                $form->remove('submit');
                $form->add('submit', SubmitType::class, [
                    'label' => 'Confirm',
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmailLoginFormData::class,
        ]);
    }
}
