<?php
declare(strict_types=1);


namespace App\Form\Constraint;


use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Validator\Constraints\Regex;

class SwanseaEmailConstraint
{
    /** @var string */
    private $pattern;

    /**
     * SwanseaEmailConstraint constructor.
     *
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->pattern = $kernel->getEnvironment() == 'dev'
            ? '/.+@.+/'
            : '/.+@swansea\.ac\.uk/';
    }

    public function i()
    {
        return new Regex([
            'pattern' => $this->pattern,
            'message' => 'You must use an email address ending with @swansea.ac.uk',
        ]);
    }

    public function test(string $email): bool
    {
        return preg_match($this->pattern, $email) === 1;
    }
}
