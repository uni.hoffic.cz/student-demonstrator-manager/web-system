<?php
declare(strict_types=1);


namespace App\Form\Constraint;


use Symfony\Component\Validator\Constraints\Regex;

class FullNameConstraint
{
    private const PATTERN = '/[A-ZÀ-Ÿ]\w*(?: [A-ZÀ-Ÿ]\w*)+/';

    public static function i()
    {
        return new Regex([
            'pattern' => self::PATTERN,
            'message' => 'Please enter your full name.',
        ]);
    }
}
