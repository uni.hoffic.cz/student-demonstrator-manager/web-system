<?php
declare(strict_types=1);


namespace App\Form;


use App\Entity\Module;
use App\Entity\ModuleFlavour;
use App\Enum\PostgraduateRequirementEnum;
use Elao\Enum\Bridge\Symfony\Form\Type\EnumType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class LecturerSurveyModuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('selected', CheckboxType::class, [
            'required' => false,
        ]);
        $builder->add('numberOfStudents', IntegerType::class, [
            'required' => false,
            'label' => 'Total expected number of students taking this module',
            'help' => 'If students belong exclusively to one flavour, this should be the sum of the flavours below.',
            'attr' => ['min' => 0],
            'constraints' => [
                new Callback([
                    'callback' => [static::class, 'validateNumberOfStudents'],
                ]),
            ],
            'error_bubbling' => false,
        ]);
        $builder->add('flavours', CollectionType::class, [
            'entry_type' => LecturerSurveyFlavourType::class,
            'help' => 'Note that a student may belong to a particular flavour AND a general flavour at the same time.',
        ]);
        $builder->add('studentsApproached', TextareaType::class, [
            'required' => false,
            'help' => 'One university email address per line. This field is optional, use it only if you have arranged this assignment yourself.'
        ]);
        $builder->add('postgraduateStudentsRequired', EnumType::class, [
            'enum_class' => PostgraduateRequirementEnum::class,
            'help' => '"Some required" means roughly 1 postgraduate per 1 undergraduate, but at least 1 postgraduate.',
        ]);
        $builder->add('notes', TextareaType::class, [
            'required' => false,
            'help' => 'Any notes or special requirements for the optimiser. This field will be read by a human.'
        ]);
    }

    public static function validateNumberOfStudents($value, ExecutionContextInterface $context, $payload) {
        /** @var Module $module */
        $module = $context->getObject()->getParent()->getData();

        if ($module->selected && is_null($value)) {
            $context->addViolation('Required');
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Module::class,
        ]);
    }
}
