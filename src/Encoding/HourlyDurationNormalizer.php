<?php
declare(strict_types=1);


namespace App\Encoding;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class HourlyDurationNormalizer implements NormalizerInterface
{
    /**
     * @param \DateInterval $object
     * @param null $format
     * @param array $context
     *
     * @return int
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return $object->h;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \DateInterval;
    }
}
