<?php
declare(strict_types=1);


namespace App\Encoding;


use Doctrine\Common\Annotations\AnnotationReader;
use Elao\Enum\Bridge\Symfony\Serializer\Normalizer\EnumNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateIntervalNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EntitySerializer
{
    /**
     * @param $object
     * @param string $group
     *
     * @return string
     */
    public function serialize($object, string $group): string
    {
        $encoders = [new JsonEncoder()];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizers = [
            new DateTimeNormalizer(),
            new HourlyDurationNormalizer(),
            new EnumNormalizer(),
            new ObjectNormalizer($classMetadataFactory),
        ];
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->serialize(
            $object,
            'json',
            [
                'groups' => $group,
            ]
        );
    }
}
