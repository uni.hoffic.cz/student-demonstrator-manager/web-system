import { RawScoreResultType } from "../types/score/RawScoreResultType";
import {
  ErrorDaysMapType,
  ErrorDemonstratorMapType,
  ErrorTimetableEntryMapType,
  ScoreBreakdownType
} from "../types/score/ScoreBreakdownType";

export const calculateScoreBreakdown = (rawScore: RawScoreResultType): ScoreBreakdownType => {
  const scoreBreakdown: ScoreBreakdownType = {
    errors: {
      entries: [],
      days: {},
      weeks: {}
    }
  };

  for (const error of rawScore.errors) {
    let errorTarget = scoreBreakdown.errors.entries;

    if (error.day !== undefined) {
      if (scoreBreakdown.errors.days[error.day] === undefined) {
        scoreBreakdown.errors.days[error.day] = {
          entries: [],
          timetableEntries: {}
        };
      }
      // @ts-ignore
      const dayScope: ErrorDaysMapType = scoreBreakdown.errors.days[error.day];
      errorTarget = dayScope.entries;

      if (error.timetableEntryId !== undefined) {
        if (dayScope.timetableEntries[error.timetableEntryId] === undefined) {
          dayScope.timetableEntries[error.timetableEntryId] = {
            entries: [],
            demonstrators: {},
          };
        }
        // @ts-ignore
        const timetableEntryScope: ErrorTimetableEntryMapType = dayScope.timetableEntries[error.timetableEntryId];
        errorTarget = timetableEntryScope.entries;

        if (error.demonstratorId !== undefined) {
          if (timetableEntryScope.demonstrators[error.demonstratorId] === undefined) {
            timetableEntryScope.demonstrators[error.demonstratorId] = {
              entries: [],
            }
          }
          // @ts-ignore
          const demonstratorScope: ErrorDemonstratorMapType = timetableEntryScope.demonstrators[error.demonstratorId];
          errorTarget = demonstratorScope.entries;
        }
      }
    }
    if (error.weekNumber !== undefined) {
      if (scoreBreakdown.errors.weeks[error.weekNumber] === undefined) {
        scoreBreakdown.errors.weeks[error.weekNumber] = [];
      }
      errorTarget = scoreBreakdown.errors.weeks[error.weekNumber] as string[];
    }

    errorTarget.push(error.message);
  }

  return scoreBreakdown;
}
