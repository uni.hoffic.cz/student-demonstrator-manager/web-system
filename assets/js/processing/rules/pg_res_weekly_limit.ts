import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";
import { ErrorType } from "../../types/score/ErrorType";
import { TimetableEntryType } from "../../types/TimetableEntryType";
import { getTimeslots, getWeek } from "../../util/dates";
import { Error } from "../../models/Error";
import { ERRORS } from "../../consts/RuleDefinitions";
import { PG_RES_STUDENT_WEEKLY_LIMIT } from "../../consts/business";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  errors: ErrorType[],
) => {
  // weekNumber => (demonstratorId => weeklyTotal)
  const weeklyAssignmentTotals: Map<number, Map<number, number>> = new Map();

  // For every day
  for (const date of database.timetableEntriesByDate.keys()) {

    const labs = database.labsByDate.get(date) as TimetableEntryType[];

    const weekNumber = getWeek(labs[0]);
    if (!weeklyAssignmentTotals.has(weekNumber)) {
      weeklyAssignmentTotals.set(weekNumber, new Map());
    }
    const currWeekAssTotal = weeklyAssignmentTotals.get(weekNumber) as Map<number, number>;

    // For every lab in a day
    for (const lab of labs) {

      // For every demonstrator assigned to the lab
      for (const demonstrator of database.getAssignments(lab, assignments)) {

        if (!currWeekAssTotal.has(demonstrator.id)) {
          currWeekAssTotal.set(demonstrator.id, 0);
        }

        // For every timeslot required by the lab (as [day]-[hour])
        for (const timeslot of getTimeslots(lab)) {
          currWeekAssTotal.set(demonstrator.id, currWeekAssTotal.get(demonstrator.id) as number + 1)
        }
      }
    }
  }

  for (const [weekNumber, demonstratorTotals] of weeklyAssignmentTotals.entries()) {

    for (const [demonstratorId, total] of demonstratorTotals.entries()) {

      const demonstrator = database.getDemonstrator(demonstratorId);

      if (demonstrator.postgraduateResearch && total > PG_RES_STUDENT_WEEKLY_LIMIT) {
        errors.push(new Error(
          ERRORS.PG_RES_WEEKLY_LIMIT,
          {name: demonstrator.fullName},
          undefined,
          undefined,
          demonstratorId,
          weekNumber
        ));
      }
    }
  }
};
