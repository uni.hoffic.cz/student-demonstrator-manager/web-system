import { TimetableEntryType } from "../../types/TimetableEntryType";
import { RatioOfPostgraduatesType } from "../../types/RatioOfPostgraduatesType";
import { Error } from "../../models/Error";
import { ERRORS } from "../../consts/RuleDefinitions";
import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";
import { ErrorType } from "../../types/score/ErrorType";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  errors: ErrorType[],
) => {
  // For every day
  for (const date of database.timetableEntriesByDate.keys()) {
    const labs = database.labsByDate.get(date) as TimetableEntryType[];

    // For every lab in a day
    for (const lab of labs) {

      const module = database.getModuleFlavour(lab.moduleFlavour.id).module;

      if (module.configurations.length > 0) {
        const config = module.configurations[0];

        if (config.ratioOfPostgraduates !== RatioOfPostgraduatesType.NONE_REQUIRED) {
          let undergraduates = 0;
          let postgraduates = 0;

          // For every demonstrator assigned to the lab
          for (const demonstrator of database.getAssignments(lab, assignments)) {

            if (demonstrator.year === 5) {
              postgraduates++;

            } else {
              undergraduates++;
            }
          }

          const allRequiredRule = config.ratioOfPostgraduates !== RatioOfPostgraduatesType.ALL_REQUIRED
            || undergraduates === 0;
          const someRequiredRule = config.ratioOfPostgraduates !== RatioOfPostgraduatesType.SOME_REQUIRED
            || postgraduates - undergraduates >= 0;

          if (!allRequiredRule || !someRequiredRule) {
            errors.push(new Error(ERRORS.MOD_SATISFACTORY_PG_RATIO, {}, date, lab.id));
            labsValidity.set(lab.id, false);
          }
        }
      }
    }
  }
};
