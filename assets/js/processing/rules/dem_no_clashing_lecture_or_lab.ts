import { TimetableEntryType } from "../../types/TimetableEntryType";
import { getTimeslotTimestamps } from "../../util/dates";
import { Error } from "../../models/Error";
import { ERRORS } from "../../consts/RuleDefinitions";
import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";
import { ErrorType } from "../../types/score/ErrorType";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  errors: ErrorType[],
) => {
  const timetableUnavailabilities = calculateDemonstratorUnavailabilityMap(database);

  // For every day
  for (const date of database.timetableEntriesByDate.keys()) {

    const labs = database.labsByDate.get(date) as TimetableEntryType[];

    // For every lab in a day
    for (const lab of labs) {

      // For every demonstrator assigned to the lab
      for (const demonstrator of database.getAssignments(lab, assignments)) {

        // For every timeslot required by the lab (as timestamps)
        for (const timestamp of getTimeslotTimestamps(lab)) {

          if (timetableUnavailabilities[demonstrator.id].has(timestamp)) {
            errors.push(new Error(
              ERRORS.DEM_NO_CLASHING_LECTURE_OR_LAB,
              {
                name: demonstrator.fullName,
              },
              date,
              lab.id,
              demonstrator.id
            ));
            labsValidity.set(lab.id, false);
          }
        }
      }
    }
  }
};

interface ModuleToTimestampsMap {
  [moduleName: string]: number[],
}

interface DemonstratorUnavailabilityMap {
  [demonstratorId: number]: Set<number>,
}

export const calculateDemonstratorUnavailabilityMap = (database: Database): DemonstratorUnavailabilityMap => {
  const moduleTimesMap: ModuleToTimestampsMap = {};
  for (const labsForDay of database.labsByDate.values()) {
    for (const lab of labsForDay) {
      const moduleName = database.getModuleFlavour(lab.moduleFlavour.id).module.name;
      if (moduleTimesMap[moduleName] === undefined) {
        moduleTimesMap[moduleName] = [];
      }
      moduleTimesMap[moduleName].push(...getTimeslotTimestamps(lab));
    }
  }

  const demonstratorUnavailabilities: DemonstratorUnavailabilityMap = {};
  for (const demonstrator of database.demonstratorsById.values()) {
    demonstratorUnavailabilities[demonstrator.id] = new Set();
    if (demonstrator.modulesTaken) {
      for (const moduleTaken of demonstrator.modulesTaken) {
        if (moduleTimesMap[moduleTaken]) {
          moduleTimesMap[moduleTaken].forEach(e => demonstratorUnavailabilities[demonstrator.id].add(e));
        }
      }
    }
  }

  return demonstratorUnavailabilities;
};
