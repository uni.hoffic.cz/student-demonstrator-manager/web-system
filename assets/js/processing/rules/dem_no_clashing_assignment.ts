import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";
import { ErrorType } from "../../types/score/ErrorType";
import { TimetableEntryType } from "../../types/TimetableEntryType";
import { getTimeslotTimestamps } from "../../util/dates";
import { Error } from "../../models/Error";
import { ERRORS } from "../../consts/RuleDefinitions";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  errors: ErrorType[],
) => {
  // For every day
  for (const date of database.timetableEntriesByDate.keys()) {

    const labs = database.labsByDate.get(date) as TimetableEntryType[];

    // key: [demonstrator id]-[timestamp]
    const usedTimeslotsForDemonstrators = new Set();

    // For every lab in a day
    for (const lab of labs) {

      // For every demonstrator assigned to the lab
      for (const demonstrator of database.getAssignments(lab, assignments)) {

        // For every timeslot required by the lab (as timestamps)
        for (const timestamp of getTimeslotTimestamps(lab)) {

          const key = `${demonstrator.id}-${timestamp}`;

          if (usedTimeslotsForDemonstrators.has(key)) {
            errors.push(new Error(
              ERRORS.DEM_NO_CLASHING_ASSIGNMENT,
              {
                name: demonstrator.fullName,
              },
              date,
              lab.id,
              demonstrator.id
            ));
            labsValidity.set(lab.id, false);

          } else {
            usedTimeslotsForDemonstrators.add(key);
          }
        }
      }
    }
  }
};
