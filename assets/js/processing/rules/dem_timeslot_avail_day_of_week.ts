import { TimetableEntryType } from "../../types/TimetableEntryType";
import { getDayName, getTimeslots } from "../../util/dates";
import { Error } from "../../models/Error";
import { ERRORS } from "../../consts/RuleDefinitions";
import { Database } from "../../models/Database";
import { ErrorType } from "../../types/score/ErrorType";
import { Assignments } from "../../models/Assignments";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  errors: ErrorType[],
) => {
  // For every day
  for (const date of database.timetableEntriesByDate.keys()) {
    const labs = database.labsByDate.get(date) as TimetableEntryType[];

    // For every lab in a day
    for (const lab of labs) {

      // For every demonstrator assigned to the lab
      for (const demonstrator of database.getAssignments(lab, assignments)) {

        // For every timeslot required by the lab (as [day]-[hour])
        for (const timeslot of getTimeslots(lab)) {

          if (!demonstrator.timeslotsAvailable.includes(timeslot)) {
            errors.push(new Error(
              ERRORS.DEM_TIMESLOT_AVAIL_DAY_OF_WEEK,
              {
                name: demonstrator.fullName,
                day: getDayName(timeslot.split('-')[0]),
                time: timeslot.split('-')[1],
              },
              date,
              lab.id,
              demonstrator.id
            ));
            labsValidity.set(lab.id, false);
          }
        }
      }
    }
  }
}
