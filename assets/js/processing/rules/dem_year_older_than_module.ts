import { TimetableEntryType } from "../../types/TimetableEntryType";
import { Error } from "../../models/Error";
import { ERRORS } from "../../consts/RuleDefinitions";
import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";
import { ErrorType } from "../../types/score/ErrorType";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  errors: ErrorType[],
) => {
  // For every day
  for (const date of database.timetableEntriesByDate.keys()) {

    const labs = database.labsByDate.get(date) as TimetableEntryType[];

    // For every lab in a day
    for (const lab of labs) {

      const module = database.getModuleFlavour(lab.moduleFlavour.id).module;

      // For every demonstrator assigned to the lab
      for (const demonstrator of database.getAssignments(lab, assignments)) {

        if (demonstrator.year <= module.year) {
          errors.push(new Error(
            ERRORS.DEM_YEAR_OLDER_THAN_MODULE,
            {
              name: demonstrator.fullName,
            },
            date,
            lab.id,
            demonstrator.id
          ));
          labsValidity.set(lab.id, false);
        }
      }
    }
  }
};
