import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  points: Map<number, number>,
) => {
  for (const lab of database.labs) {

    if (labsValidity.get(lab.id)) {
      const flavour = database.getModuleFlavour(lab.moduleFlavour.id);
      const config = flavour.module.configurations[0];

      let score = 0;

      if (config.studentsApproached !== null) {
        const assignedDems = database.getAssignments(lab, assignments);

        for (const studentApproached of config.studentsApproached) {
          if (assignedDems.find(dem => dem.universityEmailAddress === studentApproached)) {
            score += 200;
          }
        }
      }

      points.set(lab.id, points.get(lab.id) as number + score);
    }
  }
}
