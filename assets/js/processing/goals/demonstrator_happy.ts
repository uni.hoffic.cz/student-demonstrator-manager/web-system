import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  points: Map<number, number>,
) => {
  for (const lab of database.labs) {

    if (labsValidity.get(lab.id)) {
      const module = database.getModuleFlavour(lab.moduleFlavour.id).module;

      let score = 0;

      for (const demonstrator of database.getAssignments(lab, assignments)) {
        const preference = demonstrator.modulePreferences[module.name];

        score += (preference - 2) * 50;
      }

      points.set(lab.id, points.get(lab.id) as number + score);
    }
  }
}
