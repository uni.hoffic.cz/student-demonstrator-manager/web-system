import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";
import { STUDENTS_PER_DEMONSTRATOR } from "../../consts/business";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  points: Map<number, number>,
) => {
  for (const lab of database.labs) {

    if (labsValidity.get(lab.id)) {
      const flavour = database.getModuleFlavour(lab.moduleFlavour.id);
      const config = flavour.module.configurations[0];

      const studentCount = config.flavourStudentNumbers[flavour.flavour];
      const targetDems = studentCount / STUDENTS_PER_DEMONSTRATOR;
      const assignedDems = database.getAssignments(lab, assignments).length;

      const difference = Math.abs(targetDems - assignedDems);
      const score = Math.floor(1000 * (targetDems - difference) / targetDems);

      points.set(lab.id, points.get(lab.id) as number + score);
    }
  }
}
