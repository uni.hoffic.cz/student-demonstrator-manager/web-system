import { Database } from "../models/Database";
import ManualMode from "../consts/ManualMode";
import { TimetableEntryType } from "../types/TimetableEntryType";
import { getDate } from "../util/dates";

export const calculatedAffectedLabs = (rootLab: number, mode: string, database: Database) => {
  const affectedLabs = [];

  if (mode === ManualMode.SINGLE_LAB) {
    affectedLabs.push(rootLab);

  } else if (mode === ManualMode.ALL) {
    affectedLabs.push(...database.labs.map(lab => lab.id));

  } else if (mode === ManualMode.ALL_DAY) {
    const date = getDate(database.timetableEntriesById.get(rootLab) as TimetableEntryType);
    affectedLabs.push(...(database.labsByDate.get(date) as TimetableEntryType[]).map(lab => lab.id));

  } else {
    const flavour = (database.timetableEntriesById.get(rootLab) as TimetableEntryType).moduleFlavour.id;

    if (mode === ManualMode.ALL_FLAVOUR) {
      affectedLabs.push(
        ...database.labs
          .filter(lab => lab.moduleFlavour.id === flavour)
          .map(lab => lab.id));

    } else if (mode === ManualMode.ALL_MODULE) {
      const module = database.getModuleFlavour(flavour).module.id;
      affectedLabs.push(
        ...database.labs
          .filter(lab => database.getModuleFlavour(lab.moduleFlavour.id).module.id === module)
          .map(lab => lab.id));
    }
  }

  return affectedLabs;
}
