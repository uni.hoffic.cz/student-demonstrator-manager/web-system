import { Database } from "../models/Database";
import { calculatedAffectedLabs } from "./lab_affector";
import { calculateDemonstratorUnavailabilityMap } from "./rules/dem_no_clashing_lecture_or_lab";
import { TimetableEntryType } from "../types/TimetableEntryType";
import { getTimeslots, getTimeslotTimestamps } from "../util/dates";

export const calculateAvailableDemonstrators = (
  lab: number,
  mode: string,
  database: Database,
): Set<number> => {
  const availableDemonstrators = new Set<number>();
  database.demonstratorsById
    .forEach(demonstrator => availableDemonstrators.add(demonstrator.id));

  if (lab !== 0) {
    const affectedLabs = calculatedAffectedLabs(lab, mode, database)
      .map(id => database.timetableEntriesById.get(id) as TimetableEntryType);
    const affectedTimeslots = new Set<number>();
    affectedLabs.forEach(affectedLab => {
      getTimeslotTimestamps(affectedLab).forEach(timestamp => {
        affectedTimeslots.add(timestamp);
      });
    });
    const affectedWeeklySlots = new Set<string>();
    affectedLabs.forEach(affectedLab => getTimeslots(affectedLab)
      .forEach(slot => affectedWeeklySlots.add(slot)))

    const unavailabilityMap = calculateDemonstratorUnavailabilityMap(database);

    for (const demonstratorId of database.demonstratorIds) {
      for (const timeslot of affectedTimeslots) {
        if (unavailabilityMap[demonstratorId].has(timeslot)) {
          availableDemonstrators.delete(demonstratorId);
          break;
        }
      }
    }

    const remainingDemonstrators = Array.from(availableDemonstrators.values())
      .map(demonstratorId => database.getDemonstrator(demonstratorId));
    remainingDemonstrators.forEach(demonstrator => {
      for (const slot of affectedWeeklySlots) {
        if (!demonstrator.timeslotsAvailable.includes(slot)) {
          availableDemonstrators.delete(demonstrator.id);
          break;
        }
      }
    })
  }

  return availableDemonstrators;
}
