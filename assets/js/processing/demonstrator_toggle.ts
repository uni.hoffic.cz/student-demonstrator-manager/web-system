import { Assignments } from "../models/Assignments";
import { SetAssignments } from "../state/assignments_state";
import { Database } from "../models/Database";
import { calculatedAffectedLabs } from "./lab_affector";

export const toggleDemonstrator = (
  lab: number,
  demonstrator: number,
  add: boolean,
  mode: string,
  assignments: Assignments,
  setAssignments: SetAssignments,
  database: Database,
  lockedLabs: Set<number>,
) => {
  const affectedLabs = calculatedAffectedLabs(lab, mode, database)
    .filter((labId) => !lockedLabs.has(labId));

  for (const affectedLab of affectedLabs) {
    const labAssignments = assignments.get(affectedLab);
    const index = labAssignments.indexOf(demonstrator);

    if (add && index === -1) {
      labAssignments.push(demonstrator);

    } else if (!add && index !== -1) {
      labAssignments.splice(index, 1);
    }
  }

  setAssignments(new Assignments(assignments));
}
