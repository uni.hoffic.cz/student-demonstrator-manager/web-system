import { ErrorType } from "../types/score/ErrorType";
import { RawScoreResultType } from "../types/score/RawScoreResultType";
import { Database } from "../models/Database";
import { Assignments } from "../models/Assignments";
import dem_timeslot_avail_day_of_week from "./rules/dem_timeslot_avail_day_of_week";
import dem_no_clashing_lecture_or_lab from "./rules/dem_no_clashing_lecture_or_lab";
import dem_no_clashing_assignment from "./rules/dem_no_clashing_assignment";
import dem_year_older_than_module from "./rules/dem_year_older_than_module";
import dem_not_bad_mark from "./rules/dem_not_bad_mark";
import mod_satisfactory_pg_ratio from "./rules/mod_satisfactory_pg_ratio";
import pg_res_weekly_limit from "./rules/pg_res_weekly_limit";
import visa_weekly_limit from "./rules/visa_weekly_limit";
import number_of_demonstrators from "./goals/number_of_demonstrators";
import approached_students from "./goals/approached_students";
import demonstrator_happy from "./goals/demonstrator_happy";

export const calculateScore = (
  database: Database,
  assignments: Assignments,
): RawScoreResultType => {
  const errors: ErrorType[] = [];

  const points: Map<number, number> = new Map();
  database.labs.forEach(lab => points.set(lab.id, 0));

  const labsValidity: Map<number, boolean> = new Map();
  database.labs.forEach(lab => labsValidity.set(lab.id, true));

  // Rules
  dem_timeslot_avail_day_of_week(database, assignments, labsValidity, errors);
  dem_no_clashing_lecture_or_lab(database, assignments, labsValidity, errors);
  dem_no_clashing_assignment(database, assignments, labsValidity, errors);
  dem_year_older_than_module(database, assignments, labsValidity, errors);
  dem_not_bad_mark(database, assignments, labsValidity, errors);
  mod_satisfactory_pg_ratio(database, assignments, labsValidity, errors);
  pg_res_weekly_limit(database, assignments, labsValidity, errors);
  visa_weekly_limit(database, assignments, labsValidity, errors);

  // Goals
  number_of_demonstrators(database, assignments, labsValidity, points);
  approached_students(database, assignments, labsValidity, points);
  demonstrator_happy(database, assignments, labsValidity, points);

  // Fitness
  let totalPoints = errors.length * -1000;
  for (const labPoints of points.values()) {
    totalPoints += labPoints;
  }

  return {
    errors,
    points,
    totalPoints,
  };
};
