import { RatioOfPostgraduatesType } from "./RatioOfPostgraduatesType";

export interface ModuleConfigurationType {
  id: number,
  lecturerEmail: string,
  lecturerName: string,
  notes: string | null,
  numberOfStudents: number,
  flavourStudentNumbers: {[flavour: string]: number},
  ratioOfPostgraduates: RatioOfPostgraduatesType,
  studentsApproached: string[] | null,
}
