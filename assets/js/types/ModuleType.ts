import { ModuleConfigurationType } from "./ModuleConfigurationType";

export interface ModuleType {
  id: number,
  name: string,
  year: number,
  configurations: ModuleConfigurationType[],
}
