export interface DemonstratorType {
  id: number,
  fullName: string,
  marks: { [module: string]: number },
  modulePreferences: { [module: string]: number },
  modulesTaken: string[],
  postgraduateResearch: boolean,
  studentNumber: number,
  timeslotsAvailable: string[],
  universityEmailAddress: string,
  visaStudent: boolean,
  year: number,
}
