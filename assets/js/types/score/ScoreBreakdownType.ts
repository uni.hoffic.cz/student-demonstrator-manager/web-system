export interface ErrorDemonstratorMapType {
  entries: string[],
}

export interface ErrorTimetableEntryMapType {
  entries: string[],
  demonstrators: { [demonstratorId: number]: ErrorDemonstratorMapType | undefined }
}

export interface ErrorDaysMapType {
  entries: string[],
  timetableEntries: { [timetableEntryId: number]: ErrorTimetableEntryMapType | undefined },
}

export interface ErrorMapType {
  entries: string[],
  days: { [day: string]: ErrorDaysMapType | undefined },
  weeks: { [weekNumber: number]: string[] | undefined }
}

export interface ScoreBreakdownType {
  errors: ErrorMapType,
}
