import { RuleDefinition } from "../../consts/RuleDefinitions";

export interface ErrorType {
  message: string,
  day?: string,
  weekNumber?: number,
  timetableEntryId?: number,
  demonstratorId?: number,
  rule: RuleDefinition,
}
