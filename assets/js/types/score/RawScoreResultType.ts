import { ErrorType } from "./ErrorType";

export interface RawScoreResultType {
  errors: ErrorType[],
  points: Map<number, number>,
  totalPoints: number,
}
