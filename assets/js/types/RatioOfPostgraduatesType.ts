export enum RatioOfPostgraduatesType {
  NONE_REQUIRED = 'None required',
  SOME_REQUIRED = 'Some required',
  ALL_REQUIRED = 'All required',
}
