import { ModuleType } from "./ModuleType";

export interface ModuleFlavourType {
  id: number,
  flavour: string,
  module: ModuleType,
}
