export interface TimetableEntryType {
  id: number,
  moduleFlavour: { id: number },
  type: string,
  start: string,
  duration: number,
}
