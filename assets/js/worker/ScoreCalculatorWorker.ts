import { Database, DemonstratorsMap, ModuleFlavoursMap, TimetableEntriesMap } from "../models/Database";
import { Assignments } from "../models/Assignments";
import { calculateScore } from "../processing/score_calculator";

const worker: Worker = self as any;

let database: Database;

worker.addEventListener('message', (event) => {
  switch (event.data[0]) {
    case 'init':
      init(event.data[1]);
      break;
    case 'calculate-score':
      run(new Assignments(event.data[1]));
      break;
  }
});

const init = (data: {
  timetableEntries: TimetableEntriesMap,
  moduleFlavours: ModuleFlavoursMap,
  demonstrators: DemonstratorsMap,
  assignments: Assignments,
}) => {
  database = new Database(data.timetableEntries, data.moduleFlavours, data.demonstrators);
}

const run = (assignments: Assignments) => {
  postMessage(calculateScore(database, assignments));
}
