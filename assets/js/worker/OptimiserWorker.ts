import { NUMBER_OF_CHILDREN, optimise } from "../optimisation/optimiser";
import { Database, DemonstratorsMap, ModuleFlavoursMap, TimetableEntriesMap } from "../models/Database";
import { Assignments } from "../models/Assignments";
import ScoreCalculatorWorker from "worker-loader!./ScoreCalculatorWorker.ts";

const worker: Worker = self as any;

export interface OptimiserResult {
  assignments: Assignments,
  fitness: number,
  time: number,
}

let database: Database;
const scoreCalculatorWorkers: Worker[] = [];

worker.addEventListener('message', (event) => {
  switch (event.data[0]) {
    case 'init':
      init(event.data[1]);
      break;
    case 'optimise':
      run(new Assignments(event.data[1]), event.data[2], event.data[3], event.data[4]);
      break;
  }
});

const init = (data: {
  timetableEntries: TimetableEntriesMap,
  moduleFlavours: ModuleFlavoursMap,
  demonstrators: DemonstratorsMap,
  assignments: Assignments,
}) => {
  database = new Database(data.timetableEntries, data.moduleFlavours, data.demonstrators);

  for (let i = 0; i < NUMBER_OF_CHILDREN; i++) {
    const scoreCalculatorWorker = new ScoreCalculatorWorker();
    scoreCalculatorWorker.postMessage(['init', data]);
    scoreCalculatorWorkers.push(scoreCalculatorWorker);
  }
}

const run = (
  assignments: Assignments,
  lockedLabs: Set<number>,
  initMode: string,
  generations: number,
) => {
  const broadcastProgress = (percent: number, fitness: number) => {
    worker.postMessage(['progress', percent, fitness]);
  }

  optimise(
    database,
    assignments,
    lockedLabs,
    scoreCalculatorWorkers,
    broadcastProgress,
    initMode,
    generations
  ).then(
    result => {
      worker.postMessage(['result', result]);
    }
  );
}
