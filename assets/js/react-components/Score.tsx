import React from "react";
import { ScoreBreakdownType } from "../types/score/ScoreBreakdownType";
import { RawScoreResultType } from "../types/score/RawScoreResultType";

export interface ScoreProps {
  score: RawScoreResultType
}

export const Score: React.FunctionComponent<ScoreProps> = (props) => {
  return <div className={'sdm-score text-bold'}>
    Errors: {props.score.errors.length}, Total Score: {props.score.totalPoints}
  </div>;
}
