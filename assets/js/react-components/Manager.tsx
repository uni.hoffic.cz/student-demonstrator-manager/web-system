import React, { useEffect, useState } from 'react';

import { Day } from './Day';
import { Toolbar } from "./Toolbar";
import { calculateScore } from "../processing/score_calculator";
import ReactTooltip from "react-tooltip";
import { calculateScoreBreakdown } from "../processing/score_breakdown_calculator";
import { Score } from "./Score";
import { Database } from "../models/Database";
import { getWeek } from "../util/dates";
import { assignmentsState } from "../state/assignments_state";
import { TimetableEntryType } from "../types/TimetableEntryType";
import { lockedLabsState } from "../state/locked_labs_state";
import ManualMode from "../consts/ManualMode";

export interface ManagerProps {
  database: Database,
  assignments: string,
  optimiserWorker: Worker,
  debug: boolean,
}

export const Manager: React.FunctionComponent<ManagerProps> = (props) => {
  const [assignments, setAssignments] = assignmentsState(props.assignments);
  const [selectedLab, setSelectedLab] = useState<number>(0);
  const [lockedLabs, toggleLabLock] = lockedLabsState();
  const [mode, setMode] = useState<string>(ManualMode.SINGLE_LAB);

  const rawScore = calculateScore(props.database, assignments);
  const scoreBreakdown = calculateScoreBreakdown(rawScore);

  const dates = Array.from(props.database.timetableEntriesByDate.keys());
  dates.sort();

  useEffect(() => {
    ReactTooltip.rebuild();
  });

  return <div className={'sdm-manager'}>
    <Toolbar
      database={props.database}
      assignments={assignments}
      setAssignments={setAssignments}
      lockedLabs={lockedLabs}
      optimiserWorker={props.optimiserWorker}
      rawScore={rawScore}
      selectedLab={selectedLab}
      mode={mode}
      setMode={setMode}
    />
    <Score score={rawScore}/>
    {dates.map((date) => {
      const labs = props.database.labsByDate.get(date) as TimetableEntryType[];
      const week = getWeek(labs[0]);
      return <Day
        key={date}
        date={date}
        week={week}
        labs={labs}
        database={props.database}
        assignments={assignments}
        rawScore={rawScore}
        errors={scoreBreakdown.errors.days[date]}
        weekErrors={scoreBreakdown.errors.weeks[week]}
        selectedLab={selectedLab}
        setSelectedLab={setSelectedLab}
        lockedLabs={lockedLabs}
        toggleLabLock={toggleLabLock}
        mode={mode}/>;
    })}
    <ReactTooltip effect="solid"/>
  </div>;
};
