'use strict';

import React from 'react';

import { em } from '../util/units';
import { getHour } from '../util/dates';
import { tileHeight } from '../consts/visual';
import { TimetableEntryType } from "../types/TimetableEntryType";
import { TimetableEntry } from "./TimetableEntry";
import { ErrorDaysMapType } from "../types/score/ScoreBreakdownType";
import { Database } from "../models/Database";
import { Assignments } from "../models/Assignments";
import { RawScoreResultType } from "../types/score/RawScoreResultType";

export interface DayProps {
  week: number,
  date: string,
  labs: TimetableEntryType[],
  database: Database,
  assignments: Assignments,
  rawScore: RawScoreResultType,
  errors?: ErrorDaysMapType,
  weekErrors?: string[],
  selectedLab: number,
  setSelectedLab: React.Dispatch<React.SetStateAction<number>>,
  lockedLabs: Set<number>,
  toggleLabLock: (id: number, mode: string, database: Database) => void,
  mode: string,
}

export const Day: React.FunctionComponent<DayProps> = (props) => {
  const timeRange = getRange(props.labs);

  const weekErrors = props.weekErrors ?? [];

  return <div className={'sdm-day'}>
    <div className={'sdm-column'}>
      <div className={'sdm-week-name' + (weekErrors.length > 0 ? ' error' : '')}
           data-multiline={true}
           data-tip={weekErrors.join('<br/>')}
           data-type={'error'}
           data-place={'right'}
      >Week {props.week}</div>
      <div className={'sdm-day-name'}>{props.date}</div>
      {renderScore(props.errors?.entries ?? [])}
    </div>

    <div className={'sdm-day-scale'}>
      {timeRange.map((hour) => {
        return <div key={hour} style={{height: em(tileHeight)}}>{hour}</div>;
      })}
    </div>

    <div className={'sdm-entries'}>
      {props.labs.map((lab) => {
        return <TimetableEntry
          key={lab.id}
          lab={lab}
          dayStart={timeRange[0]}
          database={props.database}
          assignments={props.assignments}
          rawScore={props.rawScore}
          errors={props.errors?.timetableEntries[lab.id]}
          selected={lab.id === props.selectedLab}
          setSelectedLab={props.setSelectedLab}
          locked={props.lockedLabs.has(lab.id)}
          toggleLabLock={props.toggleLabLock}
          mode={props.mode}/>;
      })}
    </div>
  </div>;
};

const renderScore = (errors: string[]) => {
  if (errors.length > 0) {
    return <React.Fragment>
      <div className={'sdm-score'}>Score: PLACEHOLDER</div>
      <div className={'sdm-errors'}
           data-multiline={true}
           data-tip={errors.join('<br/>')}
           data-type={'error'}
           data-place={'right'}
      >Errors: {errors.length}</div>
    </React.Fragment>
  } else {
    return;
  }
}

const getRange = (entries: TimetableEntryType[]): number[] => {
  let min = 24;
  let max = 0;

  entries.forEach((entry) => {
    const start = getHour(entry);
    const end = start + entry.duration;

    if (start < min) {
      min = start;
    }

    if (end > max) {
      max = end;
    }
  });

  const range = [];

  for (let i = min; i < max; i++) {
    range.push(i);
  }

  return range;
};
