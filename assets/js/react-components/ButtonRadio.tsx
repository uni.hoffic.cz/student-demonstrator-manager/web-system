import React from 'react';

export const ButtonRadio = (props: {
  options: string[],
  value: string,
  setter: React.Dispatch<React.SetStateAction<string>>;
}) => {
  return <div className={'sdm-modes'}>
    {props.options.map(name => {
      return <button key={name}
                     className={name === props.value ? 'selected' : ''}
                     onClick={() => props.setter(name)}
      >
        {name}
      </button>
    })}
  </div>;
};
