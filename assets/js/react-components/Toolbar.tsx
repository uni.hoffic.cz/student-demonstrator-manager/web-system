import React, { useEffect, useState } from 'react';
import Draggable from 'react-draggable';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import InputNumber from 'rc-input-number';

import { Database } from "../models/Database";
import { RawScoreResultType } from "../types/score/RawScoreResultType";
import { ERRORS } from "../consts/RuleDefinitions";
import { Assignments } from "../models/Assignments";
import { SetAssignments } from "../state/assignments_state";
import { Line } from "react-chartjs-2";
import { ButtonRadio } from "./ButtonRadio";
import { toggleDemonstrator } from "../processing/demonstrator_toggle";
import ManualMode from "../consts/ManualMode";
import InitialisationMode from "../consts/InitialisationMode";
import { DemonstratorType } from "../types/DemonstratorType";
import ReactTooltip from "react-tooltip";
import { calculateAvailableDemonstrators } from "../processing/demonstrator_availability";
import { NUMBER_OF_ITERATIONS } from "../optimisation/optimiser";

export interface ToolbarProps {
  database: Database,
  assignments: Assignments,
  setAssignments: SetAssignments,
  lockedLabs: Set<number>,
  optimiserWorker: Worker,
  rawScore: RawScoreResultType,
  selectedLab: number,
  mode: string,
  setMode: React.Dispatch<React.SetStateAction<string>>,
}

const saveAssignments = (assignments: Assignments) => {
  fetch('save-assignments', {
    method: 'POST',
    body: assignments.toJson(),
  })
    .then(() => alert('Saved successfully!'))
    .catch(() => alert('An error occurred.'));
}

export const Toolbar: React.FunctionComponent<ToolbarProps> = (props) => {
  const [running, setRunning] = useState<boolean>(false);
  const [progress, setProgress] = useState<number>(0);
  const [fitnessSeries, setFitnessSeries] = useState<number[]>([]);
  const [initMode, setInitMode] = useState<string>(InitialisationMode.CURRENT);
  const [generations, setGenerations] = useState<number>(NUMBER_OF_ITERATIONS);

  return <div className={'sdm-toolbar-dock'}>
    <Draggable bounds={{top: 1050}} handle={'.sdm-handle'} defaultPosition={{x: 200, y: 1200}}>
      <div className={'sdm-toolbar'}>
        <div className={'sdm-handle'}>Toolbox</div>
        <Tabs>
          <TabList>
            <Tab>File</Tab>
            <Tab>Optimiser</Tab>
            <Tab>Manual</Tab>
          </TabList>
          <TabPanel>
            <File assignments={props.assignments} setAssignments={props.setAssignments}/>
          </TabPanel>
          <TabPanel>
            <Optimiser rawScore={props.rawScore}
                       database={props.database}
                       assignments={props.assignments}
                       lockedLabs={props.lockedLabs}
                       setAssignments={props.setAssignments}
                       optimiserWorker={props.optimiserWorker}
                       running={running}
                       setRunning={setRunning}
                       progress={progress}
                       setProgress={setProgress}
                       fitnessSeries={fitnessSeries}
                       setFitnessSeries={setFitnessSeries}
                       initMode={initMode}
                       setInitMode={setInitMode}
                       generations={generations}
                       setGenerations={setGenerations}
            />
          </TabPanel>
          <TabPanel>
            <Manual
              database={props.database}
              assignments={props.assignments}
              setAssignments={props.setAssignments}
              selectedLab={props.selectedLab}
              mode={props.mode}
              setMode={props.setMode}
              lockedLabs={props.lockedLabs}
            />
          </TabPanel>
        </Tabs>
      </div>
    </Draggable>
  </div>;
};

interface OptimiserProps {
  rawScore: RawScoreResultType,
  database: Database,
  assignments: Assignments,
  lockedLabs: Set<number>,
  setAssignments: SetAssignments,
  optimiserWorker: Worker,
  running: boolean,
  setRunning: React.Dispatch<React.SetStateAction<boolean>>,
  progress: number,
  setProgress: React.Dispatch<React.SetStateAction<number>>,
  fitnessSeries: number[],
  setFitnessSeries: React.Dispatch<React.SetStateAction<number[]>>,
  initMode: string,
  setInitMode: React.Dispatch<React.SetStateAction<string>>,
  generations: number,
  setGenerations: React.Dispatch<React.SetStateAction<number>>,
}

const File = (props: {
  assignments: Assignments,
  setAssignments: SetAssignments,
}) => {
  return <div className={'sdm-content'}>
    <button onClick={() => {
      saveAssignments(props.assignments)
    }}>Save assignments
    </button>
    <button onClick={() => {
      if (confirm('Are you sure you want to clear all assignments?')) {
        props.setAssignments(new Assignments());
      }
    }}>Clear all assignments
    </button>
  </div>;
}

let localFitnessSeries: number[] = [];

const Optimiser = (props: OptimiserProps) => {
  const runOptimiser = () => {
    props.setRunning(true);
    props.setProgress(0);
    props.setFitnessSeries([]);
    localFitnessSeries = [];

    props.optimiserWorker.onmessage = (event: MessageEvent) => {
      switch (event.data[0]) {
        case 'progress':
          props.setProgress(event.data[1]);
          localFitnessSeries.push(event.data[2]);
          props.setFitnessSeries(localFitnessSeries);
          break;

        case 'result':
          props.setAssignments(new Assignments(event.data[1].assignments));
          props.setRunning(false);
          break;
      }
    };

    props.optimiserWorker.postMessage([
      'optimise',
      props.assignments,
      props.lockedLabs,
      props.initMode,
      props.generations,
    ]);
  }

  return <div className={'sdm-content'}>
    <div className={'sdm-optimiser'}>
      <fieldset className={'sdm-init narrow'}>
        <legend>Initialisation</legend>
        <ButtonRadio options={[
          InitialisationMode.CURRENT,
          InitialisationMode.BIASED,
          InitialisationMode.EMPTY,
        ]} value={props.initMode} setter={props.setInitMode}/>
      </fieldset>
      <fieldset className={'sdm-generations narrow'}>
        <legend>Generations</legend>
        <InputNumber defaultValue={props.generations}
                     min={0}
                     step={500}
                     disabled={props.running}
                     onChange={(value) => props.setGenerations(value)}/>
      </fieldset>
      <fieldset className={'sdm-score-function'}>
        <legend>Score Function</legend>
        <table>
          <tbody>
          <tr>
            <th>Rule name</th>
            <th>Penalty</th>
            <th>Details</th>
          </tr>
          {Object.values(ERRORS).map((rule) => {
            return <tr key={rule.description}>
              <td>{rule.description}</td>
              <td>Error</td>
              <td>{rule.details}</td>
            </tr>;
          })}
          </tbody>
        </table>
      </fieldset>
      <fieldset>
        <legend>Results</legend>
        <div>
          Total errors: {props.rawScore.errors.length},
          fitness: {props.rawScore.totalPoints}
        </div>
        <Line
          data={{
            labels: Array.from(props.fitnessSeries.keys()),
            datasets: [
              {
                label: 'Fitness of the fittest',
                fill: false,
                lineTension: 0,
                borderColor: 'rgba(75,192,192,1)',
                data: props.fitnessSeries,
              }
            ]
          }}
          height={40}
          redraw={props.fitnessSeries.length === 0}
        />
      </fieldset>
      <fieldset>
        <legend>Controls</legend>
        {!props.running && <button onClick={runOptimiser}>Run Optimisation</button>}
        {props.running && <div>Progress: {props.progress}%
          <progress max={100} value={props.progress}/>
        </div>}
      </fieldset>
    </div>
  </div>;
}

const compileFavouriteModules = (demonstrator: DemonstratorType) => {
  return Object.entries(demonstrator.modulePreferences)
    .sort((a, b) => b[1] - a[1])
    .map(([module, score]) => `${module} - ${'★'.repeat(score)}${demonstrator.marks && demonstrator.marks[module] ? ' - ' + demonstrator.marks[module] + '%' : ''}`)
    .join('<br/>');
}

const Manual = (props: {
  database: Database,
  assignments: Assignments,
  setAssignments: SetAssignments,
  selectedLab: number,
  mode: string,
  setMode: React.Dispatch<React.SetStateAction<string>>,
  lockedLabs: Set<number>,
}) => {
  useEffect(() => {
    ReactTooltip.rebuild();
  });

  // @ts-ignore
  const selectedModule = props.selectedLab ? props.database.getModuleFlavour(props.database.timetableEntriesById.get(props.selectedLab).moduleFlavour.id).module : undefined;
  const approachedDemonstrators = selectedModule?.configurations[0].studentsApproached ?? [];
  const availableDemonstrators = calculateAvailableDemonstrators(
    props.selectedLab,
    props.mode,
    props.database
  );

  return <div className={'sdm-content'}>
    <fieldset>
      <legend>Modes</legend>
      <ButtonRadio options={[
        ManualMode.SINGLE_LAB,
        ManualMode.ALL_FLAVOUR,
        ManualMode.ALL_MODULE,
        ManualMode.ALL_DAY,
        ManualMode.ALL,
      ]}
                   value={props.mode}
                   setter={props.setMode}/>
    </fieldset>
    <fieldset>
      <legend>Demonstrators</legend>
      <div className={'sdm-demonstrators'}>
        {props.database.demonstratorsByYear
          .filter(demonstrators => demonstrators.size > 0)
          .map((demonstrators) => {
            const year = Array.from(demonstrators.values())[0].year;
            const insufficientYear = selectedModule ? year <= selectedModule.year : false;

            return <div key={year} className={'sdm-year'}>
              <div className={'sdm-year-label' + (insufficientYear ? ' sdm-insufficient' : '')}>Year {year}</div>
              {Array.from(demonstrators.values()).map((demonstrator) => {
                const selected = props.assignments.get(props.selectedLab).includes(demonstrator.id);
                const available = availableDemonstrators.has(demonstrator.id);

                return <div
                  key={demonstrator.id}
                  className={'sdm-entry' + (selected ? ' selected' : '') + (available ? '' : ' unavailable')}
                  onClick={() => toggleDemonstrator(
                    props.selectedLab,
                    demonstrator.id,
                    !selected,
                    props.mode,
                    props.assignments,
                    props.setAssignments,
                    props.database,
                    props.lockedLabs,
                  )}
                  data-multiline={true}
                  data-place={'right'}
                  data-tip={`${demonstrator.fullName} (${demonstrator.studentNumber})<br/>${compileFavouriteModules(demonstrator)}`}
                >
                  {demonstrator.fullName}
                  {demonstrator.visaStudent && <span className={'sdm-tag'}> V</span>}
                  {demonstrator.postgraduateResearch && <span className={'sdm-tag'}> R</span>}
                  {selectedModule && demonstrator.modulePreferences && demonstrator.modulePreferences[selectedModule.name] &&
                  <span className={'sdm-rank'}> {demonstrator.modulePreferences[selectedModule.name]}★</span>}
                  {selectedModule && demonstrator.marks && demonstrator.marks[selectedModule.name] &&
                  <span className={'sdm-mark'}> {demonstrator.marks[selectedModule.name]}%</span>}
                  {approachedDemonstrators.includes(demonstrator.universityEmailAddress) && <span> ✅</span>}
                </div>;
              })}
            </div>;
          })}
        <div className={'sdm-legend'}>
          <div className={'sdm-entry'}><span className={'sdm-tag'}>V</span> - Visa Student</div>
          <div className={'sdm-entry'}><span className={'sdm-tag'}>R</span> - Postgraduate Research Student</div>
          <div className={'sdm-entry'}><span className={'sdm-rank'}>x★</span> - Demonstrator's module preference (1-5)
          </div>
          <div className={'sdm-entry'}><span className={'sdm-mark'}>xx%</span> - Mark achieved in module</div>
          <div className={'sdm-entry'}><span style={{textDecoration: 'line-through'}}>Name</span> - Unavailable during
            the time
          </div>
          <div className={'sdm-entry'}><span className={'sdm-insufficient'} style={{fontWeight: 'bold'}}>Year x</span> -
            Students not senior-enough
          </div>
          <div className={'sdm-entry'}><span>✅</span> - Student approached by lecturer</div>
        </div>
      </div>
    </fieldset>
  </div>;
}
