import React from "react";
import { initials } from "../util/strings";
import { ErrorDemonstratorMapType } from "../types/score/ScoreBreakdownType";
import { DemonstratorType } from "../types/DemonstratorType";

export interface DemonstratorProps {
  demonstrator: DemonstratorType,
  errors?: ErrorDemonstratorMapType,
}

export const Demonstrator: React.FunctionComponent<DemonstratorProps> = (props) => {
  const errors = props.errors?.entries ?? [];
  return <div
    className={'sdm-demonstrator' + (errors.length > 0 ? ' error' : '')}
    data-multiline={true}
    data-tip={`${props.demonstrator.fullName} (year ${props.demonstrator.year})<br/>${errors.join('<br/>')}`}
    data-type={(errors.length > 0 ? 'error' : 'info')}>
    {initials(props.demonstrator.fullName)}
  </div>;
}
