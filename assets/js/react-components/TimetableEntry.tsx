import React from 'react';

import { em } from '../util/units';
import { tileHeight, tileWidth } from '../consts/visual';
import { TimetableEntryType } from "../types/TimetableEntryType";
import { ModuleFlavourType } from "../types/ModuleFlavourType";
import { DemonstratorType } from "../types/DemonstratorType";
import { ErrorTimetableEntryMapType } from "../types/score/ScoreBreakdownType";
import { Demonstrator } from "./Demonstrator";
import { Database } from "../models/Database";
import { Assignments } from "../models/Assignments";
import { RawScoreResultType } from "../types/score/RawScoreResultType";
import { STUDENTS_PER_DEMONSTRATOR } from "../consts/business";

export interface TimetableEntryProps {
  lab: TimetableEntryType,
  dayStart: number,
  database: Database,
  assignments: Assignments,
  rawScore: RawScoreResultType,
  errors?: ErrorTimetableEntryMapType,
  selected: boolean,
  setSelectedLab: React.Dispatch<React.SetStateAction<number>>,
  locked: boolean,
  toggleLabLock: (id: number, mode: string, database: Database) => void,
  mode: string,
}

export const TimetableEntry: React.FunctionComponent<TimetableEntryProps> = (props) => {
  const moduleFlavour: ModuleFlavourType = props.database.getModuleFlavour(props.lab.moduleFlavour.id);
  const assignedDemonstrators = props.database.getAssignments(props.lab, props.assignments);
  const errors = props.errors?.entries ?? [];
  const config = moduleFlavour.module.configurations[0];
  const targetDemonstrators = config.flavourStudentNumbers[moduleFlavour.flavour];
  const pgRatio = moduleFlavour.module.configurations[0].ratioOfPostgraduates;

  return <div
    className={'sdm-entry'}
    style={calculateStyles(props.lab, props.dayStart)}
  >
    <div
      className={"sdm-name" + (errors.length > 0 ? ' error' : '')}
      data-multiline={true}
      data-tip={`
${moduleFlavour.flavour}<br/>
Postgraduates: ${pgRatio}<br/>
${assignedDemonstrators.length} assigned / ${(targetDemonstrators / STUDENTS_PER_DEMONSTRATOR).toFixed(2)} ideal<br/>
Points: ${props.rawScore.points.get(props.lab.id)}<br/>
Notes: ${config.notes ?? '-'}<br/>
<br/>
${errors.join('<br/>')}`}
      data-type={errors.length > 0 ? 'error' : 'info'}
    >{moduleFlavour.flavour}</div>
    <div className={'sdm-assignees' + (props.selected ? ' selected' : '')}
         onClick={() => {
           if (props.selected) {
             props.setSelectedLab(0);
           } else {
             props.setSelectedLab(props.lab.id);
           }
         }}>
      {assignedDemonstrators.map((demonstrator: DemonstratorType) => {
        return <Demonstrator
          key={demonstrator.id}
          demonstrator={demonstrator}
          errors={props.errors?.demonstrators[demonstrator.id]}
        />;
      })}
    </div>
    <div className={'sdm-state-bar'}>
      <div className={'sdm-status' + (props.locked ? ' fa fa-lock' : ' fa fa-unlock')}
           onClick={() => props.toggleLabLock(props.lab.id, props.mode, props.database)}
      />
    </div>
  </div>;
}

const calculateStyles = (entry: TimetableEntryType, dayStart: number) => {
  const entriesStart = new Date(entry.start).getUTCHours();
  const margin = tileHeight * (entriesStart - dayStart);

  return {
    width: em(tileWidth),
    height: em(tileHeight * entry.duration),
    marginTop: em(margin),
  };
};
