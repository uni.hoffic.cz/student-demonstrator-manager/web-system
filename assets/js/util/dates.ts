import { TimetableEntryType } from "../types/TimetableEntryType";

export const getHour = (entry: TimetableEntryType): number => {
  return new Date(entry.start).getUTCHours();
};

export const getDate = (entry: TimetableEntryType): string => {
  const date = new Date(entry.start);
  const month = date.getUTCMonth() + 1;
  const day = date.getUTCDate();
  return [
    date.getUTCFullYear(),
    '-',
    month < 10 ? '0' : '',
    month,
    '-',
    day < 10 ? '0' : '',
    day
  ].join('');
}

// https://stackoverflow.com/questions/6117814/get-week-of-year-in-javascript-like-in-php
export const getWeek = (entry?: TimetableEntryType): number => {
  if (entry === undefined) {
    return 0;
  } else {
    const d = new Date(entry.start);
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    const yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    return Math.ceil((((d.getTime() - yearStart.getTime()) / 86400000) + 1) / 7);
  }
}

export const getTimeslots = (entry: TimetableEntryType): string[] => {
  const start = new Date(entry.start);
  const range = [];

  for (let hour = start.getUTCHours(); hour < start.getUTCHours() + entry.duration; hour++) {
    range.push(`${start.getUTCDay()}-${hour}`);
  }

  return range;
}

export const getTimeslotTimestamps = (entry: TimetableEntryType): number[] => {
  const times: number[] = [];

  const start = Math.floor((new Date(entry.start)).getTime() / 1000);
  for (let i = 0; i < entry.duration; i++) {
    times.push(start + i * 1000);
  }

  return times;
}

const dayNames: string[] = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];
export const getDayName = (dayNumber: number | string): string => {
  if (typeof dayNumber === 'string') {
    dayNumber = parseInt(dayNumber, 10);
  }
  return dayNames[dayNumber - 1];
};
