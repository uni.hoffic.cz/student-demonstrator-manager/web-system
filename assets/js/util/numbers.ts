export function* range(end: number, start: number = 0) {
  for (let i = start; i <= end; i++) {
    yield i;
  }
}
