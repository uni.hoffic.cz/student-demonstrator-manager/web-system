export const initials = (name: string): string => {
  const names = name.split(' ');
  if (names.length < 2) {
    return '-';
  } else {
    return names[0][0] + names[names.length - 1][0];
  }
}
