import React, { useState } from "react";
import { Assignments } from "../models/Assignments";

export type SetAssignments = React.Dispatch<React.SetStateAction<Assignments>>;

export const assignmentsState = (assignmentsString: string) => {
  const assignmentsObject = JSON.parse(assignmentsString);
  const assignmentsMap: Assignments = new Assignments();

  for (const [timetableEntryId, demonstratorIds] of Object.entries(assignmentsObject)) {
    assignmentsMap.set(parseInt(timetableEntryId, 10), demonstratorIds as number[]);
  }

  return useState<Assignments>(assignmentsMap);
}
