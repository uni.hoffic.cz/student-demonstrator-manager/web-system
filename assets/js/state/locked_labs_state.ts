import { useState } from "react";
import { calculatedAffectedLabs } from "../processing/lab_affector";
import { Database } from "../models/Database";

export const lockedLabsState: () => [
  Set<number>,
  (id: number, mode: string, database: Database) => void
] = () => {
  const [lockedLabs, setLockedLabs] = useState<Set<number>>(new Set());

  const toggleLabLock = (id: number, mode: string, database: Database) => {
    const affectedLabs = calculatedAffectedLabs(id, mode, database);
    const unlocking = lockedLabs.has(id);

    for (const affectedLab of affectedLabs) {
      if (unlocking) {
        lockedLabs.delete(affectedLab);
      } else {
        lockedLabs.add(affectedLab);
      }
    }

    setLockedLabs(new Set(lockedLabs));
  };

  return [lockedLabs, toggleLabLock];
}
