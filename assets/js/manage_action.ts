import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Manager } from './react-components/Manager';
import OptimiserWorker from "worker-loader!./worker/OptimiserWorker.ts";
import { Database, DemonstratorsMap, ModuleFlavoursMap, TimetableEntriesMap } from "./models/Database";

const managerContainer: HTMLElement | null = document.querySelector('#manager-root');
if (!managerContainer) {
  console.log('Unable to locate root element.');
} else {
  const timetableEntries: TimetableEntriesMap = JSON.parse(
    managerContainer.dataset.timetableEntries as string);
  const moduleFlavours: ModuleFlavoursMap = JSON.parse(
    managerContainer.dataset.moduleFlavours as string);
  const demonstrators: DemonstratorsMap = JSON.parse(
    managerContainer.dataset.demonstrators as string);
  const assignments = managerContainer.dataset.assignments as string;
  const debug = managerContainer.dataset.debug === 'true';

  const optimiserWorker = new OptimiserWorker();
  optimiserWorker.postMessage([
    'init',
    {
      timetableEntries,
      moduleFlavours,
      demonstrators,
    }
  ]);

  const database = new Database(
    timetableEntries,
    moduleFlavours,
    demonstrators,
  );

  ReactDOM.render(
    React.createElement(
      Manager,
      {
        database,
        assignments,
        optimiserWorker,
        debug,
      }
    ),
    managerContainer);
}
