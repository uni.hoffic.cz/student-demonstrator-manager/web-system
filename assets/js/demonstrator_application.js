const FORM_NAME = 'demonstrator_application_form';
const YEAR_ID = 'demonstrator_application_form_year';
const PG_RES_NAME = 'demonstrator_application_form_postgraduateResearch';
const demonstratorForm = document.getElementsByName(FORM_NAME)[0];
const modulePreferenceRows = demonstratorForm.querySelectorAll('[data-type="module-preference"].row');
const modulesTakenRows = demonstratorForm.querySelectorAll('[data-type="module-taken"].row');
const yearPicker = document.getElementById(YEAR_ID);
const postgraduateResearch = document.getElementsByName(PG_RES_NAME)[0];

const filterAvailableModules = () => {
  modulePreferenceRows.forEach((element) => {
    if (yearPicker.value > element.dataset.year) {
      element.classList.remove('hidden');
    } else {
      element.classList.add('hidden');
    }

    const modulesHeader = document.querySelector('.modules-header');
    if (yearPicker.value < 2) {
      modulesHeader.classList.add('hidden');
    } else {
      modulesHeader.classList.remove('hidden');
    }
  });

  modulesTakenRows.forEach((element) => {
    if (element.dataset.year === yearPicker.value) {
      element.classList.remove('hidden');
    } else {
      element.classList.add('hidden');
    }
  });
};
yearPicker.addEventListener('change', filterAvailableModules);
filterAvailableModules();

const timeslotsRow = document.querySelector('.row.timeslots-available');
const timeslotInputs = timeslotsRow.querySelectorAll('input');
const displayUnavailReasonBox = () => {
  let found = false;
  timeslotInputs.forEach((input) => {
    if (!input.checked) {
      found = true;
    }
  });
  const reasonBox = document.querySelector('.unavailable-reason-box');
  if (found) {
    reasonBox.classList.remove('hidden');
  } else {
    reasonBox.classList.add('hidden');
  }
};
displayUnavailReasonBox();
timeslotInputs.forEach((input) => {
  input.onchange = displayUnavailReasonBox;
});

const postgraduateResearchBox = document.querySelector('.postgraduate-research-box');
const displayPostgraduateResearch = () => {
  if (yearPicker.value >= 4) {
    postgraduateResearchBox.classList.remove('hidden');
  } else {
    postgraduateResearchBox.classList.add('hidden');
  }
};
yearPicker.addEventListener('change', displayPostgraduateResearch);
displayPostgraduateResearch();
