import { ModuleFlavourType } from "../types/ModuleFlavourType";
import { DemonstratorType } from "../types/DemonstratorType";
import { TimetableEntryType } from "../types/TimetableEntryType";
import { getDate } from "../util/dates";
import { Assignments } from "./Assignments";

export interface TimetableEntriesMap {
  [day: string]: TimetableEntryType[],
}

export interface ModuleFlavoursMap {
  [moduleFlavourId: number]: ModuleFlavourType,
}

export interface DemonstratorsMap {
  [demonstratorId: number]: DemonstratorType
}

export class Database {
  // Indices
  readonly timetableEntriesById: Map<number, TimetableEntryType>;
  readonly timetableEntriesByDate: Map<string, TimetableEntryType[]>;
  readonly labsByDate: Map<string, TimetableEntryType[]>;
  readonly demonstratorsById: Map<number, DemonstratorType>;
  readonly moduleFlavoursById: Map<number, ModuleFlavourType>;

  // Pre-computed
  readonly demonstratorIds: number[] = [];
  readonly labs: TimetableEntryType[];
  readonly demonstratorsByYear: Set<DemonstratorType>[] = [];

  constructor(
    timetableEntries: TimetableEntriesMap,
    moduleFlavours: ModuleFlavoursMap,
    demonstrators: DemonstratorsMap,
  ) {
    // TimetableEntryType
    this.timetableEntriesById = new Map();
    this.timetableEntriesByDate = new Map();
    for (const dayEntries of Object.values(timetableEntries)) {
      for (const timetableEntry of dayEntries) {
        this.timetableEntriesById.set(timetableEntry.id, timetableEntry);

        const date = getDate(timetableEntry);
        if (!this.timetableEntriesByDate.has(date)) {
          this.timetableEntriesByDate.set(date, []);
        }
        (this.timetableEntriesByDate.get(date) as TimetableEntryType[]).push(timetableEntry);
      }
    }

    for (const dayTimetableEntries of this.timetableEntriesByDate.values()) {
      dayTimetableEntries.sort((a: TimetableEntryType, b: TimetableEntryType): number => {
        return a.start.localeCompare(b.start);
      });
    }

    this.labsByDate = new Map();
    for (const [date, dayTimetableEntries] of this.timetableEntriesByDate.entries()) {
      this.labsByDate.set(
        date,
        dayTimetableEntries.filter(
          (entry) => entry.type === 'LAB'));
    }

    this.labs = Array.from(this.timetableEntriesById.values())
      .filter((entry) => entry.type === 'LAB');

    // DemonstratorType
    this.demonstratorsById = new Map();
    for (let i = 0; i <= 5; i++) {
      this.demonstratorsByYear[i] = new Set();
    }
    for (const demonstrator of Object.values(demonstrators)) {
      this.demonstratorsById.set(demonstrator.id, demonstrator);
      this.demonstratorIds.push(demonstrator.id);
      this.demonstratorsByYear[demonstrator.year].add(demonstrator);
    }

    // ModuleFlavourType
    this.moduleFlavoursById = new Map();
    for (const moduleFlavour of Object.values(moduleFlavours)) {
      this.moduleFlavoursById.set(moduleFlavour.id, moduleFlavour);
    }
  }

  getModuleFlavour(id: number): ModuleFlavourType {
    return this.moduleFlavoursById.get(id) as ModuleFlavourType;
  }

  getDemonstrator(id: number): DemonstratorType {
    return this.demonstratorsById.get(id) as DemonstratorType;
  }

  getAssignments(
    timetableEntry: TimetableEntryType,
    assignments: Assignments,
  ): DemonstratorType[] {
    const entryAssignments = assignments.get(timetableEntry.id);

    return entryAssignments.map((id: number): DemonstratorType => {
      return this.demonstratorsById.get(id) as DemonstratorType;
    });
  }

  dump() {
    console.log('Timetable Entries', this.timetableEntriesById);
    console.log('Module Flavours', this.moduleFlavoursById);
    console.log('Demonstrators', this.demonstratorsById);
  }
}
