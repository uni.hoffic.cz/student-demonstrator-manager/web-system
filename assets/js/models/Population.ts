import { Assignments } from "./Assignments";

export class Population extends Array<Assignments> {
  constructor(size: number) {
    super(size);
  }

  fight = (assignments: Assignments) => {
    let pointer = 0;
    while (pointer < this.length) {
      const pointerFitness = this[pointer] === undefined
        ? Number.MIN_SAFE_INTEGER
        : this[pointer].fitness;

      if (assignments.fitness >= pointerFitness) {
        break;
      }

      pointer++;
    }

    let tmp1 = assignments;
    let tmp2;
    for (let i = pointer; i < this.length; i++) {
      tmp2 = this[i];
      this[i] = tmp1;
      tmp1 = tmp2;
    }
  }

  fittest = (): Assignments => {
    return this[0];
  }
}
