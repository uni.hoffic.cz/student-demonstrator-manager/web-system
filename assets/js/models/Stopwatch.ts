export class Stopwatch {
  private readonly start: number;

  constructor() {
    this.start = performance.now();
  }

  stop = (): number => {
    return performance.now() - this.start;
  }
}
