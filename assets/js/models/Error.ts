import { RuleDefinition } from "../consts/RuleDefinitions";
import { ErrorType } from "../types/score/ErrorType";

export interface ErrorOptions {
  [optionName: string]: string,
}

export class Error implements ErrorType {
  readonly rule: RuleDefinition;
  readonly message: string;
  readonly day?: string;
  readonly timetableEntryId?: number;
  readonly demonstratorId?: number;
  readonly weekNumber?: number;

  constructor(
    rule: RuleDefinition,
    options: ErrorOptions = {},
    day?: string,
    timetableEntryId?: number,
    demonstratorId?: number,
    weekNumber?: number
  ) {
    this.rule = rule;

    this.message = rule.error
    for (const [option, value] of Object.entries(options)) {
      this.message = this.message.replace(`[${option}]`, value);
    }

    this.day = day;
    this.timetableEntryId = timetableEntryId;
    this.demonstratorId = demonstratorId;
    this.weekNumber = weekNumber;
  }
}
