export class Assignments extends Map<number, number[]> {
  fitness: number = Number.MIN_SAFE_INTEGER;

  get = (id: number): number[] => {
    if (!super.has(id)) {
      super.set(id, []);
    }
    return super.get(id) as number[];
  }

  clone = () => {
    const copy: Assignments = new Assignments();

    for (const [timetableEntryId, demonstratorIds] of this.entries()) {
      copy.set(timetableEntryId, [...demonstratorIds]);
    }

    return copy;
  }

  toJson = () => {
    const builder: {[demonstratorId: string]: number[]} = {};

    this.forEach((value, key) => {
      builder[key.toString(10)] = value;
    })

    return JSON.stringify(builder);
  }

  dump = () => {
    console.log('Assignments', this);
  }
}
