export default {
  SINGLE_LAB: 'Single Lab',
  ALL_FLAVOUR: 'All Flavour',
  ALL_MODULE: 'All Module',
  ALL_DAY: 'All Day',
  ALL: 'All',
}
