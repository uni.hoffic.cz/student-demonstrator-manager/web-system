import { PG_RES_STUDENT_WEEKLY_LIMIT, VISA_STUDENT_WEEKLY_LIMIT } from "./business";

export interface RuleDefinition {
  description: string,
  error: string,
  details?: string,
}

export interface RuleDefinitions {
  [ruleName: string]: RuleDefinition,
}

export const ERRORS: RuleDefinitions = {
  DEM_TIMESLOT_AVAIL_DAY_OF_WEEK: {
    description: "Demonstrator indicated they're available during that timeslot when applying.",
    error: "[name] is not available on [day]s from [time]:00.",
  },
  DEM_NO_CLASHING_LECTURE_OR_LAB: {
    description: "Demonstrator does not have a lab or a lecture at the same time as their demonstrator session.",
    error: "[name] has a clashing lecture or a lab.",
  },
  DEM_NO_CLASHING_ASSIGNMENT: {
    description: "Demonstrator does not have an assigned session that clashes with another one.",
    error: "[name] is already assigned to another session during this time.",
  },
  DEM_YEAR_OLDER_THAN_MODULE: {
    description: "Demonstrator is at least a year above the module they're demonstrating on.",
    error: "[name] is not senior-enough.",
  },
  DEM_NOT_BAD_MARK: {
    description: "Demonstrator does not have a bad mark in the assigned module.",
    error: "[name] has a bad mark.",
    details: "Bad mark = <70%",
  },
  MOD_SATISFACTORY_PG_RATIO: {
    description: "Module has a satisfactory postgraduate to undergraduate student ratio.",
    error: "Module has too many undergraduate demonstrators.",
  },
  PG_RES_WEEKLY_LIMIT: {
    description: `Postgraduate research students can only demonstrate ${PG_RES_STUDENT_WEEKLY_LIMIT}h in any single week.`,
    error: "[name] violates postgraduate research weekly limit.",
  },
  VISA_WEEKLY_LIMIT: {
    description: `Visa students can only demonstrate ${VISA_STUDENT_WEEKLY_LIMIT}h in any single week.`,
    error: "[name] violates visa student weekly limit.",
  }
}
