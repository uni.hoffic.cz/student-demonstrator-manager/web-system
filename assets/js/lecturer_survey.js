const lecturerSurveyForm = document.getElementsByName('lecturer_survey_form')[0];
if (lecturerSurveyForm) {
  const checkboxes = lecturerSurveyForm.querySelectorAll('input[type="checkbox"]');
  checkboxes.forEach((element) => {
    const container = element.closest('.module-block');
    if (element.checked) {
      container.classList.add('selected');
    }
    element.onchange = () => {
      container.classList.toggle('selected');
    };
  });
}
