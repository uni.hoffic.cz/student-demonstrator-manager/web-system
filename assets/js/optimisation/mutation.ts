import { Assignments } from "../models/Assignments";
import { Database } from "../models/Database";

export const mutate = (
  children: Assignments[],
  database: Database,
  lockedLabs: Set<number>,
  removeAddSplit: number,
) => {
  for (const child of children) {
    if (Math.random() < removeAddSplit) {
      removeDemonstrators(child, 2, lockedLabs);

    } else {
      addDemonstrators(child, database, 2, lockedLabs);
    }
  }
}

const removeDemonstrators = (child: Assignments, targetAmount: number, lockedLabs: Set<number>) => {
  let assignedCount = 0;
  for (const labDemonstrators of child.values()) {
    assignedCount += labDemonstrators.length;
  }
  const removeChance = targetAmount / assignedCount;

  for (const [labId, labDemonstrators] of child.entries()) {
    if (!lockedLabs.has(labId) && Math.random() < removeChance) {
      labDemonstrators.splice(Math.floor(Math.random() * labDemonstrators.length), 1);
    }
  }
}

const addDemonstrators = (child: Assignments, database: Database, targetAmount: number, lockedLabs: Set<number>) => {
  const addChance = targetAmount / database.labs.length;

  for (const lab of database.labs) {
    if (!lockedLabs.has(lab.id) && Math.random() < addChance) {
      const labDemonstrators = child.get(lab.id);

      while (true) {
        const randomDem = database.demonstratorIds[Math.floor(Math.random() * database.demonstratorIds.length)];
        if (!labDemonstrators.includes(randomDem)) {
          labDemonstrators.push(randomDem);
          break;
        }
      }
    }
  }
}
