import { Population } from "../models/Population";
import { Assignments } from "../models/Assignments";
import { Database } from "../models/Database";
import { calculateAvailableDemonstrators } from "../processing/demonstrator_availability";
import ManualMode from "../consts/ManualMode";
import { STUDENTS_PER_DEMONSTRATOR } from "../consts/business";
import { ModuleFlavourType } from "../types/ModuleFlavourType";
import { RatioOfPostgraduatesType } from "../types/RatioOfPostgraduatesType";

type FlavourAssignments = Map<number, number[]>;

export const biasedInit = (population: Population, database: Database) => {
  const flavourAssignments: FlavourAssignments = new Map();

  const flavours = Array.from(new Set(database.labs
    .map(lab => lab.moduleFlavour.id)
    .map(flavourId => database.getModuleFlavour(flavourId))));
  flavours.sort((a, b) => b.module.year - a.module.year);

  assignApproachedStudents(database, flavourAssignments);
  assignAnyAvailable(flavours, database, flavourAssignments);

  return flavourAssignmentsToLabs(flavourAssignments, database);
}

const assignApproachedStudents = (database: Database, flavourAssignments: FlavourAssignments) => {
  for (const flavour of database.moduleFlavoursById.values()) {
    const config = flavour.module.configurations[0];

    if (config && config.studentsApproached !== null) {
      const approachedEmails = new Set(config.studentsApproached);
      const approachedStudents = [...database.demonstratorsById.values()]
        .filter(dem => approachedEmails.has(dem.universityEmailAddress))
        .map(dem => dem.id);
      flavourAssignments.set(flavour.id, approachedStudents);
    }
  }
}

const assignAnyAvailable = (flavours: ModuleFlavourType[], database: Database, flavourAssignments: FlavourAssignments) => {
  for (const flavour of flavours) {
    const referenceLab = database.labs.find(lab => lab.moduleFlavour.id === flavour.id)?.id ?? 0;
    const availableDemonstrators = calculateAvailableDemonstrators(referenceLab, ManualMode.ALL_FLAVOUR, database);
    const config = flavour.module.configurations[0];

    const targetNumber = Math.round(config.flavourStudentNumbers[flavour.flavour] / STUDENTS_PER_DEMONSTRATOR);
    const existingDemonstrators = flavourAssignments.get(flavour.id) ?? [];
    const uniqueAvailable = [...availableDemonstrators.values()]
      .filter(demonstratorId => !existingDemonstrators.includes(demonstratorId))
      .map(demonstratorId => database.getDemonstrator(demonstratorId))
      .filter(demonstrator => {
        const minYear = config.ratioOfPostgraduates === RatioOfPostgraduatesType.ALL_REQUIRED
          ? 5
          : flavour.module.year + 1;
        return demonstrator.year >= minYear;
      })
      .sort((a, b) => {
        const preferenceFactor = b.modulePreferences[flavour.module.name] - a.modulePreferences[flavour.module.name];
        const yearFactor = a.year - b.year;
        return preferenceFactor * 10 + yearFactor;
      })
      .map(demonstrator => demonstrator.id);

    flavourAssignments.set(flavour.id, [...existingDemonstrators, ...uniqueAvailable.slice(0, Math.max(targetNumber - existingDemonstrators.length, 0))]);
  }
}

const flavourAssignmentsToLabs = (flavourAssignments: FlavourAssignments, database: Database) => {
  const assignments = new Assignments();

  for (const lab of database.labs) {
    const flavourId = lab.moduleFlavour.id;

    if (flavourAssignments.has(flavourId)) {
      const affectedLabIds = database.labs
        .filter(affectedLab => affectedLab.moduleFlavour.id === flavourId)
        .map(affectedLab => affectedLab.id);

      affectedLabIds.forEach(affectedLabId => {
        assignments.set(affectedLabId, flavourAssignments.get(flavourId) as number[]);
      });
    }
  }

  return assignments;
}
