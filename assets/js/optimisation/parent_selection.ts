import { Population } from "../models/Population";
import { Assignments } from "../models/Assignments";

export const selectParents = (population: Population): [Assignments, Assignments] => {
  return [
    population[expProb(population.length)],
    population[expProb(population.length)]
  ];
}

const expProb = (max: number) => {
  return Math.floor(Math.sqrt(Math.sqrt(Math.random())) * max);
}
