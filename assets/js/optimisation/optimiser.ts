import { Database } from "../models/Database";
import { Assignments } from "../models/Assignments";
import { Population } from "../models/Population";
import { selectParents } from "./parent_selection";
import { pertube } from "./pertubation";
import { mutate } from "./mutation";
import { OptimiserResult } from "../worker/OptimiserWorker";
import { RawScoreResultType } from "../types/score/RawScoreResultType";
import { Stopwatch } from "../models/Stopwatch";
import InitialisationMode from "../consts/InitialisationMode";
import { biasedInit } from "./biased_init";

const POPULATION_SIZE = 8;
export const NUMBER_OF_CHILDREN = 6;
export const NUMBER_OF_ITERATIONS = 250;

export const optimise = async (
  database: Database,
  assignments: Assignments,
  lockedLabs: Set<number>,
  scoreCalculatorWorkers: Worker[],
  broadcastProgress: (percent: number, fitness: number) => void,
  initMode: string,
  generations: number,
): Promise<OptimiserResult> => {

  const population: Population = new Population(POPULATION_SIZE);
  if (initMode === InitialisationMode.CURRENT) {
    for (let i = 0; i < POPULATION_SIZE; i++) {
      population.fight(assignments.clone());
    }

  } else if (initMode === InitialisationMode.BIASED) {
    const bias = biasedInit(population, database);
    for (let i = 0; i < POPULATION_SIZE; i++) {
      population.fight(bias.clone());
    }

  } else {
    for (let i = 0; i < POPULATION_SIZE; i++) {
      population.fight(new Assignments());
    }
  }

  const stopwatch = new Stopwatch();
  const percentChunk = Math.ceil(generations / 100);
  let percent = 0;
  for (let i = 0; i < generations; i++) {
    await round(population, lockedLabs, database, scoreCalculatorWorkers, i);

    if (i % percentChunk === 0) {
      broadcastProgress(percent, population.fittest().fitness);
      percent++;
    }
  }

  return {
    assignments: population.fittest(),
    fitness: population.fittest().fitness,
    time: stopwatch.stop(),
  };
};

const round = async (
  population: Population,
  lockedLabs: Set<number>,
  database: Database,
  scoreCalculatorWorkers: Worker[],
  i: number
) => {
  const parents = selectParents(population);
  const children = pertube(parents, NUMBER_OF_CHILDREN, lockedLabs);
  mutate(children, database, lockedLabs, i / NUMBER_OF_ITERATIONS * 0.5);

  const scorePromises: Promise<RawScoreResultType>[] = [];
  for (let childIndex = 0; childIndex < children.length; childIndex++) {
    scorePromises.push(calculateScoreAsync(
      children[childIndex],
      scoreCalculatorWorkers[childIndex]));
  }

  const scores = await Promise.all(scorePromises);

  for (let childIndex = 0; childIndex < children.length; childIndex++) {
    const child = children[childIndex];
    const score = scores[childIndex];
    child.fitness = score.totalPoints;
    population.fight(child);
  }
}

const calculateScoreAsync = (
  assignments: Assignments,
  worker: Worker,
): Promise<RawScoreResultType> => {
  return new Promise<RawScoreResultType>((resolve => {
    worker.onmessage = (event) => {
      resolve(event.data);
    }
    worker.postMessage(['calculate-score', assignments]);
  }));
}
