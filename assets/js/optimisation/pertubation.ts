import { Assignments } from "../models/Assignments";

export const pertube = (
  parents: [Assignments, Assignments],
  numberOfChildren: number,
  lockedLabs: Set<number>,
): Assignments[] => {
  const children: Assignments[] = [];
  for (let i = 0; i < numberOfChildren; i++) {
    children[i] = new Assignments();
  }

  for (const labIndex of parents[0].keys()) {
    const p1Dems = parents[0].get(labIndex);
    const p2Dems = parents[1].get(labIndex);

    if (lockedLabs.has(labIndex)) {
      for (const child of children) {
        child.get(labIndex).push(...p1Dems);
      }
    }

    const demonstratorIdPool: Set<number> = new Set();
    p1Dems.forEach(demonstrator => demonstratorIdPool.add(demonstrator));
    p2Dems.forEach(demonstrator => demonstratorIdPool.add(demonstrator));

    const targetDemonstratorCount = (p1Dems.length + p2Dems.length) / 2;
    const acceptanceProbability = targetDemonstratorCount / demonstratorIdPool.size;

    for (const child of children) {
      for (const demonstratorId of demonstratorIdPool.values()) {
        if (Math.random() < acceptanceProbability) {
          child.get(labIndex).push(demonstratorId);
        }
      }
    }
  }

  return children;
}
