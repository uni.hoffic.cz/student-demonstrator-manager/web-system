<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210413170522 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE demonstrator (id SERIAL NOT NULL, full_name VARCHAR(255) NOT NULL, student_number INT NOT NULL, module_preferences TEXT DEFAULT NULL, university_email_address VARCHAR(255) DEFAULT NULL, year INT DEFAULT NULL, postgraduate_research BOOLEAN DEFAULT NULL, visa_student BOOLEAN DEFAULT NULL, modules_taken TEXT DEFAULT NULL, timeslots_available TEXT DEFAULT NULL, share_key VARCHAR(255) DEFAULT \'invalid\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN demonstrator.module_preferences IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN demonstrator.year IS \'(DC2Type:studentyear)\'');
        $this->addSql('COMMENT ON COLUMN demonstrator.modules_taken IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN demonstrator.timeslots_available IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE demonstrator_application (id SERIAL NOT NULL, full_name VARCHAR(255) DEFAULT NULL, student_number INT DEFAULT NULL, module_preferences TEXT DEFAULT NULL, university_email_address VARCHAR(255) DEFAULT NULL, year INT DEFAULT NULL, postgraduate_research BOOLEAN DEFAULT NULL, visa_student BOOLEAN DEFAULT NULL, modules_taken TEXT DEFAULT NULL, timeslots_available TEXT DEFAULT NULL, timeslots_unavailable_reasons TEXT DEFAULT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, action VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN demonstrator_application.module_preferences IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN demonstrator_application.year IS \'(DC2Type:studentyear)\'');
        $this->addSql('COMMENT ON COLUMN demonstrator_application.modules_taken IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN demonstrator_application.timeslots_available IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN demonstrator_application.action IS \'(DC2Type:applaction)\'');
        $this->addSql('CREATE TABLE login_token (email VARCHAR(255) NOT NULL, one_time_code VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(email))');
        $this->addSql('CREATE TABLE module (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, year INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE module_configuration (id SERIAL NOT NULL, module_id INT DEFAULT NULL, lecturer_name VARCHAR(255) NOT NULL, lecturer_email VARCHAR(255) NOT NULL, number_of_students INT DEFAULT NULL, flavour_student_numbers TEXT NOT NULL, students_approached TEXT DEFAULT NULL, ratio_of_postgraduates VARCHAR(255) DEFAULT NULL, notes TEXT DEFAULT NULL, date_created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F639657FAFC2B591 ON module_configuration (module_id)');
        $this->addSql('COMMENT ON COLUMN module_configuration.flavour_student_numbers IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN module_configuration.students_approached IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN module_configuration.ratio_of_postgraduates IS \'(DC2Type:pgreq)\'');
        $this->addSql('CREATE TABLE module_flavour (id SERIAL NOT NULL, module_id INT DEFAULT NULL, flavour VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E489E79AAFC2B591 ON module_flavour (module_id)');
        $this->addSql('CREATE TABLE student_mark_record (id SERIAL NOT NULL, student_number INT DEFAULT NULL, marks TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN student_mark_record.marks IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE timetable_entry (id SERIAL NOT NULL, module_flavour_id INT DEFAULT NULL, start TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, duration VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CC0C8C9D139EA65D ON timetable_entry (module_flavour_id)');
        $this->addSql('COMMENT ON COLUMN timetable_entry.duration IS \'(DC2Type:dateinterval)\'');
        $this->addSql('CREATE TABLE demonstrator_allocations (timetable_entry_id INT NOT NULL, demonstrator_id INT NOT NULL, PRIMARY KEY(timetable_entry_id, demonstrator_id))');
        $this->addSql('CREATE INDEX IDX_70AF6F25D0A7533 ON demonstrator_allocations (timetable_entry_id)');
        $this->addSql('CREATE INDEX IDX_70AF6F25A63690C4 ON demonstrator_allocations (demonstrator_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE module_configuration ADD CONSTRAINT FK_F639657FAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE module_flavour ADD CONSTRAINT FK_E489E79AAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE timetable_entry ADD CONSTRAINT FK_CC0C8C9D139EA65D FOREIGN KEY (module_flavour_id) REFERENCES module_flavour (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE demonstrator_allocations ADD CONSTRAINT FK_70AF6F25D0A7533 FOREIGN KEY (timetable_entry_id) REFERENCES timetable_entry (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE demonstrator_allocations ADD CONSTRAINT FK_70AF6F25A63690C4 FOREIGN KEY (demonstrator_id) REFERENCES demonstrator (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE demonstrator_allocations DROP CONSTRAINT FK_70AF6F25A63690C4');
        $this->addSql('ALTER TABLE module_configuration DROP CONSTRAINT FK_F639657FAFC2B591');
        $this->addSql('ALTER TABLE module_flavour DROP CONSTRAINT FK_E489E79AAFC2B591');
        $this->addSql('ALTER TABLE timetable_entry DROP CONSTRAINT FK_CC0C8C9D139EA65D');
        $this->addSql('ALTER TABLE demonstrator_allocations DROP CONSTRAINT FK_70AF6F25D0A7533');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE demonstrator');
        $this->addSql('DROP TABLE demonstrator_application');
        $this->addSql('DROP TABLE login_token');
        $this->addSql('DROP TABLE module');
        $this->addSql('DROP TABLE module_configuration');
        $this->addSql('DROP TABLE module_flavour');
        $this->addSql('DROP TABLE student_mark_record');
        $this->addSql('DROP TABLE timetable_entry');
        $this->addSql('DROP TABLE demonstrator_allocations');
        $this->addSql('DROP TABLE "user"');
    }
}
