# Testing

## Automated Testing

This project is constrained by being delivered as a part of a university course in the span of roughly 7 months. The
time, however, is not exclusively available to this project, but rather is shared among all university work and personal
interests.

The scope of the project is very large, particularly in the implementation. The result is that a thorough automated
testing is not feasible. This is especially the case because the project experiments with several new technologies, and
locking any part of the system with unit or E2E tests would result in too much overhead since we are constantly
refactoring like in extreme programming and any part seemingly unrelated may change suddenly.

To mitigate the impact of not having automated code tests, we run a SonarQube static analysis with a very strict preset,
which analyses the code comprehensively. This tool can uncover subtle bad practices and flag "code smells" that have the
potential to develop into problems in the future.

Using self-documenting code and using the intuitive ReactJS jsx/tsx component notation further adds to the code
readability decreasing the potential for misunderstandings in the first place. SonarQube also calculates and restricts
"Cognitive Complexity" of any part of the code, which correlates well with parts of the code likely for bugs to be
introduced into.

The project has a [CI/CD pipeline](DOCUMENTATION.md#ci--cd-pipeline) where the application is being tested, built and
published. Currently, only smoke tests are in place, which are checking the infrastructure and the build process. These
tests guarantee that the dependencies and configuration are set up correctly and that the landing page works.

The system is configured and ready for unit and E2E tests to be added in the future.

## Acceptance Testing

This section shows how this project fulfills its requirements. We performed a series of 11 tests at the end of
development and documented the results, which can be seen below. 9 of the 11 tests passed and 2 failed. The failing
tests, however, are acceptable since in one case, the substitute functionality is superior to the required and in the
second case, a design decision was made that made meeting the requirement infeasible.

Each test started with a sample situation inferred from the requirements, and was based on state achieved by performing
a previous test where applicable. Factual data was acquired from official sources or estimated, personal data was faked
using a custom [fake data command](DOCUMENTATION.md#fake-data). Each test was performed by precisely following
instructions found in [Manual](MANUAL.md) for the part(s) of the system under test. The PASS/FAIL decision was obtained
by testing whether each requirement as a propositional logic statement evaluated to true. Ambiguities in the exact
meaning evaluated to the more lenient interpretation if the user could be reasonably expected to use the part of the
system to successfully achieve their goal.

Each test case attempted to verify one or more requirements, which are listed in the test case's header. The
requirements can be seen in a table below for reference. Each test case also contains a brief description of the test
and what conditions constituted a PASS. A short context is provided in the details section.

### Requirements

Base requirements as defined in the initial document, included for reference.

| Requirement | Description |
| --- | --- |
| R01 | The system can obtain a list of modules and lab sessions from the public timetable |
| R02 | The system can determine the assigned lecturer for each module |
| R03 | The system can collect lecturers’ requirements and preferences about student demonstrators to be assigned to their modules |
| R04 | The system can collect students demonstrators’ preferences about their assigned modules and time availability |
| R05 | The system can determine when student demonstrators are busy with their own lectures |
| R06 | The system only allows student demonstrators to demonstrate in lab sessions for lower year students |
| R07 | The system can visualize all lab sessions and the assigned students |
| R08 | The system asks the optimiser for constraints and associated weights or parameters |
| R09 | The system can automatically optimise the student demonstrator assignment based on the specified constraints |
| R10 | The optimiser can intervene during the optimisation by locking particular demonstrator assignments |
| R11 | The optimiser can change the constraints or their weights or parameters during optimisation |
| R12 | The optimisation result can be exported in formats that allow lecturers and demonstrators to verify they are comfortable with the generated assignments |

### Stretch Requirements

Stretch requirements as defined in the initial document, included for reference.

| Stretch Requirement | Description |
| --- | --- |
| S01 | The system is able to react to timetable changes by suggesting new assignments |
| S02 | The system can suggest substitutions for unavailable student demonstrators |
| S03 | Lecturers can authenticate using their Swansea University account |
| S04 | The system can notify students by email with the summary of their assigned lab sessions |


### Test Cases

| Asserts Requirements: R01, R02, R07 |
|---|
| **Description:** Tests whether the system can obtain a list of modules and lab sessions from the official timetable and determine the lecturer responsible for each module. |
| **Acceptance Criteria:**<br/>- The process only requires a constant amount of interaction O(1)<br/>- The timetable data is correct and valid to its specification<br/>- The complete process takes less than 15 minutes<br/>- The lecturer has been determined correctly<br/>- The timetable data can be viewed in the system by an administrator |
| **Result:** PASS |
| **Details:** The results were achieved by using the [Timetable Parser](https://gitlab.com/uni.hoffic.cz/student-demonstrator-manager/timetable-parser) included with the project, which scrapes the College of Science timetable. The outputted file was imported using a designated form in the application and 5 randomly selected days were thoroughly checked against the official timetable to check for data correctness from the administrator's view.<br/><br/>![](images/testing_4.png)<br/>![](images/testing_1.png) |

| Asserts Requirements: R03 |
|---|
| **Description:** Tests whether the system can collect lecturers’ requirements and preferences about student demonstrators to be assigned to their modules. |
| **Acceptance Criteria:**<br/>- Lecturers can submit module requirements and preferences using a web interface<br/>- The system will not allow submitting incomplete data<br/>- Students are denied access to the interface<br/>- Submitted data is persisted, correct and visible to an administrator |
| **Result:** PASS |
| **Details:** This result was achieved by filling out the [Lecturer Survey](MANUAL.md#for-lecturers). The form demonstrated a satisfactory amount of input verification and all entered data was observed in the administrator's interface.<br/><br/>![](images/testing_2.png)<br/>![](images/testing_3.png) |

| Asserts Requirements: R04, R05 |
|---|
| **Description:** Tests whether the system can collect student demonstrators’ preferences about their assigned modules and time availability and determine when the are available. |
| **Acceptance Criteria:**<br/>- Students can submit the required information using a web interface<br/>- The system will refuse incomplete data<br/>- The system will refuse application that do not acknowledge they had applied on the Swansea University job site <br/>- Submitted data is persisted, correct and visible to an administrator<br/>- The system correctly calculates when students are available |
| **Result:** PASS |
| **Details:** This result was achieved by filling out the [Demonstrator Application Form](MANUAL.md#for-students). The form demonstrated satisfactory robustness and validation, and enforced acknowledgements required. The filled-out application was successfully observed in the admin interface and was correct.<br/><br/>![](images/testing_5.png)<br/>![](images/testing_6.png) |

| Asserts Requirements: R07 |
|---|
| **Description:** Tests whether the system can visualize all lab sessions and the assigned students. |
| **Acceptance Criteria:**<br/>- Lab sessions indicate their start time and duration<br/>- Labs convey details about their module and flavour<br/>- Assigned demonstrators are clearly indicated</br>- An arbitrary day or week can be found intuitively |
| **Result:** PASS |
| **Details:** This result was achieved by navigating to the [Manage view](MANUAL.md#for-administrators) of the admin interface. The vertical position of each lab indicated the start time and the height of its box indicated its duration. Module and flavour details were observed upon hovering over the lab header. Assigned demonstrators were clearly visible in the lab's box and the date and the week number were indicated on the left side of the day. Days were displayed in order making it simple to find a desired date.<br/><br/>![](images/testing_7.png) |

| Asserts Requirements: R08, R11 |
|---|
| **Description:** Tests whether the system asks the optimiser for constraints and associated weights or parameters and whether the admin can change rule weights during the optimisation. |
| **Acceptance Criteria:**<br/>- Administrator can specify new fitness rules<br/>- Administrator can tweak weights of existing rules<br/>- Administrator can change weights of the rules during optimisation |
| **Result:** FAIL |
| **Detail:** As explained in [Rejected Designs](REJECTED_DESIGNS.md#administrator-tweaking-optimisation-weights), it was decided that this feature would not be helpful in its proposed form and to make it useful, it would require the experimental optimiser to be much more mature. It is still possible to add new rules and tweak the outcomes of existing rules, just not using a GUI but rather by changing the [rule definitions](DOCUMENTATION.md#manage-view) in code. This, however, means, that changing weights is not possible during an optimisation. |

| Asserts Requirements: R09 |
|---|
| **Description:** Tests whether the system can automatically optimise the student demonstrator assignment based on the specified constraints. |
| **Acceptance Criteria:**<br/>- The optimisation can be started by an administrator<br/>- The optimisation runs for a specified number of generations<br/>- The optimisation produces assignments with less errors and more points<br/>- The optimisation reports its progress in terms of generations and score |
| **Result:** PASS |
| **Details:** This result was achieved by navigating to the [Manage view](MANUAL.md#for-administrators) of the admin interface with existing demonstrator assignments. The optimisation was started and the progress was reported, indicating the score/fitness increasing over time. The result was a set of assignments with less errors than before running the optimiser.<br/><br/>![](images/testing_8.png) |

| Asserts Requirements: R10 |
|---|
| **Description:** Tests whether the optimiser can intervene during the optimisation by locking particular demonstrator assignments. |
| **Acceptance Criteria:**<br/>- The optimiser can lock a lab, a day, all labs from a module or a flavour, or all labs<br/>- Locked labs are not affected by the optimisation</br>- Locked labs cannot be changed by the administrator<br/>- Locked labs can be unlocked |
| **Result:** PASS |
| **Details:** This result was achieved by running the optimisation for a specified number of generations, locking labs desired not to be changed, and finally, running the optimiser again. The optimiser was observed not to alter any of the locked labs.<br/></br>![](images/testing_9.png) |

| Asserts Requirements: R12 |
|---|
| **Description:** Tests whether the optimisation result can be exported in formats that allow lecturers and demonstrators to verify they are comfortable with the generated assignments. |
| **Acceptance Criteria:**<br/>- The administrator can export lab assignments<br/>- The file displays the module, flavour, start time and duration of labs<br/>- The file shows assigned demonstrator names<br/>- The file shows assigned demonstrator student numbers in a format simple to copy and paste somewhere else |
| **Result:** PASS |
| **Details:** The result was achieved by navigating to the timetable entries section of the admin interface, clicking the "Download" button and choosing the CSV format.<br/><br/>![](images/testing_11.png)<br/>![](images/testing_10.png) |

| Asserts Requirements: S01, S02 |
|---|
| **Description:** Tests whether the system is able to react to timetable changes and unavailable demonstrators by suggesting substitute demonstrators. |
| **Acceptance Criteria:**<br/>- The system suggests alternative demonstrators for a lab or a group of labs, required for whatsoever reason |
| **Result:** PASS |
| **Details:** This result was achieved by navigating to the [Manage view](MANUAL.md#for-administrators) of the admin interface with existing demonstrator assignments. After clicking a lab session, the manual tab of the toolbox indicates which demonstrators are available to be assigned to the selected lab session(s).<br/><br/>![](images/testing_12.png) |

| Asserts Requirements: S03 |
|---|
| **Description:** Tests whether lecturers can authenticate using their Swansea University account. |
| **Acceptance Criteria:**<br/>- Lecturers are redirected to the university's OAuth portal and offered to be logged in using their university account |
| **Result:** PASS |
| **Note:** The functionality has been implemented and tested on a different authentication domain. However, Swansea University decided to block this application's access to the authentication portal. More details in [Documentation for Developers](DOCUMENTATION.md#authentication).<br/><br/>![](images/testing_13.png) |

| Asserts Requirements: S04 |
|---|
| **Description:** Tests whether the system can notify students by email with the summary of their assigned lab sessions. |
| **Acceptance Criteria:**<br/>- Students receive an email with their assignments summary |
| **Result:** FAIL |
| **Detail:** As explained in [Rejected Designs](REJECTED_DESIGNS.md#emailing-students-their-assignments), emailing students was not practical. Instead, we implemented a far superior feature which allows students to subscribe to a calendar and always see their up-to-date assignments.<br/><br/>![](images/manual_student_application_9.png) |

## Testing Conclusion

The automated testing infrastructure is currently not being heavily utilised for code testing, but it is a solid base
for doing so in the future if there is need for it. The continuous integration and deployment pipeline already performs
smoke testing and packages the application for production deployment, but can likewise be expanded to gate releases
more eagerly according to stricter rules as the reliability needs change.

The acceptance testing verified that the system built does fulfill the requirements. Although some tests failed, the
functionality is superior to the one required.
