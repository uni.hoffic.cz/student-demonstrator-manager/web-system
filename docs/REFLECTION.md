# Reflection

This document reflects on the way the Student Demonstrator Manager project was delivered as a requirement in obtaining a
Master's degree in software engineering.

## Background

This project has been developed for Swansea University with the intent that it could potentially be used in real student
allocation. The project deadline was arbitrarily set by the university, but the remaining aspects of the project were
left for me, the student, to decide. Since I have years of software engineering experience from the industry, I aimed at
delivering a piece of software that would proudly represent my abilities and that was as close to a production software
as possible.

The reason why I did not opt for a simpler project or a simpler implementation of the current one is that I wanted a
challenge in order to be motivated at all. University coursework can be extremely boring for someone with experience who
is used to solving practical software engineering problems. I chose to go outside my comfort zone and deliver a project
that was:

- Front-end heavy
- Using ReactJS
- Using TypeScript
- Running asynchronous, parallelized code (Web Workers)
- Involving a genetic optimisation algorithm

My motivation throughout the project was to learn the technologies I chose to interact with and the concepts surrounding
them. Secondarily, as the timeline progressed and my experimentation window ended, I became more focused with a good
delivery of the software I set out to deliver.

## Planning

I planned for the project to contain several experimental parts, and allowed time buffers for various parts of the
system to encounter issues. Together with variable time I was able to commit throughout the year and the unpredictable
nature of a project with ambitious, not clearly defined requirements, Kanban as a methodology worked out great. I kept a
backlog of issues the system needed in order to satisfy the requirements, and I would break them down into pieces of
work addressable at a time and implement. Any changes in requirements or a new insight into the researched technology
would update the priorities and scopes of the existing backlog issues and allow new, higher order issues to be broken
down.

The issue with Kanban was the unpredictable project delivery/conclusion date, which I had to work around. I did set the
project milestones to verify the progress was on track, but the further in the future a milestone was from the project
start, the less specific it was and the more ambiguous the project progress indication was. This is normally mitigated
by having experienced developers in a team, who can foresee this situation and better plan for it. This probably would
not have happened to me had I chosen a simple project, but, as I said earlier, I wanted to challenge myself and this was
one of the associated risks.

Although I was progressing well through my timeline, at the end of it I had to finish numerous uncategorized issues.
Many issues were also discovered during the creation of documentation. This meant I had to spend many extra days on the
project right around the time when other university deadlines were due. I did have to use my time buffers, but I did not
have to decrease the project scope or settle for a lower quality of delivery.

Overall, I don't think there was a better way of managing this project due to the special status and requirements.

## Risks

My philosophy of delivering software is that it does not matter whether we adhere to the requirements that we set in the
beginning as long as we solve the client's problem and make them happy, not do what they asked us to do if it does not
solve their problem. This way the requirements become an initial idea of what is being built and will change as the
project progresses.

In the case of this project, the requirements that were the most precisely defined happened to be implemented first, and
so there was no or very little deviation. Requirements that were less precisely defined happened to be implemented
later, often because of their pre-requisite features. This meant that it was not difficult to meet the requirements.
Also, the implementation ended up delivering significantly more functionality than the requirements stated, which is
also a consequence of a client-first philosophy.

All risks that were encountered during the project were accounted for, but like with the requirements, their specificity
decayed as the forecast period increased and therefore their mitigations were not particularly helpful. The project was
very dynamic throughout and a catch-all mitigation would in general just be to find another way of doing X or what if we
deliver Y instead of X to still address the client's needs. The specific risks would become obsolete within weeks.

An example of a risk encountered was the university's poor communication in the matter of allowing the system to use the
university's authentication domain. From my initial inquiry it took weeks for my request to get assigned to the
responsible person/team, and then I received a rejection with the reason being they "would not feel comfortable". They
did not react to my further questions and eventually closed the request. To mitigate this, I found another way of
authenticating students and lecturers using their university email addresses. This was within the prepared mitigation
strategy, but the strategy was very general, without concrete action items relating to the problem. The risk as defined
in the initial document can be seen below for reference.

| Encountered Risk ID | Description | Likelihood | Impact |
| --- | --- | --- | --- |
| S8 | University bureaucracy halts progress | High | Severe |

**S8**: If the university's processes or requirements halt the project progress, we will stop working with the
university to the extent permitted by the curriculum, and deliver the project for the client without the aspects that
require university cooperation.

Overall, I would not change my approach to risks in this project. If we are being honest, the worst that would have
happened if the project implementation had been a complete disaster is a lower awarded mark for the university project.
For a real world project, it is much simpler to identify risks because the exploration of what assumptions in the
project planning might be wrong or what outside events can disrupt the execution of the project do not have roots in the
fact that the university project is an arbitrary project and the university does not actually need our project.

It is difficult to draw the line between what we infer from the real world and what we pretend for the purposes of the
project. I made my best effort to plan and execute this project as if it was going to be used by the university from now
on.

## Future Work

Throughout the development of the project I identified several opportunities to dig deeper and directions to expand the
functionality and the scope of the project into. If the scope expansion seemed like it would work with the project's
timeline, I tried implementing it. However, most of the time, the ideas were potentially very deep and challenging, and
would likely justify a project on their own.

### Automatic Optimiser

A great example is the experimental automatic optimiser. This part of the system can be thought of as an embedded plugin
that works by loading data from the system, running an algorithm, and returning the results back to the system. It is
currently coded in TypeScript/JavaScript running in a browser, but it could be very simply be offloaded to a dedicated
program. It would, for example, be possible to create a c++ program that does the same type of work, except is much more
powerful. For this to happen, someone experienced with c++ would just need to load the system data from the system's
API, interpret them based on the existing type system, write a better algorithm than the current one is, and push the
result back using the API.

Another option related to the experimental automatic optimiser is to keep the component running in a browser, but change
the algorithm itself. With a more thought-out algorithm that could be biased in some decisions it would be possible to
achieve a great solve approximation in short time. Having deterministic algorithms requires more domain knowledge and
algorithmic complexity upfront, but yields more consistent and reliable results in this problem domain.

### Dynamic Data

The system is designed to be reactive and always display the newest changes of data in every step of the process, but
the implementation so far makes some assumptions in order to keep the project scope manageable. As the timetable
published by the university changes, it is currently not automatically reflected in the system. At the moment, the
administrator would be provided with alternatives for demonstrator assignments, but they would need to remember to make
the changes to the assignments. In practice from my own experience, this does not happen very often, and if it does, the
lecturer emails their demonstrators to let them know. It would, however, be a nice feature and could be used as another
layer of defence against human error.

### Integration With Other Systems

The university uses some systems built by its staff for the purpose of running lab sessions. Particularly, there exists
a system called "Lab Class Tracker", which allows student demonstrators to sign students off on their lab work, and
students to view their finished and remaining labs. The process of granting student demonstrators permissions to sign
students off could be automated by leveraging information in the student demonstrator manager system.

It is my understanding that the Lab Class Tracker does not currently distinguish between demonstrators and students on a
per-module basis. Instead, a user is either a student or a demonstrator. This leads to situations where a demonstrator
is not granted access to the system, because although they are a demonstrator for one module, they are a student for a
different module, and would therefore be in a conflict of interest in being able to sign themselves of. This problem
could also be addressed with the help of information provided by the student demonstrator manager.

### Manager User Interface

The user interface for the manage view has been created by incrementally adding features onto a scaffold rather than
designing a coherent user interface upfront. I see a great potential in reworking and expanding this part of the system.
Trying to use this system in production will produce important insights into the useful and the cumbersome parts of user
experience.

Changes could include the dispersal of the floating toolbox that often covers a large portion of the screen, or
alternative views, perhaps a view that focuses on modules being assigned to demonstrators rather than demonstrators
being assigned to labs. By observing how an administrator interacts with the interface and particularly what
assignment-related operations they perform, we could create a user experience that does not require the administrator to
switch contexts or to translate the intention to the way the system would understand it.

For example, an administrator might know that a postgraduate student is working in a concurrency field and therefore
they might want to assign this demonstrator to all modules related to concurrency. Currently, the administrator would
have to find each module's lab in the timetable, select all module labs and find the demonstrator name each time. A
better experience might be to find the student first, and then assign them to the selected modules. Afterwards the view
could be switched to address what times the demonstrator is available.

Another way of improving the user experience might be a drag-and-drop functionality for demonstrators. The administrator
would have a "bucket" of bricks, each representing one demonstrator. They would have a limited amount of these brick
accounting to the limited hours any demonstrator is allowed or expected to demonstrate. Upon grabbing a brick, the slots
representing modules or module flavours would change colour and/or shape to indicate whether the demonstrator being held
is suitable to be assigned to the particular module, module flavour, or the lab sessions.

Finally, the interface could use contextual menus and actions. For example right-clicking a lab would offer selecting
all labs in the module or module flavour. Right-clicking the selection could calculate the most suitable demonstrator to
be assigned to the selected labs. Common error-correcting actions could be defined and also available through the
context menus, such as substituting a demonstrator for just one lab session in case they are available for the rest.

## Picking Up This Project

If I was to pick up this project again in the future, the first thing I would do is collect usage data from a production
use of the system. This could be openly asking an administrator for feedback, asking them to rate specific parts of the
system, and observing them interact with the system. The most important first step is to understand what is slowing them
down. With this new insight I would start fixing issues with a low effort to benefit ratio that do not introduce
breaking changes.

If a different developer was to begin working on this system, I would recommend them to read the [manual](MANUAL.md) and
the [documentation](DOCUMENTATION.md), and to familiarise themselves with the system from the perspective of a user.
After that, a great step would be to [contact me](README.md#contact) to clarify any information and to consult
architectural constraints. With a clear goal in mind, it should not be difficult to begin extending any part of the
system as it has been kept strictly structured. There are no known limitations of the system that could not be overcome
or changed. That said, picking up any existing project is challenging, so starting with small improvements to different
parts of the system would be a great practice before setting to change the application's mechanisms.

## Evaluation Reflection

The project testing has its own [dedicated chapter](TESTING.md).

The evaluation was constrained by it not being possible to evaluate a production system, but only a version with mock
data. It was this project's stretch goal to perform a user study with real users and objectives, but during the
development of the project I realised that there was not enough time to build the system before demonstrator allocations
would be required by the university. We could have prioritised it despite that, and perhaps amend the scope of the
project to be able to perform the study, but it would still only evaluate the limited version of the project, likely
with an even less usable user interface. I am happy with the deliverables we had decided for.

My decision not to require high code test coverage allowed me to deliver a far greater scope than originally planned.
The downside is a lower confidence for potential other developers to make changes to the system. The truth is that,
having high test coverage would probably identify code errors causing regressions, but if the developers were unable to
consciously obey the principles of this application or if they were not comfortable with test-driven development, they
would introduce bugs and regressions regardless, but also be slower at development in the first place. Automated tests
are not a substitute for manual testing, rather, they are a tool to automate some of it, and the benefits can be seen
with large projects worked on by large or distributed teams. In all cases, it is impossible to capture every decision
and every justification for a project in documentation or a set of automated tests.

## Conclusion

I feel the project was of the right complexity and challenge. The milestones were being met and the project was
delivered in time with a greater scope than originally planned. Testing confirmed that the system fulfills all
requirements, even though some tests failed due to changes to the design. I reinforced my project management skills most
of all, and I consider this project a success.

The system is not perfect, but it is where it needs to be now, with a well-documented production system deployed and
potential perfective future work identified. It is ready to be used in production by the university for real
demonstrator assignments, and thanks to failsafe mechanisms such as being able to export data at each step of the way,
should the system not be able to complete its full journey, data can be downloaded in a spreadsheet format and
assignments finished the old way.
