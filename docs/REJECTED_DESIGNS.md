# Rejected Designs

## Java Application

We considered creating an application in Java with GUI created in JavaFX, which would make for a performant automatic
and interactive solvers. There exist libraries for reactive java, which would make it possible to use the same design
pattern as with ReactJS. This design was rejected, because Java does not have a suitable CMS library or framework that
is comparably simple to use like Sonata Admin, and we would still need an external way of gathering data from students
and lecturers.

## Optimiser written in c++

We considered writing the automatic optimiser in c++. The standalone c++ program would fetch the context data from a
REST endpoint, perform the optimisation extremely fast as c++ can have very low overhead in algorithmic uses and push
the results back using the REST api. There were several problems with this approach, mainly:

- We are not familiar with c++
- Lower portability than a solution in a browser
- Higher time requirement for the lower-level c++ development

## Using D3.js instead of ReactJS

After being recommended D3.js, we considered using it instead of ReactJS for rendering the timetable and the rest of the
user interface reactively. D3.js directly manipulates the DOM and thus performs well. The problem is, that logically it
does not abstract any complexity and state management must be explicitly maintained.

## Assignments As Adjacency Matrix

We considered storing assignments information in an adjacency matrix using the browser's native
low-lever [integer arrays](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int8Array)
instead of storing a Set of demonstrators inside each lab session. This would make the mutations and pertubations during
optimisation very fast. After implementing this, however, we found out that writing optimisation rules for this data
format was difficult and unintuitive, and keeping the data in an object form added negligible overhead.

## Administrator Tweaking Optimisation Weights

The original plan was to let the Administrator set a weight to each rule for the automatic optimisation. We hoped that
this would allow the automatic optimiser to be used on a differently constrained data than we developed it on. The
problem is, that our automatic optimiser uses a genetic algorithm, which is very general, and that means that the
optimisation does not reasonably have enough time to come up with a solution good-enough that the slight differences
between rule weights would make a significant difference. The automatic optimiser can therefore only be used for "
out-of-the-box" solution ideas for very constrained situations.

## Emailing Students Their Assignments

Originally, the plan was to assign students to lab sessions at the beginning of the year and then email them their
assignments for the rest of the semester. To utilise the flexibility of the system, however, we decided to instead
create a dynamic calendar URL for each demonstrator which they can import into their personal calendars and always see
the most up-to-date assignment information, even if it changes suddenly.
