# Manual

This page explains how to use the system from the perspective of administrators, lecturers and students.

## Navigation

- [For Administrators](#for-administrators)
    - [Admin Interface Overview](#admin-interface-overview)
    - [Dashboard](#dashboard)
    - [Modules Admin Page](#modules-admin-page)
    - [Module Flavours Admin Page](#module-flavours-admin-page)
    - [Timetable Entries Admin Page](#timetable-entries-admin-page)
    - [Module Configurations Admin Page](#module-configurations-admin-page)
    - [Demonstrator Applications Admin Page](#demonstrator-applications-admin-page)
    - [Demonstrators Admin Page](#demonstrators-admin-page)
    - [Manager](#manager)
        - [Timetable](#timetable)
            - [Demonstrator Violations](#demonstrator-violations)
            - [Lab Violations](#lab-violations)
            - [Day Violations](#day-violations)
            - [Week Violations](#week-violations)
            - [Global Violations](#global-violations)
        - [Toolbox](#toolbox)
            - [File Tab](#file-tab)
            - [Optimiser Tab (experimental)](#optimiser-tab-experimental)
            - [Manual Tab](#manual-tab)
- [For Lecturers](#for-lecturers)
- [For Students](#for-students)
- [Troubleshooting](#troubleshooting)
    - [Logging In](#logging-in)
    - [Viewing Logs](#viewing-logs)
    - [Still Having Issues?](#still-having-issues)

## For Administrators

This manual assumes that the [installation](/README.md#installation) has been successful.

If you wish to get the system running so that you could start collecting data and applications from lecturers and
students, you may refer to the [Quick Start Guide](/README.md#quick-start-guide) first.

If you wish to understand the system on a deeper level than to use it for its intended purpose, refer to
the [Documentation for Developers](docs/DOCUMENTATION.md).

### Admin Interface Overview

The administrator's interface consists of:

- [The dashboard](#dashboard) containing a list of actions in a suggested order
- Admin pages representing data in certain database tables
    - [Modules](#modules-admin-page) representing the module code
    - [Module Flavours](#module-flavours-admin-page) representing a student group for a module
    - [Timetable Entries](#timetable-entries-admin-page) representing a lecture, lab or other event in a timetable
    - [Module Configurations](#module-configurations-admin-page) representing details about labs entered by lecturers
    - [Demonstrator Applications](#demonstrator-applications-admin-page) representing students applying for the
      demonstrator position
    - [Demonstrators](#demonstrators-admin-page) representing the accepted student demonstrators
- [The Manager view](#manager) for assigning demonstrators to labs

Each page of the admin interface has a purpose and both the links on the dashboard and the links in the left panel with
categories displays a suggested order of operations. When using the system, make sure that all action items in the page
above are complete.

### Dashboard

![](images/manual_admin_dashboard.png)

The dashboard is the entrypoint into the rest of the admin interface. It displays a list of actions that should be taken
in the order suggested.

```text
1. Import timetable data (Timetable Data -> Modules -> Import Scraped Timetable)
```

Each list entry contains the name of the action (Import timetable data in example) and how to navigate to it without
using the direct link in the entry itself. Feel free to use either method of navigation.

### Modules Admin Page

![](images/manual_admin_modules.png)

The purpose of the module admin page is to verify that the imported modules have correct spelling and their year has
been determined correctly. Rows can be sorted by any column by clicking the column header.

This section should contain all modules from the timetable even if there are no lab sessions associated with the module.

In the top right corner a button for importing timetable entries can be found.

### Module Flavours Admin Page

![](images/manual_admin_module_flavours.png)

The purpose of the module flavours admin page is to verify spelling of the module flavours. Rows can be sorted by any
column by clicking the column header.

In order to search it's recommended to increase the per-page entries shown in the bottom right corner of the page to
display all entries on one page and then using the browser's built-in search functionality (e.g. Ctrl+F).

### Timetable Entries Admin Page

![](images/manual_admin_timetable_entries_1.png)

The timetable entries admin page serves for verifying the imported timetable entries and for looking up demonstrator
assignments for specific modules, module flavours or date ranges.

You can filter the timetable entries shown by:

- Timetable entry type (lab, lecture, other)
- Module
- Module Flavour
- Start date

![](images/manual_admin_timetable_entries_2.png)

By default, the type filter is set to only show labs and not lectures or other. The remaining filters are not active and
are hidden. You can bring them by clicking the "Filters" button in the top right and selecting the filters you wish to
use.

![](images/manual_admin_timetable_entries_3.png)

Eventually, when demonstrators have been assigned to labs, the assignments will be shown in the table for each lab
session. The data shown in the table **with the current filters applied** can be exported in several formats by clicking
the "Download" button and selecting a format.

![](images/manual_admin_timetable_entries_4.png)

The exported data will contain one column with the assigned demonstrator names and student numbers and one column with
just student numbers. The file may look like this:

```csv
type,start,duration,demonstratorsList,demonstratorsStudentNumbers
LAB,"Mon, 28 Sep 2020 10:00:00 +0000",PT2H,"4: Barton Bergnaum (631650), Sophie Towne (470106), Heber Hill DVM (873747), Gerry McClure (980567)","631650, 470106, 873747, 980567"
LAB,"Mon, 28 Sep 2020 10:00:00 +0000",PT2H,"2: Geo Koch (672690), Cordell Kling (298935)","672690, 298935"
```

![](images/manual_admin_timetable_entries_5.png)

In the top right of the page there is a "Manage" button that leads to the Manager view.

### Module Configurations Admin Page

![](images/manual_admin_module_config_1.png)

The module configurations admin page serves several purposes:

- Checking module configurations submitted by lecturers
- Amending, fixing and deleting module configurations submitted by lecturers
- Checking for missing or duplicate module configurations

The table shows module configuration entries created by lecturers submitting the lecturer survey form. Notice that the
table is wide and may not fit on the page all at once. It's possible to scroll the table sideways, for example by
clicking anywhere in the table and pressing the right arrow. On large screen sizes the whole table may be visible.

![](images/manual_admin_module_config_2.png)

Clicking the "Edit" button inline with a table entry will open the entry for editing:

![](images/manual_admin_module_config_3.png)

**Warning:** The editing view, unlike the lecturer survey form, does not validate the data. This is intentional in order
to give the administrator the option to override special cases that the system did not account for.

To delete one or more module configurations, select the entries to be deleted by checking the boxes in their left-most
column, select the "Delete" batch action in the bottom left and press OK.

![](images/manual_admin_module_config_4.png)

You will be asked to confirm the deletion:

![](images/manual_admin_module_config_5.png)

Since we deleted the 3 module configurations, they are now missing and will be listed in the "Lab modules with missing
configuration" section.

**Note:** You will not be allowed to enter the Manager view if any module is missing a configuration.

![](images/manual_admin_module_config_6.png)

In case a lecturer submitted a configuration for a module multiple times (which is encouraged in order to correct a
mistake), or a lecturer filled out a configuration for an incorrect module accidentally, multiple configurations may
exist for a module.

![](images/manual_admin_module_config_7.png)

You will be able to click the duplicate module name in the right box below filters, which will filter the configurations
list to those of the duplicated module you clicked.

![](images/manual_admin_module_config_8.png)

You will be able to decide which one to keep if any and delete the rest.

**Note:** You will be allowed to enter the Manager mode with duplicate configurations. In that case the system will use
the one created earliest.

### Demonstrator Applications Admin Page

![](images/manual_admin_demonstrator_applications_1.png)

The purpose of this page is to verify student demonstrator applications and either accept or reject them.

First you should consider uploading students' marks from previous years. This step is optional but strongly recommended
as this information will help during accept/reject decision and during assignment to labs. The marks need to be in a CSV
file, and must have a "Student Number" column and a column with marks for each module taken. Empty values and text will
be ignored. The file may look something like:

```csv
Student Number",CS-110,CS-230,CSCM04,...
963810,98,83,80,...
1011324,68,,67,...
615484,,,88,...
...,...,...,...,...
```

![](images/manual_admin_demonstrator_applications_2.png)

To upload this file click the "Import Students' Marks" button in the top right corner.

![](images/manual_admin_demonstrator_applications_3.png)

Select the file and click upload.

![](images/manual_admin_demonstrator_applications_4.png)

You will see a flash message saying the import was successful. Notice that if you scroll the table sideways or increase
the size of the browser window, you will see the matched marks for some students.

![](images/manual_admin_demonstrator_applications_5.png)

The table also shows what timeslots the students indicated they are not available and the reason they provided for it.
These timeslots each represent 1 hour, starting at 9am and finishing at 6pm (left to right), Monday to Friday (top to
bottom).

To approve or reject one or more applicants, select the rows using the checkbox in the left-most column and select the
appropriate batch action, then press ok. You will be required to confirm the batch action.

![](images/manual_admin_demonstrator_applications_6.png)

If you reject an application, its status will change to "Rejected". By default, a filter is applied to only show
applications with the "Pending" status, so if you wish to see rejected applications you must change or disable that
filter.

If you accept an application, its status will change to "Accepted". Furthermore, a
new [Demonstrator](#demonstrators-admin-page) entity will be created.

It **is** possible to accept a rejected application. It **is** also possible to reject an accepted application, which
will also result in the deletion of the associated Demonstrator.

**Note:** You will not be allowed to reject an accepted demonstrator if they are assigned to any labs.

There are filters available to simplify searching for a particular demonstrator application:

![](images/manual_admin_demonstrator_applications_7.png)

### Demonstrators Admin Page

![](images/manual_admin_demonstrators_1.png)

The demonstrators page is helpful as an overview of active demonstrators. When demonstrators are assigned to labs, this
page will also show the allocation summary for each demonstrator.

```text
CSC306: 106, CS-110: 24
```

The above example indicates that the demonstrator is assigned to a total of 106 labs for the CSC306 module and a total
of 24 for the CS-110 module.

![](images/manual_admin_demonstrators_2.png)

This list can also be exported by clicking the "Download" button in the bottom right corner.

![](images/manual_admin_demonstrators_3.png)

There are filters available to simplify searching for a particular demonstrator application:

![](images/manual_admin_demonstrators_4.png)

### Manager

![](images/manual_admin_manage_1.png)

The Manager view's purpose is to assign demonstrators to labs. It is an interactive solver that shows all labs that are
in the system in a time structure displaying demonstrators assigned to each lab. It is strongly recommended to use this
view on a large screen only.

The view consists of:

- The timetable as a scrollable pane of days that contain labs
- The Toolbox that hovers over the timetable and can be dragged around

![](images/manual_admin_manage_2.png)

#### Timetable

The timetable shows days as large boxes filled with content, stacked on top of each other. Each representation of a day
contains:

- The week number (starting on Mondays, not corresponding to the University's numbering)
- The date
- Row headers representing timeslots
- Labs depicted by boxes carefully positioned between row headers to indicate start and end times

![](images/manual_admin_manage_3.png)

If you hover over a lab's header, a tooltip showing details about the lab will appear. It will contain:

- Module Flavour
- Ration of postgraduate students required
- The number of demonstrators currently assigned
- The ideal number of demonstrators, requested by the lecturer
- Notes from the lecturer
- A number of points for demonstrator assignments (discussed later)
- A list of assignment violations (discussed later)

![](images/manual_admin_manage_4.png)

In the above example there are no demonstrators assigned, therefore there are no errors to be shown, but also no points
to be gained. The next example shows a lab with some correctly assigned demonstrators. Notice that the lab tooltip now
shows 4 assigned demonstrators and 824 points. Also notice that each demonstrator is represented by a box with their
initials inside.

**Note:** The concepts of points and violations will be thoroughly explained later. For now just remember points are
good and errors/violations are bad.

**Note:** You can also ignore the lock icons for now, they will equally be introduced later.

![](images/manual_admin_manage_5.png)

We can also hover over an assigned demonstrator to see the full name and their year.

![](images/manual_admin_manage_6.png)

There can also exist incorrectly assigned demonstrators, for example if they are busy during the timeslot or if they are
not senior-enough for that module. These violations can exist on several levels:

- [Demonstrator](#demonstrator-violations) (e.g. demonstrator has a clashing lecture)
- [Lab](#lab-violations) (e.g. lab does not have enough postgraduates assigned)
- [Day](#day-violations) (currently no rules in place)
- [Week](#week-violations) (e.g. visa students exceeding their weekly limit)
- [Global](#global-violations) (currently no rules in place)

The next 5 sections show how each of the violations will be indicated.

##### Demonstrator Violations

A demonstrator violation causes the demonstrator's box to turn red. The demonstrator's tooltip will also become red and
will list the violation message.

![](images/manual_admin_manage_7.png)

Notice that the lab containing the demonstrator with violations will not turn red, neither will its tooltip. It will,
however, not be awarded any points.

![](images/manual_admin_manage_8.png)

##### Lab Violations

Lab violations happen when no individual demonstrator causes a violation, but the collective assignment of all the
demonstrators for the lab causes a violation. This can happen when a lecturer requests a mixed ratio of postgraduate to
undergraduate students and the lab has too few postgraduates assigned.

All demonstrators' boxes will remain blue, but the lab header and tooltip will turn red and list the violation.

![](images/manual_admin_manage_9.png)

##### Day Violations

Day violations would happen if a rule existed which governed the assignment of demonstrators to labs, but violation of
which could not be pin-pointed to a particular demonstrator or a particular lab.

There are currently no such rules in place, but if they are added in the future, they will be shown in the date box.

![](images/manual_admin_manage_10.png)

##### Week Violations

Week violations happen when assignments during a week cause a violation that cannot be pin-pointed to any one day. This
happens, for example, when a postgraduate research student hits their weekly demonstrating limit.

You will notice that on the day shown, the demonstrator in question only has 1 assignment, however, on the following day
not shown on screen they have assignments that breach their weekly limit.

![](images/manual_admin_manage_11.png)

##### Global Violations

Global violations would happen if a violation could not be pin-pointed to any particular demonstrator, lab, day or week.
There are currently no such rules, but if they are added in the future, they will be displayed at the very top of the
page.

![](images/manual_admin_manage_12.png)

#### Toolbox

The toolbox contains controls to manipulate demonstrator assignments shown in the timetable that was explained in the
previous section. It consists of 3 tabs, and will be explained separately in the next sections:

- [File Tab](#file-tab)
- [Optimiser Tab (experimental)](#optimiser-tab-experimental)
- [Manual Tab](#manual-tab)

![](images/manual_admin_manage_13.png)

##### File Tab

The file tab is dedicated to input and output operations.

![](images/manual_admin_manage_14.png)

It contains a "Save assignments" button, which uploads the assignments currently in the timetable view to the server,
replacing any existing ones. Once uploaded, the changes are reflected immediately.

It also contains a "Clear assignments" button, which will clear all assignments in the timetable view, but not yet on
the server.

##### Optimiser Tab (experimental)

The experimental optimiser tab aims to help the administrator with assigning demonstrators to labs automatically. It is
only experimental and the purpose is to demonstrate where this project could be taken in the future and as a last resort
for very difficult assignments.

![](images/manual_admin_manage_15.png)

In order to facilitate automatic optimisation of any kind, a fitness/reward function was implemented. Each lab has an
ideal number of points, which is 1,000. This number of points is decreased if the module does not have enough
demonstrators or if it has too many of them. If the lab or its demonstrators cause any violations, the lab receives no
points. Furthermore, each error causes -1,000 points to be added to the total score.

The goal of the optimisation is to achieve the highest score possible. The optimisation process uses a genetic algorithm
where a population of 8 assignment sets is kept and randomly perturbed and mutated. Children replace parents if they
achieve a higher score. The simulation runs for a set number of generations and cannot be interrupted until it has
finished. The number of generations can be adjusted:

![](images/manual_admin_manage_16.png)

The rules and their impact can be seen in the "Score Function" table. Currently, all rules result in errors, but it is
possible to define rules that only decrease score. In order to add new or amend existing rules, you will need to refer
to the [documentation for developers](docs/DOCUMENTATION.md).

![](images/manual_admin_manage_17.png)

The initial state of the optimisation population can be chosen from 3 options:

- Current - uses the assignments currently loaded and visible in the timetable structure
- Biased - uses a hard-coded algorithm to initiate the assignments
- Empty - starts with no demonstrators assigned to any lab

![](images/manual_admin_manage_18.png)

The automatic optimisation can be started by clicking "Run Optimisation" in the bottom left corner. During the
optimisation, the progress of the simulation will be reported. You will be able to see how the score evolves inside
the "Results" box.

![](images/manual_admin_manage_19.png)

At the end of the optimisation, the result will be applied to the current timetable (but not uploaded to the server).

**Note:** Lab locks, which will be explained in the next chapter, also apply to the automatic optimiser. This will be
explained more thoroughly later, but by strategically locking labs that you like the assignments of, you will be able to
only run the optimiser on the remaining labs, where you wish for better assignments.

##### Manual Tab

The manual tab is the main way of assigning demonstrators to labs. It allows the administrator to iteratively assign
demonstrators to labs and play with the assignment options.

![](images/manual_admin_manage_20.png)

The box contains a list of all demonstrators in the system (not demonstrator applications). These demonstrators are
grouped by their study year, where all Master's students show up as year 4 and all Postgraduate students are displayed
as year 5. After their full name, you will see tags giving additional information about the demonstrator. The legend for
these tags is below the list.

![](images/manual_admin_manage_21.png)

Upon hovering over a demonstrator's name you will see how happy they indicated they are demonstrating for each lab
module. The system will also try displaying marks achieved in those modules if the marks data are present.

![](images/manual_admin_manage_22.png)

Before assigning a demonstrator to a lab, first select the lab to assign to. Notice that the demonstrators box has been
populated with more information.

![](images/manual_admin_manage_23.png)

- Some names are struck through, which indicates they are not available during the time of the lab.
- Year groups with a red label indicate that those students are not senior-enough for that module.
- Some students have a green checkmark after their name, which indicates that the lecturer for that module specifically
  requested and pre-allocated that student to the module. Note that it could happen that a lecturer pre-allocated a
  demonstrator to a lab but the student is not available during that time as the example shows. The demonstrator will
  then have a green checkmark and will be struck through.
- All students will display how happy they are demonstrating on the selected module by a number of stars from 1 to 5,
  with higher meaning happier.
- If marks for a student and the selected lab module are available, they will also be displayed inline.
- Visa students with a weekly demonstrating limit will be indicated by an orange V.
- Postgraduate research students with a weekly demonstrating limit will be indicated by an orange R.

![](images/manual_admin_manage_24.png)

To assign demonstrators to the selected lab, click their name in the box. Notice that their name will become highlighted
in the toolbox list and a box representing the demonstrator will be added to the selected lab.

![](images/manual_admin_manage_25.png)

To assign more demonstrators to the same lab, simply click more demonstrators in the toolbox list.

![](images/manual_admin_manage_26.png)

To remove a demonstrator simply click their name again, and they will be removed from the lab's demonstrator list.

![](images/manual_admin_manage_27.png)

Note that this assignment is only applied to the one selected lab. If we select a different lab, either at a different
time or of a different flavour/group, there will be no assignments selected.

![](images/manual_admin_manage_28.png)

It is possible and encouraged to assign demonstrators to multiple labs at a time. There are 5 modes of selection:

- Single Lab - only makes changes to the one selected lab
- All Flavour - makes changes to all labs of the same module flavour as the selected lab
- All Module - makes changes to all labs of the same module (including all flavours)
- All Day - makes changes to all labs on the same day
- All - makes changes to all labs

![](images/manual_admin_manage_29.png)

Different selection methods are useful for different purposes. You might start with assigning students to module
flavours by default, but for example remove them from one day when they are busy. You might also want to remove a
demonstrator from all assigned labs in case they do not comply with Swansea University's HR requirements.

The selection mode also applies to locks. Any lab can be locked by clicking the lock icon in the bottom left corner of
the lab box.

![](images/manual_admin_manage_30.png)

Upon clicking the lock, the selected lab will become locked.

![](images/manual_admin_manage_31.png)

When a lab is locked, its assigned demonstrators will not change. The lock applies to both the optimiser and the manual
mode. Notice that in the example below, the locked lab remained without assigned demonstrators even though we assigned a
demonstrator to all labs of the same module.

![](images/manual_admin_manage_32.png)

Lab locks also respect the selection mode. With "All Day" mode selected, locking one lab results in all labs in that day
becoming locked.

![](images/manual_admin_manage_33.png)

The recommended approach to demonstrator assignment is to start with an arbitrary module, select the module or the
module flavour mode, select a lab from that module, and in the toolbox find a demonstrator that:

- Is senior-enough (year group not red)
- Is available for all selected labs (name not struck through)
- Indicated they are happy to demonstrate (higher number of stars)
- Has a high-enough mark if available (blue 00%)

Priority should be given to students pre-allocated by the module lecturers, which are indicated by a green tick.

- Progressively lock modules or module flavours as they are completed
- If facing a complex assignment situation, consider locking all labs except for the problem group and trying the
  optimiser
- **Remember to save the assignments** by uploading them to the server on the File tab!

You are encouraged to [reach out](README.md#contact) to the authors of this system for help if you encounter any issues.

## For Lecturers

As a lecturer you will only need to interact with this system to submit details about modules you are responsible for.
You will do that by filling out a lecturer survey, which can be navigated to from the project's landing page.

![](images/manual_lecturer_survey_1.png)

You will be prompted to log in unless you are already authenticated. Enter your university email address and press "
Confirm."

![](images/manual_lecturer_survey_2.png)

You will receive an email with a one-time code. This code is only valid one time and expires in 1 hour.

![](images/manual_lecturer_survey_3.png)

Enter it into the login form and press "Confirm" again.

![](images/manual_lecturer_survey_4.png)

You will be redirected to the lecturer survey. You will be asked to enter your name and to select all modules that you
are responsible for.

![](images/manual_lecturer_survey_5.png)

After selecting your modules, for each of the modules selected, a section with details for that module appears. Fields
have descriptions and most of them should be self-explanatory.

![](images/manual_lecturer_survey_6.png)

The filled-out form may look something like in the example below.

![](images/manual_lecturer_survey_7.png)

After you have filled out details for all modules you are responsible for, click the "Submit" button on the bottom of
the page. You will be redirected back to the project landing page.

![](images/manual_lecturer_survey_8.png)

In case you forgot to fill out a module you are responsible for, simply complete the survey out again, only filling out
the forgotten module.

In case you submitted incorrect data, simply submit a new version and let the administrator know. All submissions are
saved, and the administrator will be able to delete all except for the one you state is correct.

The administrator will be able to generate lists of students assigned to your modules after the assignments have been
finalised.

## For Students

As a student, you will interact with the system when you are applying for the demonstrator position and when you are
checking what labs you have been assigned to. First you will need to fill out an application form, navigate to it from
the landing page.

![](images/manual_student_application_1.png)

You will be prompted to log in unless you are already authenticated. Enter your university email address and press "
Confirm."

![](images/manual_lecturer_survey_2.png)

You will receive an email with a one-time code. This code is only valid one time and expires in 1 hour.

![](images/manual_lecturer_survey_3.png)

Enter it into the login form and press "Confirm" again.

![](images/manual_lecturer_survey_4.png)

You will be redirected to the application form.

![](images/manual_student_application_2.png)

Fill out the form according to your specific situation. Some questions and fields will only be visible to some students,
depending on their circumstances. See an example below, but beware that yours may have different options and may be
shorter.

**Warning:** Example is for information purposes only! Do not copy anything from the example in your own application!

![](images/manual_student_application_3.png)
![](images/manual_student_application_4.png)
![](images/manual_student_application_5.png)
![](images/manual_student_application_6.png)

After successfully submitting your application, you will see a green flash message. Your application has been submitted
successfully, and now you need to wait for the university to reach out to you.

![](images/manual_student_application_7.png)

If you have been accepted as a demonstrator, and you have received a confirmation from the university, you will be able
to view your assigned labs. You can navigate to the overview page from the landing page.

![](images/manual_student_application_8.png)

You will see a list of labs that you have been assigned to. You will also see a calendar link which you can import into
your calendar and always see your up-to-date lab assignments.

**Warning:** If you share the link with others, they will be able to see your timetable!

![](images/manual_student_application_9.png)

## Troubleshooting

### Logging In

If during login you encounter errors, you should check if the `MAILER_DSN` is configured properly, instructions can be
found in the [Quick Start Guide](README.md#quick-start-guide). If this dies not help, continue
to [Viewing Logs](#viewing-logs).

### Viewing Logs

To view the application logs, execute the following commands in the directory where the project is set up:

```bash
docker-compose exec app ash
cd var/log/
ls
```

This will list all log files available. You can read the log files by opening them in `less` or `tail`:

```bash
less prod.log
# or
tail prod.log
```

### Still Having Issues?

As a last resort, you should consider checking the [documentation for developers](DOCUMENTATION.md). It explains
concepts that are being used in the system and may be useful to your troubleshooting.

You may also consider reaching out to the [authors of the system](README.md#contact).
