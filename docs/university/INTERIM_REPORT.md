# Student Demonstrator Manager: Interim Report

| Petr Hoffmann | 963810 |
|---|---|

This report explains what progress has been made on the Student Demonstrator Manager project.



## Narrative

Since the Initial Document has been published at the beginning of December, we have
been working intensively on this project. We started with the planned development of
the high-priority parts of the system. These included improving the data model from
the prototyping phase, processing the students' and lecturers' form submissions and
persisting them in a database as well as the creation of the administrator's interface,
which is treated as a subsystem due to its complexity.

During the planned high-priority development we discovered many feature dependencies
that only came to light when working on those parts of the system. This was expected,
and our Kanban approach handled this well. We remained working on the high-priority
features, but we descended to work on the dependencies and their siblings in order to
work efficiently and not to introduce technical debt.

We have divided the administrator's interface into 2 subsystems to separate concerns
and adhere to best practices. One subsystem handles the data processing and
verification such as importing scraped timetable data, viewing and approving student
demonstrator applicants or checking the lab module configurations entered by lecturers.
The second subsystem is dedicated to assigning student demonstrators to lab sessions
and includes the automatic optimiser. This split was planned and works well.

We found out that most of the low-priority tasks were dependencies of the optimisation
subsystem and therefore it was not possible to evaluate the high-priority features
meaningfully before finishing the remaining development. We had planned for this case,
and the timeline was amended to reflect this.

One of the dependencies was manufacturing fake data for development purposes. Due to
the specific shape and complexity of the data we needed the generated fakes to
resemble roughly the distribution of the real data. We used real data from previous
years stripped of all identifying information to generate accurate fakes.

After we resolved the dependencies, we started working on the optimisation subsystem.
We had chosen JavaScript for implementing the optimisation algorithm and React Native
for the graphical interface. As we started implementing these, it became clear the shape
and the complexity of the data made it too difficult and error-prone to continue using
JavaScript, so we switched to TypeScript. TypeScript brings static typing to JavaScript
which solved our problem.

When the first version of the optimisation algorithm was implemented, we discovered many
improvements that could be made to it. We added them all to the backlog except for one,
which we decided to implement immediately. This improvement was multi-threading, which
we achieved using the Web Worker API and which improved the performance of the algorithm
5-10 times, depending on the machine's performance, particularly the number of cores
and the memory speed. We also discovered that the optimisation problem was not
constrained enough and the solving times in units of hours could not be considered
satisfactory and therefore we plan an update to the algorithm later in the
development. This was expected and the system is designed to operate in an assistance
mode where the automatic optimiser is suggesting assignments to the human optimiser who
locks the assignments that they are happy with and asks the automatic optimiser for
suggestions to the more constrained problem.

We encountered issues with signing users in using the Swansea University's Microsoft
accounts. The application implemented the OAuth protocol, which is designed precisely
for our scenario where an authority (such as Swansea University) allows untrusted
vendors (such as us) to authenticate users with a limited scope using the authority's
user accounts. We implemented and tested our authentication system against other
authorities such as `microsoft.com` and confirmed it worked. Swansea University,
however, has disabled this functionality and denied our request for our application
being whitelisted. They did not provide a satisfactory reason and refused to
communicate further. We planned for this case and executed our backup plan, which was
to implement an authentication system using emails sent to the `@swansea.ac.uk` domain
containing one-time temporary passwords.

We have finished all development except for the optimiser subsystem, which needs
tweaking. We have tested whether the system covers all user stories and previewed
the system with the project supervisor. Some minor issues were found and will be
addressed before the next evaluation. These issues can be found in the section
**Feedback**.

We have packaged our application to be easily deployable using a Docker Compose array.
We have demonstrated this by deploying our system to a dedicated server which the
project supervisor interacted with during testing. More information about this
server can be found under section **Demo**.



## Work Remaining

### Legend

| Term | Explanation |
| --- | --- |
| WIP | The requirement is currently being worked on |
| Finished | The requirement has been fulfilled and does not need any further work |
| MVP Finished | The requirement has been met but the quality can be improved |
| Backlog | The requirement has been placed in a backlog and will be addressed if there is spare time |
| Equivalent Finished | The requirement has not been met but its purpose has been fulfilled |

### Requirements

| Requirement | Status | Description |
| --- | --- | --- |
| R01 | Finished | The system can obtain a list of modules and lab sessions from the public timetable |
| R02 | Finished | The system can determine the assigned lecturer for each module |
| R03 | Finished | The system can collect lecturers’ requirements and preferences about student demonstrators to be assigned to their modules |
| R04 | Finished | The system can collect students demonstrators’ preferences about their assigned modules and time availability |
| R05 | Finished | The system can determine when student demonstrators are busy with their own lectures |
| R06 | Finished | The system only allows student demonstrators to demonstrate in lab sessions for lower year students |
| R07 | MVP Finished | The system can visualize all lab sessions and the assigned students |
| R08 | WIP | The system asks the optimiser for constraints and associated weights or parameters |
| R09 | MVP Finished | The system can automatically optimise the student demonstrator assignment based on the specified constraints |
| R10 | WIP | The optimiser can intervene during the optimisation by locking particular demonstrator assignments |
| R11 | WIP | The optimiser can change the constraints or their weights or parameters during optimisation |
| R12 | MVP Finished | The optimisation result can be exported in formats that allow lecturers and demonstrators to verify they are comfortable with the generated assignments |

### Stretch Requirements

| Stretch Requirement | Status | Description |
| --- | --- | --- |
| S01 | Backlog | The system is able to react to timetable changes by suggesting new assignments |
| S02 | Backlog | The system can suggest substitutions for unavailable student demonstrators |
| S03 | Finished | Lecturers can authenticate using their Swansea University account |
| S04 | Equivalent Finished | The system can notify students by email with the summary of their assigned lab sessions |



## Risks

### Risks Encountered

Of the project management risks the following have happened that were accounted for
and mitigated by their mitigation strategies.

| Risk ID | Description | Likelihood | Impact |
| --- | --- | --- | --- |
| S8 | University bureaucracy halts progress | High | Severe |

### Likely Risks

The following project-specific risks may still occur:

| Risk ID | Description | Likelihood | Impact |
| --- | --- | --- | --- |
| S2 | Poor optimiser performance | High | Moderate |
| S3 | Input data too large | Low | Critical |
| S6 | Wrong results produced | Medium | Critical |

### New Risks

No new risks have been identified.

## Timeline Adherence

We have committed more time to this project and thus the amount of work done so far
exceeds expectations. We have been able to expand the scope of some features and
implement some stretch goals while keeping the technical debt at minimum.

Our plan is to finish the development of the optimiser view and fix high priority
issues. Then we will perform user testing with the lecturer currently responsible
for assigning demonstrators and if there is spare time, we will perform a case study.
Near the end, we will finalise project documentation and hand the project over.
We will be supporting the project during the transition period until the end of
the university term.

See the current projected timeline below:

![](http://116.203.37.141:8080/png/ZPBBJiCm44Nt-OhPiQhK1du0bIuHYCSgoc9gJsceOrkUIKl-7kTLd28UtiwzrsSUhD0Gv9aFxfsK00i6OSYN-Mgnp1UhvLJXNdWYEfWsouexEf9nlYOh9HZawBdfgvn1VCNBdk31IrLNJ07MhlREzbfaACIfoNITcZOVtWMfx1je51oqf2VfgfS2MZrHeWnPLmnaTQJQ7D4qA9MpPQf9OGTjdpB4vYD1SMjgxAf9HWU0z3j011p_BzR854kDRWUhuhcLBxqn-yn-ZvKLicWLpU1ZoC_CpoqBrI0dmfY-ZVzhS1PsH7gBwb3EzPVVdwEpuhLRKrVMT0JjLDCDAvc2_bhPds5PyJXUqnD9S4E03xEkz7bjILlZb6B0EbHInOi3ZxAFxQ0qWHW2eMvXvm88SNSe9UpHUxBVRY5kq_pRbwnm8bVEAjDmttygZMqCqay0)

## Feedback

We have tested the system with the project supervisor. The supervisor used his
computer to interact with a deployed version of the system, which was pre-populated
with fake data.

We run through all happy paths defined by the user stories and opportunistically tested
error handling. We identified the following issues:

| Issue ID | Description | Action |
| --- | --- | --- |
| I01 | It would be more intuitive and useful to display module configuration duplicates inline rather than just in the summary. | TODO |
| I02 | The student email addresses in the "demonstrators approached" field should be validated more strictly. | TODO |
| I03 | The demonstrator view in the admin panel should display all distinct modules that the demonstrator is assigned to. | TODO |
| I04 | The system should handle student names that only consist of one word. | TODO |
| I05 | The admin panel should offer filters to more easily find desired rows. | TODO |
| I06 | The fake data sometimes contains corrupted marks entries. | Backlog |

**Legend**

| Action | Explanation |
| --- | --- |
| TODO | The issue will be addressed before the evaluation |
| Backlog | The issue has been placed in the backlog and will be addressed if there is spare time |

Overall the system is in a great shape.

## Demo

The deployed system is currently available at [`https://sdm-prod.hoffic.dev/`](https://sdm-prod.hoffic.dev/).
Swansea University lecturers can interact with the student and lecturer forms after they have logged in.
For privacy reasons only selected lecturers have access to the administrator panel. You can see parts of the
inaccessible interface on the images below:

![Admin Home](images/admin_home.png)

The admin home screen is reminding the human optimiser what actions need to be taken and in what order.

![Admin Timetable Entries](images/admin_timetable_entries.png)

The timetable entries section lets the human optimiser view all labs and lectures and what student demonstrators
are currently assigned.

![Admin Module Configurations](images/admin_module_configurations.png)

The module configurations section lets the human optimiser view module configurations submitted by lecturers
and amend them if necessary.

![Admin Demonstrator Applicants](images/admin_demonstrator_applicants.png)

The demonstrator applicants section allows the human optimiser to verify and accept or reject the student
demonstrator applications.

![Admin Optimiser](images/admin_optimiser.png)

The optimiser view is where the human optimiser works on assigning student demonstrators to lab sessions
with the help of the automatic optimiser. The interface currently shows the list of hard rules and the fitness
of solutions produced by an organic algorithm.

![Admin Demonstrator Calendar](images/demonstrator_calendar.png)

The demonstrator calendar is a section where accepted student demonstrators can view their assigned lab sessions
and obtain a calendar URL to import into their calendars to get up-to-date assignments.
