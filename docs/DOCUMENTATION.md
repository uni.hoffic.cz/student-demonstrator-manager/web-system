# Documentation for Developers

This document is intended for developers looking to change how the system works, fix issues, and for advanced users.

## Navigation

- [Architecture](#architecture)
    - [Architecture - Database](#architecture---database)
    - [Architecture - Reverse Proxy](#architecture---reverse-proxy)
    - [Architecture - Application](#architecture---application)
- [CI / CD Pipeline](#ci--cd-pipeline)
- [Application Structure](#application-structure)
- [Manage View](#manage-view)
  [Web Workers](#web-workers)
- [Fake Data](#fake-data)
- [Authentication](#authentication)
- [Timetable Entries File Structure](#timetable-entries-file-structure)
- [Clearing All Data](#clearing-all-data)

## Architecture

This system is encapsulated in a docker compose array for better portability and dependency management. There are 3
containers inside the docker compose array that are connected together by an internal network bridge, which allows
secure communication between containers. The entrypoint into the application is the NGINX reverse proxy, which exposes
an encrypted port 443 and forwards requests to the app container. All containers are configured to start automatically
with the system and to restart on failure.

```plantuml
@startuml
left to right direction

package "Docker Compose Array" #lightblue {  
  frame "Reverse Proxy Container" as a_proxy #lightgreen {
    frame "NGINX" #white {
      [443:8080 passthrough]
    }
  }
  
  frame "Application Container" as b_app #lightgreen {
    frame "PHP/Symfony Web App" #white {
      
      [Landing Page]
      [Lecturer Survey]
      [Demonstrator Application Form]
      [Demonstrator Assignments Overview]
      
      frame "Sonata Admin CMS" #orange {
        frame "ReactJS Module" #plum {
          [Manager View]
        }
        
        [Modules Admin]
        [Module Flavours Admin]
        [Timetable Entries Admin]
        [Module Configurations Admin]
        [Demonstrator Applications Admin]
        [Demonstrators Admin]
      }
    }
  }
  
  frame "Database Container" as c_db #lightgreen {
  	database "PostgreSQL Database" #white {
      [Modules]
      [Module Flavours]
      [Timetable Entries]
      [Demonstrator Applocations]
      [Demonstrators]
      [Demonstrator Assignments]
      [Module Configurations]
      [Student Mark Records]
      [Login Tokens]
    }
  }
}
@enduml
```

### Architecture - Database

The database container is an official pre-built PostgreSQL image based on the small-footprint alpine
linux: `postgres:12.4-alpine`. Database data is stored on an internal volume in the docker array. The development
configuration allows connections on port `5432`, but the production configuration does not expose any ports outside the
internal network.

### Architecture - Reverse Proxy

The reverse proxy container is an official NGINX pre-built image also based on the small-footprint alpine
linux: `nginx:1.19-alpine`. This container only exposes the encrypted 443 port both in the production and the
development configurations. Using an encrypted connection is not just a good practice, but also necessary for OAuth 2.0
to work. This container is given read-only access to the configuration file, and the certificate public and private key
pair, which are stored on the host machine.

Certificate generation is almost fully automated. The certificate authority is [Let's Encrypt](https://letsencrypt.org/)
and the tool for obtaining the certificates is [Certbot](https://certbot.eff.org/) running in a docker image outside the
docker compose array. Generated certificates are stored on the host machine and passed to the reverse proxy as read-only
references.

### Architecture - Application

The application container is based on the alpine linux base image, but is described in a Dockerfile that builds the
image from the latest source code and contains several build stages. Each build stage serves a different purpose.

```plantuml
@startuml
state "Alpine Linux" as a #lightblue
a : Alpine linux for a small fingerprint.

state "Base Stage" as b
b : Install PHP, NGINX and Supervisor.
b : Configure PHP, NGINX and Supervisor.
b : Configure container heart beat check.

state "Dev Stage" as c
c : Set environment to DEV.
c : Install Composer, NodeJS, Yarn and XDebug.
c : Configure XDebug.
c : Disable heart beat check.

state "Build Stage" as d
d : Set environment to PROD.
d : Install Composer, NodeJS, Yarn and XDebug.
d : Install Composer dependencies.
d : Install NodeJS dependencies.
d : Build assets.
d : Run tests.

state "Prod Stage" as e
e : Set environment to PROD.
e : Copy built and warmed-up application from build stage.

a -down-> b : Continue into
b -down-> c : Continue into
a -down-> d : Continue into
b -down-> e : Continue into
d -down-> e : Copy files
@enduml
```

The Base stage prepares the environment for the application by installing and configuring dependencies and configuring
the container. This stage contains only the required runtime environment.

The Dev stage installs development tools on top of the runtime environment from the Base stage and configures the
environment for development. This stage does not contain the application source code or a built version of the
application. Instead, on the developer's machine, the project root directory is mounted inside the container so that the
container operates on the developer's project directory.

The Build stage installs build and verification tools and sets the PROD environment. The application is built and
warmed-up in order to run tests and to save time during deployment.

The Prod stage is based on the Base stage in order to only bundle the necessary runtime environment and the built and
warmed-up application is copied over from the Build stage without requiring to copy any of the build tools that would
increase the final artifact size dramatically.

Secrets such as mailer configuration or OAuth tokens are passed to the containers using environmental variables. On
server and during development these can be defined in `docker-compose.yml`.

## CI / CD Pipeline

The project has a CI/CD pipeline set up, which will take the committed code and complete all steps in verification and
preparation for deployment. It is also possible to automatically deploy all new changes to the application server if it
is configured to run the `update.sh` script automatically.

```plantuml
@startuml
partition "Developer" #lightblue {
  start
  :Commit and push to git repository;
}

partition "CI / CD Pipeline" #orange {
  partition "Build app container to Prod stage" #white {
    if (Construct environment) then (Success)
      if (Install dependencies) then (Success)
        if (Build assets) then (Success)
          if (Run tests) then (Success)
          else (Failed)
            stop
          endif
        else (Failed)
          stop
        endif
        note left
          Building assets includes
          JavaScript/TypeScript.
        endnote
      else (Failed)
        stop
      endif
    else (Failed)
      stop
    endif
  }
  
  :Save build artifacts;
  :Push the built image to the container registry under "test" tag;
  
  if (Master branch?) then (Yes)
  else (No)
    stop
  endif
  
  :Run SonarQube static analysis;
  note right
    Could be set up to fail the build if the code
    quality falls below a threshold, but currently
    not in place due to insufficient code test
    coverage.
  endnote
  :Push the built image to the container registry under "release" tag;
}

partition "Administrator" #lightgreen {
  -[dotted]-> Run ./update.sh on server;
  :Pull latest container images from the registry;
  :Restart the docker compose array;
  :Execute database migrations;
  end
}
@enduml
```

Running and past pipelines can be seen
in [GitLab Pipelines](https://gitlab.com/uni.hoffic.cz/student-demonstrator-manager/web-system/-/pipelines). Static
analysis report can be seen on [SonarCloud](https://sonarcloud.io/dashboard?id=uni.hoffic.cz_web-system).

## Application Structure

The application has been built in PHP using [Symfony framework version 4.4 LTS](https://symfony.com/doc/4.4/index.html).
The landing page and the forms for lecturers and students have been created using components offered by the framework.
The administrator interface was created in
the [Sonata Admin CMS](https://sonata-project.org/bundles/admin/3-x/doc/index.html), which is a Symfony extension. The
Manage view is a standalone [ReactJS](https://reactjs.org/) app that is embedded inside of one of the Sonata Admin
pages. The data from Sonata Admin is being passed to the ReactJS app by html data attributes, passing data back is
implemented using REST http requests.

The diagram below shows which parts of the system users will interact during a journey through the system.

```plantuml
@startuml
|Symfony Page|
|Sonata Admin CMS|
|ReactJS App|

|Sonata Admin CMS|
start
fork
  :Import Timetable Entries;

  |Symfony Page|
  fork
    :Lecturers submit
    module configurations
    for modules they are
    responsible for;
    |Sonata Admin CMS|
    :Admin reviews
    module configurations;
  |Symfony Page|
  fork again
    :Students submit their
    demonstrator applications;
    |Sonata Admin CMS|
    :Admin reviews
    demonstrator applications;
  endfork
fork again
  :Upload student marks;
endfork

|ReactJS App|
:Assing demonstrators
to labs;

split
  |Symfony Page|
  :Demonstrators view their
  assignments and calendar;
  detach
split again
  |Sonata Admin CMS|
  :Export demonstrator
  assignments for lecturers;
  detach
endsplit
@enduml
```

The Sonata Admin integrates fully with the Symfony framework and resides in the same sources directories as the rest of
the Symfony application. The ReactJS is a standalone reactive app written in TypeScript and compiled to JavaScript. The
compilation happens during the build process during which SCSS is compiled to CSS and TypeScript is compiled to
JavaScript. See the project directory structure below.

```text
.                               # Root directory, configuration files
├── assets                      # Project's raw assets
│   ├── images                  # Original images (unprocessed)
│   ├── js                      # JavaScript/TypeScript source root
│   │   ├── consts              # Business logic constants
│   │   ├── models              # Business objects (Modules, Timetable Entries, ...)
│   │   ├── optimisation        # All code related ot automatic optimisation
│   │   ├── processing          # Algorithms and helper functions
│   │   │   ├── goals           # Definitions for goals
│   │   │   └── rules           # Definitions for rules
│   │   ├── react-components    # ReactJS UI components
│   │   ├── state               # Helpers for state management
│   │   ├── types               # TypeScript type definitions
│   │   ├── typings             # TypeScript definitions for external code
│   │   ├── util                # State-less, utility code
│   │   └── worker              # WebWorker wrappers
│   ├── sim                     # Aids for generating fake data
│   └── styles                  # SCSS style definition
│       ├── components          # Reusable style components
│       └── themes              # Reusable component themes
├── bin                         # Executable framework files
├── config                      # Framework configuration files
├── docs                        # Project documentation (what you're reading now)
├── migrations                  # Automatic database migration definitions
├── ops                         # System operations files
│   ├── dev                     # Scripts for local development
│   ├── docker                  # Docker image definition and configuration files
│   └── server                  # Server configuration files
├── public                      # Web server directory root
│   ├── build                   # Built assets
│   └── bundles                 # Generated Symfony code
├── src                         # Symfony sources root
│   ├── Admin                   # Admin interface UI definitions
│   ├── Console                 # Console commands (console entry points)
│   ├── Controller              # Application web entry points
│   │   └── Security            # Entry points for authentication methods
│   ├── Encoding                # Object graph serialisation algorithms
│   ├── Entity                  # Objects mapping the database schema (ORM)
│   ├── Enum                    # Enumeration types
│   ├── Form                    # Web form definitions
│   │   └── Constraint          # Form field constraint definitions
│   ├── FormEntity              # Data transfer objects for web forms
│   ├── Processing              # Business logic algorithms
│   ├── Repository              # Database access abstraction objects
│   ├── Security                # Authentication logic
│   ├── Twig                    # Page rendering extensions
│   └── Util                    # State-less helpers
├── templates                   # Dynamic templates for HTML rendering
│   ├── admin                   # Templates for the admin UI
│   │   ├── action              # Templates for admin page actions
│   │   ├── action_button       # Templates for admin action buttons
│   │   ├── list_field          # Templates for admin table fields
│   │   └── template            # General Sonata Admin templates
│   ├── email                   # Dynamic templates for email rendering
│   ├── page                    # Page level (full) templates
│   ├── partial                 # Templates for parts of pages
│   └── template                # General theme templates
├── tests                       # PHP unit and E2E tests
└── translations                # Static text translations (not in use)
```

The system data is persisted in a PostgreSQL database running in a separate container. It is accessed through Doctrine
ORM abstraction layer which allows us to treat database entities as objects inside repositories. Data that needs to be
passed to the Manage view ReactJS app is serialised into JSON, and is described by a TypeScript schema.

The entity relations are shown in a diagram below. Red and blue arrows indicate explicit and implicit relation. Text
after a tilda (~) indicates the data type that is being represented by the column.

```plantuml
@startuml
entity module {
  * id : integer
  --
  * name : varchar(255)
  * year : integer
}


entity module_flavour {
  * id : integer
  --
  * **module_id** : integer
  * flavour : varchar(255)
}
module ||--o{ module_flavour : module_id : id

entity module_configuration {
  * id
  --
  * **module_id** : integer
  * lecturer_name : varchar(255)
  * lecturer_email : varchar(255)
  * number_of_students : integer
  * flavour_student_numbers : text ~ array
  * students_approached : text ~ array
  * ratio_of_postgraduates : varchar(255) ~ Enum
  * notes : text
  * date_created : timestamp(0) ~ DateTime
}
module ||--o{ module_configuration : module_id : id

entity timetable_entry {
  * id : integer
  --
  * **module_flavour_id** : integer
  * start : timestamp(0) ~ DateTime
  * duration : varchar(255) ~ DateInterval
  * type : varchar(255) ~ Enum
}
module_flavour ||--o{ timetable_entry : module_flavour_id : id

entity demonstrator {
  * id : integer
  --
  * full_name : varchar(255)
  * student_number : integer
  * module_preferences : text ~ array
  * university_email_address : varchar(255)
  * year : integer
  * postgraduate_research : boolean
  * modules_taken : text ~ array
  * timeslots_available : text ~ array
  * share_key : varchar(255)
}

entity demonstrator_allocation {
  --
  * **timetable_entry_id** : integer
  * **demonstrator_id** : integer
}
timetable_entry ||--o{ demonstrator_allocation : timetable_entry_id : id
demonstrator ||--o{ demonstrator_allocation : demonstrator_id : id

entity demonstrator_application {
  * id : integer
  --
  * full_name : varchar(255)
  * student_number : integer
  * module_preferences : text ~ array
  * university_email_address : varchar(255)
  * year : integer
  * postgraduate_research : boolean
  * visa_student : boolean
  * modules_taken : text ~ array
  * timeslots_available : text ~ array
  timeslots_unavailable_reasons : text
  * date_created : timestamp(0)
  * action : varchar(255) ~ Enum
}
demonstrator ||--o{ demonstrator_application #blue : university_email_address : university_email_address

entity student_mark_record {
  * id : integer
  --
  * student_number : integer
  * marks : text ~ array
}
student_mark_record |o--o| demonstrator : student_number : student_number
student_mark_record |o--o| demonstrator_application : student_number : student_number

entity user {
  * id : integer
  --
  * email : varchar(255)
  * roles : json
}
demonstrator |o--|| user #blue : email : university_email_address

entity login_token {
  * email : varchar(255)
  --
  * one_time_code : varchar(255)
  * created_at : timestamp(0) : DateTime
}
user ||--o| login_token #blue : email : email
@enduml
```

The data-accompanying business logic is structured according to Symfony's best practices. For more information refer to
the directory structure above or Symfony documentation.

## Manage View

The manage view is a ReactJS application embedded inside a Sonata Admin view. It is made up of functional and stateful
components. See the component hierarchy below.

```plantuml
@startuml
skinparam componentStyle rectangle

component Manager {
  note as a
    **State:**
    - assignments
    - selectedLab
    - lockedLabs
    - mode
  endnote
  
  component Toolbox {
    note as b
      **State:**
      - running
      - progress
      - fitnessSeries
      - initMode
      - generations
    endnote
    
    component FileTab {
    }
    
    component OptimiserTab {
    }
    
    component ManualTab {
    }
  }
  
  component Score {
  }
  
  component Day... {
    component TimetableEntry {
      component Demonstrator {
      }
    }
  }
}
@enduml
```

Components explicitly pass some state variables to their children and the ReactJS library ensures that they are
appropriately updated and re-rendered when the state changes. Any change of data and the consequential rendering is a
result of state mutation first.

In the example below, the Administrator clicks a demonstrator in a toolbox to assign them to the selected lab.

![](images/doc_react_1.png)

When the Admin clicks the demonstrator, an onclick handler calls a `toggleDemonstrator()` method that calculates all
labs that are selected based on selection mode and sets a new assignments state which contains the newly assigned
demonstrator. This causes the component tree to recursively update and re-render. The result is displayed to the user.

```plantuml
@startuml
actor Admin
participant Manager.tsx
participant Toolbar.tsx
participant demonstrator_toggle.ts
participant lab_affector.ts

Admin -> Toolbar.tsx : Click demonstrator name
Toolbar.tsx -> demonstrator_toggle.ts : toggleDemonstrator()
demonstrator_toggle.ts -> lab_affector.ts : calculateAffectedLabs()
return
demonstrator_toggle.ts -> Manager.tsx : setAssignments()
Manager.tsx -[#blue]-> Manager.tsx : Update and re-render
Manager.tsx -[#blue]-> Toolbar.tsx : Update and re-render
Manager.tsx -[dashed]-> Admin : Display
Toolbar.tsx -[dashed]-> Admin : Display
@enduml
```

The result is a new demonstrator assigned to the lab session.

![](images/doc_react_2.png)

In case of the automatic optimiser, the reactivity part is the same, except the new assignments are calculated in a
long-running algorithm and are asynchronously offloaded to a WebWorker running on a separate thread. The main thread and
the worker communicate using messages as a substitute to asynchronous methods and returning results. The optimiser also
reports progress, which is, however, not shown in the diagram for simplicity.

```plantuml
@startuml
actor Admin
participant Manager.tsx
participant Toolbar.tsx
participant OptimiserWorker.ts
participant optimiser.ts
participant parent_selection.ts
participant pertubation.ts
participant mutation.ts
participant ScoreCalculatorWorker.ts
participant score_calculator.ts

Admin -> Toolbar.tsx : Click "Run Optimisation"
Toolbar.tsx -> Manager.tsx : setRunning(true)
Toolbar.tsx -> Manager.tsx : setProgress(0)
Toolbar.tsx -> Manager.tsx : setFitnessSeries([])
Toolbar.tsx -[#green]-> OptimiserWorker.ts : message "run"
Manager.tsx -[#blue]-> Manager.tsx : Update and re-render
Manager.tsx -[#blue]-> Toolbar.tsx : Update and re-render
Manager.tsx -[dashed]-> Admin : Display
Toolbar.tsx -[dashed]-> Admin : Display
OptimiserWorker.ts -[#green]-> optimiser.ts : optimise()

group Optimisation flow (simplified)
  optimiser.ts -[#green]-> parent_selection.ts : selectParents()
  return
  optimiser.ts -[#green]-> pertubation.ts : pertube()
  return
  optimiser.ts -[#green]-> mutation.ts : mutate()
  return
  optimiser.ts -[#green]-> ScoreCalculatorWorker.ts : message "run"
  ScoreCalculatorWorker.ts -[#green]-> score_calculator.ts : calculateScore()
  return
  ScoreCalculatorWorker.ts -[#green]-> optimiser.ts
end

optimiser.ts -[#green]-> OptimiserWorker.ts
OptimiserWorker.ts -[#green]-> Toolbar.tsx : message "result"

Toolbar.tsx -> Manager.tsx : setRunning(false)
Toolbar.tsx -> Manager.tsx : setAssignments()
Manager.tsx -[#blue]-> Manager.tsx : Update and re-render
Manager.tsx -[#blue]-> Toolbar.tsx : Update and re-render
Manager.tsx -[dashed]-> Admin : Display
Toolbar.tsx -[dashed]-> Admin : Display
@enduml
```

The optimisation algorithm can be seen below.

```plantuml
@startuml
start
if ("Current" init mode) then (Yes)
  :Take the current assignments and
  make copies to form the population;
elseif ("Biased" init mode) then (Yes)
  partition "Biased Init" {
    :Assign students pre-allocated by lecturers;
    while (for each Module Flavour) is (do)
      :Find demonstrators who are available
      during all of the module flavour's labs;
      :Filter for demonstrators who do not
      violate lab requirements;
      :Order them from highest indicated delight
      for demonstration on the module to lowest
      and then from the lowest year students up;
      :Take up to first N and assign them to
      all labs of the flavour, where N is the
      target number of demonstrators;
    endwhile
  }
else ("Empty" init mode)
  :Initialise the population with empty
  assignments;
endif

while (for N number of generations) is (do)
  partition "Optimisation Round" {
    :Select 2 parents with a probability exponential to their fitness;
    :Generate children by pertubing parents by a uniform crossover;
    :Mutate children by randomly adding or removing demonstrators;
    :Calculate fitness for all children;
    :Add the N children to the population and remove N members with the lowest fitness;
  }
endwhile

:Return the member (assignments) with the highest fitness;
@enduml
```

Score is calculated from the number of errors and from the number and the quality of demonstrators assigned. Rules that
yield errors and score are defined as TypeScript functions that iterate over the assignments and each check for
violations of their rule.

The example rule below checks if the assigned demonstrators are
senior-enough (`assets/js/processing/rules/dem_year_older_than_module.ts`).

```typescript
import { TimetableEntryType } from "../../types/TimetableEntryType";
import { Error } from "../../models/Error";
import { ERRORS } from "../../consts/RuleDefinitions";
import { Database } from "../../models/Database";
import { Assignments } from "../../models/Assignments";
import { ErrorType } from "../../types/score/ErrorType";

export default (
  database: Database,
  assignments: Assignments,
  labsValidity: Map<number, boolean>,
  errors: ErrorType[],
) => {
  // For every day
  for (const date of database.timetableEntriesByDate.keys()) {

    const labs = database.labsByDate.get(date) as TimetableEntryType[];

    // For every lab in a day
    for (const lab of labs) {

      const module = database.getModuleFlavour(lab.moduleFlavour.id).module;

      // For every demonstrator assigned to the lab
      for (const demonstrator of database.getAssignments(lab, assignments)) {

        if (demonstrator.year <= module.year) {
          errors.push(new Error(
            ERRORS.DEM_YEAR_OLDER_THAN_MODULE,
            {
              name: demonstrator.fullName,
            },
            date,
            lab.id,
            demonstrator.id
          ));
          labsValidity.set(lab.id, false);
        }
      }
    }
  }
};
```

The rule function is accompanied by a human-readable explanation.

```typescript
export const ERRORS: RuleDefinitions = {
  //...

  DEM_YEAR_OLDER_THAN_MODULE: {
    description: "Demonstrator is at least a year above the module they're demonstrating on.",
    error: "[name] is not senior-enough.",
  },

  //...
}
```

## Web Workers

The manage view utilises web workers in order to increase performance of the optimisation. During automatic
optimisation, the most amount of time is spent calculating the scores of the children. By offloading this computation to
as many web workers as the administrator's machine has and as many children as are created in one generation, we are
able to drastically reduce the optimisation time.

The optimisation is being parallelized by calculating one child per CPU processing core. The example below shows an
optimisation with 6 children being produced each round being run on a 12-thread system. 525% indicated by Htop on a 6
thread problem very roughly indicates 87.5% parallelization efficiency, which is remarkably high.

**Note:** The calculation assumes that the cpu utilisation of the rest of the system is negligible.

![](images/doc_web_worker.png)

## Fake Data

For development purposes, fake data was required in order to test individual components of the system. This data is
being generated in `src/Console/FakeDataCommand.php`. The structure of the data should vaguely resemble data in a real
world. This is achieved by basing distributions on real anonymised data from previous years and modelling the situation
from memory as close to the reality as possible.

The fake data generation has been split into 2 stages. The first stage generates everything apart from assignments and
the second stage generates just assignments. This is so that the admin has a chance to manually create a demonstrator
and be included in the fake data assignment generation process.

## Authentication

There are 2 means of authentication currently implemented:

- OAuth 2.0 using Microsoft ID
- Custom email OTC tokens

The Microsoft authenticator's implementation is implemented in `src/Security/MicrosoftAuthenticator.php` and can be
enabled by changing the `AUTHENTICATION_METHOD` environmental variable in `docker-compose.yml`. It's currently disabled
because Swansea University "does not feel like students should have access" to the user domain for its intended purpose.
The custom email authenticator using one-time codes is arguably less secure than the first method, but it's the best
that can be achieved considering the requirements and constraints since authentication state is not persisted.

## Timetable Entries File Structure

The file containing timetable entries for import into the system needs to be in the following format:

- Comma separated file
- With the following columns
    - DURATION - [PHP DateInterval duration](https://www.php.net/manual/en/dateinterval.construct.php)
    - ROOM - string
    - START - [date in format Y-m-d\TH:i](https://www.php.net/manual/en/datetime.format.php)
    - TITLE - string
    - TYPE - string, one of "LECTURE", "LAB" or "OTHER"
    - UNIID - integer
- With no duplicates
- With each entry covering the whole duration of one lab

![](images/doc_import_format.png)

An example file could look like this:

```csv
"DURATION","ROOM","START","TITLE","TYPE","UNIID"
"PT1H","Zoom ID: 4177612215","2020-09-28T09:00","CSC318 Lab Selection","LECTURE","40121"
"PT1H","Zoom ID: 4177612215","2020-09-28T09:00","CSCM18 Lab Selection","LECTURE","40122"
"PT1H","Online Class","2020-09-28T10:00","CS-110","LECTURE","39829"
"PT2H","Online Class","2020-09-28T10:00","CS-250","LECTURE","39857"
"PT2H","School of Management 128 (PC)","2020-09-28T10:00","CSC345 Group A","LAB","39468"
"PT2H","School of Management 128 (PC)","2020-09-28T10:00","CSCM45 Group A","LAB","39469"
"PT2H","Grt Hall 037","2020-09-28T10:00","CSCM69","LECTURE","39444"
"PT1H","Online Class","2020-09-28T11:00","CS-170"docs/DOCUMENTATION.md,"LECTURE","39879"
"PT2H","School of Management 128 (PC)","2020-09-28T12:00","CSC345 Group B","LAB","39466"
"PT2H","School of Management 128 (PC)","2020-09-28T12:00","CSCM45 Group B","LAB","39467"
```

## Clearing All Data

To delete **all** data from the system, execute the following command on the server.

**Warning:** Note that this operation cannot be undone!

```bash
docker-compose exec app ash -c "php bin/console sdm:data:clear --force"
```
